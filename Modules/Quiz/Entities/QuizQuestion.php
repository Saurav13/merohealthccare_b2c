<?php

namespace Modules\Quiz\Entities;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model
{
    protected $fillable = ['question', 'answer_explanation'];

    public function options()
    {
        return $this->hasMany(QuizOption::class, 'question_id');
    }

    public function getAnswerAttribute()
    {
        return $this->options()->where('correct', 1)->first()->option;
    }
}
