<?php

namespace Modules\Quiz\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Quiz\Entities\QuizOption;
use Modules\Quiz\Entities\QuizQuestion;

class QuizQuestionController extends Controller
{
    public function index()
    {
        $questions = QuizQuestion::with('options')->orderBy('id', 'desc')->paginate(20);

        return view('quiz::quiz.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('quiz::quiz.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'questions' => 'required|array|min:1',
        //     'questions.*.question' => 'required',
        //     'questions.*.option1' => 'required',
        //     'questions.*.option2' => 'required',
        //     'questions.*.option3' => 'required',
        //     'questions.*.option4' => 'required',
        //     'questions.*.correct' => 'required|in:option1,option2,option3,option4',
        // ]);

        // $this->validate($request, [
        //     'question' => 'required',
        //     'option1' => 'required',
        //     'option2' => 'required',
        //     'option3' => 'required',
        //     'option4' => 'required',
        //     'correct' => 'required|in:option1,option2,option3,option4',
        // ]);

        // foreach($request->questions as $question){
            $question = QuizQuestion::create($request->all());

            foreach ($request->input() as $key => $value) {
                if(strpos($key, 'option') !== false && $value != '') {
                    $status = $request->input('correct') == $key ? 1 : 0;
                    QuizOption::create([
                        'question_id' => $question->id,
                        'option'      => $value,
                        'correct'     => $status
                    ]);
                }
            }
        // }

        $request->session()->flash('success', 'Questions successfully added.');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $question = QuizQuestion::findOrFail($id);

        return view('quiz::quiz.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        // $this->validate($request, [
        //     'question' => 'required',
        //     'option1' => 'required',
        //     'option2' => 'required',
        //     'option3' => 'required',
        //     'option4' => 'required',
        //     'correct' => 'required|in:option1,option2,option3,option4',
        // ]);
        
        $question = QuizQuestion::findOrFail($id);
        $question->update($request->all());

        $options = $question->options;
        $i = 0;
        foreach ($request->input() as $key => $value) {
            if(strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                $options[$i++]->update([
                    'option'      => $value,
                    'correct'     => $status
                ]);
            }
        }

        $request->session()->flash('success', 'Question successfully edited.');

        return redirect()->route('admin-quiz.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $question = QuizQuestion::findOrFail($id);
        $question->delete();

        $request->session()->flash('success', 'Question successfully deleted.');
        return redirect()->back();
    }
}
