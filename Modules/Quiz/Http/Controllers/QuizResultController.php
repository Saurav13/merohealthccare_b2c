<?php

namespace Modules\Quiz\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Quiz\Entities\QuizResult;

class QuizResultController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $results = QuizResult::orderBy('id', 'desc')->paginate(20);

        return view('quiz::quiz.result', compact('results'));
    }
}
