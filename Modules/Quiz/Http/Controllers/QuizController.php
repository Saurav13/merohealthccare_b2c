<?php

namespace Modules\Quiz\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Quiz\Entities\QuizOption;
use Modules\Quiz\Entities\QuizQuestion;
use Modules\Quiz\Entities\QuizResult;

class QuizController extends Controller
{
    public function index()
    {
        $questions = QuizQuestion::with('options')->inRandomOrder()->limit(10)->get();
        return view('quiz::index', compact('questions'));
    }

    public function save(Request $request){
        $validator = \Validator::make($request->customer, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Incomplete Data Provided.'], 422);

        }
        $score = 0;

        foreach ($request->input('result.questions', []) as $question_id => $answer) {
            $answerDB =  QuizOption::where('question_id', $question_id)->find($answer);

            if ($answerDB && $answerDB->correct) {
                $score++;
            }
        }

        $result = new QuizResult();
        $result->customer_name = $request->customer['name'];
        $result->customer_email = $request->customer['email'];
        $result->customer_phone = $request->customer['phone'];
        $result->score = $score;
        $result->save();

        return response()->json(['message' => 'Result saved successfully.'], 200);
    }
}
