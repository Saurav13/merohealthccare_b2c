<?php

Route::group(['middleware' => ['web','auth:admin'], 'prefix' => 'admin/quiz', 'namespace' => 'Modules\Quiz\Http\Controllers'], function()
{
    // Route::get('/', 'QuizController@index');
    Route::get('/','QuizQuestionController@index')->name('admin-quiz.index');
    Route::get('questions/create','QuizQuestionController@create')->name('admin-questions.create');
    Route::post('questions','QuizQuestionController@store')->name('admin-questions.store');
    Route::get('questions/{id}/edit','QuizQuestionController@edit')->name('admin-questions.edit');
    Route::post('questions/{id}/update','QuizQuestionController@update')->name('admin-questions.update');
    Route::post('questions/{id}/delete','QuizQuestionController@destroy')->name('admin-questions.delete');

    Route::get('/results','QuizResultController@index')->name('admin-quiz.results');
    
});

Route::group(['middleware' => ['web'], 'prefix' => '/quiz', 'namespace' => 'Modules\Quiz\Http\Controllers'], function()
{
    Route::get('/','QuizController@index')->name('quiz');
    Route::post('/save','QuizController@save')->name('quiz.save');
});
