<?php

namespace Modules\Quiz\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Quiz\Entities\QuizOption;
use Modules\Quiz\Entities\QuizQuestion;

class QuizDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for($i=0; $i<20; $i++){
            $question = QuizQuestion::create([
                'question' => 'Question '.($i+1)
            ]);

            $correct = rand(0,3);
            for($j=0; $j<4; $j++){
           
                QuizOption::create([
                    'question_id' => $question->id,
                    'option'      => 'Option '.($j+1),
                    'correct'     => $correct == $j ? 1 : 0,
                ]);
                
            }
        }
    }
}
