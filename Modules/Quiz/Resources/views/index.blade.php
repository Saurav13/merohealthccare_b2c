<html>
    <head>
        <title>Mero Health Quiz</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" src="{{ Module::asset('quiz:css/sales.css') }}" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

        <style>
            [ng\:cloak],
            [ng-cloak],
            [data-ng-cloak],
            [x-ng-cloak],
            .ng-cloak,
            .x-ng-cloak {
                display: none !important;
            }
            .invalid-feedback{
                color: #f50b0b;
            }
            .btn-block {
                text-align: left;
            }
        
            .score{
                font-weight: 600;
                color: black;
                font-size: 15px;
            }
            .start_div{
                display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            grid-gap: 13px;
            }
            .celebration{
                background: url({{ Module::asset('quiz:img/stars.gif') }})
            }
          
            .bottom_text{
                font-size: 16px;
                font-weight: 600;
            }
            
        </style>
        
    </head>

    <body>

        <div class="container-fluid bg-info" ng-app="QuizApp" ng-controller="QuizController" ng-init="init('{{ base64_encode($questions) }}')" ng-cloak>
            <div class="modal-dialog" id="page1" ng-show="current_page == 'page1'">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3><span class="label label-warning" id="qid"><i class="fa fa-star"></i></span> Mero Health Quiz</h3>
                    </div>
                    <div class="modal-body">
                        
                        <div class="start_div" id="quiz" data-toggle="buttons">
                            <img src="{{ Module::asset('quiz:img/logo.webp') }}" style="height: 8rem"/>
                            <button class="btn btn-primary" ng-click="current_page = 'page2'">Start Quiz</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-dialog" id="page2" ng-show="current_page == 'page2'">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3><span class="label label-warning" id="qid"><i class="fa fa-star"></i></span> Mero Health Quiz</h3>
                    </div>
                    <div class="modal-body">
                        
                        <form name="nameForm" novalidate class="start_div"  data-toggle="buttons">
                            <label>Enter Your Name</label>
                            <!-- <img src="./img/logo.webp" style="height: 8rem"/> -->
                            <input type="text" class="form-control" name="name" style="width:80%" ng-model="customer.name" required />
                            <div class="invalid-feedback" ng-messages="nameForm.name.$touched && nameForm.name.$error">
                                <div ng-message="required">
                                    This field is required.
                                </div>
                            </div>
                            <button class="btn btn-primary" ng-disabled="nameForm.$invalid" ng-click="current_page = 'page3'">Start</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-dialog" id="page3" ng-show="current_page == 'page3'">
                <div class="modal-content" > 
                    <div class="modal-header">
                        <h3><span class="label label-warning" id="qid">@{{ current_index + 1 }}</span> @{{ questions[current_index].question }} </h3>
                    </div>
                    <div class="modal-body">

                        <div class="quiz row" id="quiz" data-toggle="buttons">
                            <div class="col-md-6 mb-3" style="margin-bottom:1rem" ng-repeat="option in questions[current_index].options">
                                <label ng-click="check($index, $event)" class="option@{{ option.id }} option_click element-animation4 btn btn-lg btn-primary btn-block">
                                    <span class="btn-label">
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                    </span>
                                    @{{ option.option }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-muted">
                        <span class="score">Your Score:  @{{ result.score }} /10</span>
                        <span class="pull-left">
                            <img id="bottom_loading" hidden src="{{ Module::asset('quiz:img/loading.gif') }}" style="height:3rem">
                            <span class="bottom_text text-success" id="bottom_correct" hidden >Correct</span>
                            <span class="bottom_text text-danger" id="bottom_incorrect" hidden >Incorrect</span>
                        </span>
                        <div>
                            <p id="answer" style="margin: 12px 0; text-align: left;"></p>

                            <button class="btn btn-primary" ng-if="evaluated" ng-click="next()"> @{{ current_index == 9 ? 'Finish' : 'Next' }}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-dialog" id="page4" ng-show="current_page == 'page4'">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3><span class="label label-warning" id="qid"><i class="fa fa-star"></i></span> Mero Health Quiz</h3>
                    </div>
                    <div class="modal-body">
                        <div class="invalid-feedback" ng-if="error">
                            @{{ error }}
                        </div>
                        <form name="infoForm" ng-submit="submitResult()" novalidate class="start_div" id="quiz" data-toggle="buttons">
                            <h4>@{{ result.score >= 8 ? 'Congratulations!!' : 'Thank You!' }}</h4>

                            <p ng-if="result.score >= 8">You have won a special reward from Mero Health Care. Please fill up the necessary details to claim your reward.</p>
                            <p ng-if="result.score < 8">Thank your for participating. Please fill up the necessary details below to stay connected with Mero Health Care.</p>

                            <input type="email" class="form-control" placeholder="Email Address" ng-model="customer.email" required name="email" />
                            <div class="invalid-feedback" ng-messages="infoForm.email.$touched && infoForm.email.$error">
                                <div ng-message="required">
                                    This field is required.
                                </div>
                                <div ng-message="email">
                                    Email must be valid.
                                </div>
                            </div>
                            <input type="text" class="form-control" ng-model="customer.phone" placeholder="Phone Number" name="phone" required />
                            <div class="invalid-feedback" ng-messages="infoForm.phone.$touched && infoForm.phone.$error">
                                <div ng-message="required">
                                    This field is required.
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" ng-disabled="infoForm.$invalid || evaluated" value="Submit">
                        </form>
                    </div>
                    <div class="modal-footer text-muted">
                        <span class="score">Your Score:  @{{ result.score }} /10</span>
                    </div>
                </div>
            </div>
            <div class="modal-dialog" id="page5" ng-show="current_page == 'page5'">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3><span class="label label-warning" id="qid"><i class="fa fa-star"></i></span> Mero Health Quiz</h3>
                    </div>
                    <div class="modal-body">
                        
                        <p>Thank you for participating! We will get back to you as soon as possible.</p>

                    </div>
                </div>
            </div>
            
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-messages.min.js"></script>
        <script>
            var r_app = angular.module('QuizApp', ['ngMessages'])
            .controller('QuizController', function ($scope, $http, $filter) {
                $scope.questions = [];
                $scope.current_index = 0;
                $scope.current_page = 'page1';
                $scope.customer;
                $scope.result = {
                    'questions': {},
                    'score': 0
                };
                $scope.evaluated = false;

                $scope.init = function(data){
                    $scope.questions = JSON.parse(atob(data));
                }

                $scope.next = function(){

                    $('.modal-content').removeClass("celebration");
                    $('#answer').html("");
                    $('#bottom_correct').attr("hidden", "hidden");
                    $('#bottom_incorrect').attr("hidden", "hidden");
                    $('#bottom_loading').attr("hidden","true");
                    $scope.evaluated = false;

                    if($scope.current_index == 9){
                        $scope.current_page = 'page4';
                    }else
                        $scope.current_index++;
                }

                $scope.check = function(index, event){
                    if($scope.evaluated) return;
                    var question = $scope.questions[$scope.current_index];

                    if($scope.result.questions[question.id]) return;

                    $scope.result.questions[question.id] = question.options[index].id;
                    
                    $(event.target).removeClass("btn-primary");
                    $(event.target).addClass("btn-warning");
                    $('#bottom_loading').removeAttr("hidden");
                    setTimeout(() => {
                        if(question.options[index].correct)
                        {
                            $(event.target).removeClass("btn-warning");
                            $(event.target).addClass("btn-success");
                            $('.modal-content').addClass("celebration");
                            $('#bottom_correct').removeAttr("hidden");
                            $('#bottom_loading').attr("hidden","true");

                            $scope.result.score++;
                        }
                        else{
                            var correct = $filter('filter')(question.options, {'correct': 1})[0];
                            
                            $(event.target).addClass("btn-danger");
                            $('.quiz').find('.option'+correct.id).removeClass("btn-warning");
                            $('.quiz').find('.option'+correct.id).addClass("btn-success");
                            $('#bottom_incorrect').removeAttr("hidden");
                            $('#bottom_loading').attr("hidden","true");
                        }
                        $('#answer').html(question.answer_explanation);
                        $scope.evaluated = true;
                        $scope.$apply();

                    }, 1000);
                }

                $scope.submitResult = function() {
                    if($scope.infoForm.$valid) {
                        $scope.evaluated = true;

                        $http({
                            method: 'POST',
                            url: "{{ route('quiz.save') }}",
                            data: {
                                'customer': $scope.customer,
                                'result': $scope.result,
                                '_token': "{{ csrf_token() }}"
                            },
                        }).then(function successCallback(response) {
                            $scope.evaluated = false;
                            $scope.current_page = 'page5';

                        }, function errorCallback(response) {
                            $scope.evaluated = false;

                            if(response.data && response.data.message){
                                $scope.error = response.data.message;
                            }else
                                $scope.error = "Something went wrong, please try again after few minutes.";
                        });
                    }
                }
            });

        </script>
    </body>
</html>