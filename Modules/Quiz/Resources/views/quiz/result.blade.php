@extends('layouts.admin')

@section('content')

<style>
    .pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
    color: #777;
    cursor: not-allowed;
    background-color: #fff;
    border-color: #fff;
}
</style>


    <div class="right-side">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Starting of Dashboard data-table area -->
                    <div class="section-padding add-product-1">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="add-product-box">
                                    <div class="product__header">
                                        <div class="row reorder-xs">
                                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                                <div class="product-header-title">
                                                    <h2>Quiz Program Results</h2>
                                                    <p>Dashboard <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Quiz Program <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Results
                                                </div>
                                            </div>
                                            @include('includes.notification')
                                        </div>   
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                             
                                                <div class="table-responsive">
                                                    <table id="order-table_wrapper" class="table table-striped table-hover products dt-responsive" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 130px;">Date </th>
                                                                <th style="width: 130px;">Customer Details</th>
                                                                <th style="width: 150px;">Score</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            @foreach($results as $result)                                                  

                                                                <tr>
                                                                    <td> {{date('d M Y h:i A',strtotime($result->created_at))}} </td>
                                                                    <td>
                                                                        
                                                                        {{ $result->customer_name }}
                                                                        <br>
                                                                        {{ $result->customer_email }}
                                                                        <br>
                                                                        {{ $result->customer_phone }}
                                                                    </td>
                                                                    <td> {{ $result->score }}</td>
                                                                    
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="text-right">
                                                        {!! $results->render() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ending of Dashboard data-table area -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $('#order-table_wrapper').dataTable( {
        "pageLength": 20,
        "order": [[ 2, "desc" ]]
    });
 </script>


@endsection