@extends('layouts.admin')

@section('content')
    <div class="right-side">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- Starting of Dashboard area -->
                    <div class="section-padding add-product-1">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="add-product-box">
                                    <div class="product__header"  style="border-bottom: none;">
                                        <div class="row reorder-xs">
                                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                                <div class="product-header-title">
                                                    <h2>Edit Question
                                                        <a href="{{ route('admin-quiz.index') }}" style="padding: 5px 12px;" class="btn add-back-btn"><i class="fa fa-arrow-left"></i> Back</a></h2>
                                                    <p>Dashboard <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Quiz Program <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Questions
                                                </div>
                                            </div>
                                            @include('includes.notification')
                                        </div>   
                                    </div>
                                    <hr>
                                    <div>

                                        {{-- @include('includes.form-error') --}}
                                        @include('includes.form-success')

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane fade active in">
                                                <form class="form-horizontal" action="{{ route('admin-questions.update', $question->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <div class="question">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="questionText">Question*</label>

                                                            <div class="col-sm-6">
                                                                <textarea class="form-control " rows="5" name="question" id="questionText" placeholder="Question" required="">{{ $question->question }}</textarea>
                                                                
                                                                @if($errors->has('question'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('question') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        @for ($i = 0; $i < 4; $i++)
                                                            <div class="form-group">
                                                                <label for="editOption{{ $i+1 }}" class="control-label col-sm-4">Option #{{ $i+1 }}*</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control rounded-0 form-control-md" name="option{{ $i+1 }}" value="{{ $question->options[$i]->option }}" id="editOption{{ $i+1 }}" placeholder="Option {{ $i+1 }}" required>
                                                                    @if($errors->has('option'.($i+1)))
                                                                        <p class="help-block">
                                                                            {{ $errors->first('option'.($i+1)) }}
                                                                        </p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    
                                                        <div class="form-group">
                                                            <label for="exampleSelect1" class="control-label col-sm-4">Correct Option*</label>
                                                            <div class="col-sm-6">
                                                                <select class="form-control rounded-0" id="exampleSelect1" name="correct" required>
                                                                    <option value="option1" {{ $question->options[0]->correct == 1 ? 'selected' : '' }}>Option #1</option>
                                                                    <option value="option2" {{ $question->options[1]->correct == 1 ? 'selected' : '' }}>Option #2</option>
                                                                    <option value="option3" {{ $question->options[2]->correct == 1 ? 'selected' : '' }}>Option #3</option>
                                                                    <option value="option4" {{ $question->options[3]->correct == 1 ? 'selected' : '' }}>Option #4</option>
                                                                </select>
                                                                @if($errors->has('correct'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('correct') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                            
                                                        </div>
        
                                                        <div class="form-group g-mb-25">
                                                            <label for="exampleTextarea" class="control-label col-sm-4">Answer Explanation</label>
                                                            <div class="col-sm-6">
                                                                <textarea class="form-control rounded-0 form-control-md" rows="6" name="answer_explanation">{{ $question->answer_explanation }}</textarea>
                                                                @if($errors->has('answer_explanation'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('answer_explanation') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="add-product-footer">
                                                        <button type="submit" class="btn add-product_btn">Save</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Ending of Dashboard area --> 
                </div>
            </div>
        </div>
    </div>

@endsection