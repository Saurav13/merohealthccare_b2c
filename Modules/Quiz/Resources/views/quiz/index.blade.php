@extends('layouts.admin')

@section('content')

<style>
    .pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
        color: #777;
        cursor: not-allowed;
        background-color: #fff;
        border-color: #fff;
    }
</style>


    <div class="right-side">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Starting of Dashboard data-table area -->
                    <div class="section-padding add-product-1">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="add-product-box">
                                    <div class="product__header">
                                        <div class="row reorder-xs">
                                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                                <div class="product-header-title">
                                                    <h2>Quiz Questions</h2>
                                                    <p>Dashboard <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Quiz Program <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Questions
                                                </div>
                                            </div>
                                            @include('includes.notification')
                                        </div>   
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                             
                                                <div class="table-responsive">
                                                    <table id="order-table_wrapper" class="table table-striped table-hover products dt-responsive" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 130px;">Question </th>
                                                                <th style="width: 130px;">Answer</th>
                                                                <th style="width: 100px;">Actions</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            @foreach($questions as $question)                                                  

                                                                <tr>
                                                                    <td> {{ $question->question }} </td>
                                                                    <td>
                                                                       {{ $question->answer }}
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin-questions.edit',$question->id)}}" class="btn btn-primary product-btn"><i class="fa fa-edit"></i> Edit</a>
                                                                        <a href="javascript:;" data-href="{{route('admin-questions.delete',$question->id)}}" data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger product-btn"><i class="fa fa-trash"></i> Remove</a>
                                                                    </td>
                                                                    
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="text-right">
                                                        {!! $questions->render() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ending of Dashboard data-table area -->
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center">You are about to delete this Question. Everything will be deleted under this.</p>
                    <p class="text-center">Do you want to proceed?</p>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger btn-ok" type="submit">Delete</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $('#order-table_wrapper').dataTable( {
        "pageLength": 20,
        "order": [[ 2, "desc" ]]
    });
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('form').attr('action', $(e.relatedTarget).data('href'));
    });

    $( document ).ready(function() {
        $(".add-button").append('<div class="col-sm-4 add-product-btn text-right">'+
          '<a href="{{route('admin-questions.create')}}" class="add-newProduct-btn">'+
          '<i class="fa fa-plus"></i> Add New Question</a>'+
          '</div>');                                                                       
    });
 </script>


@endsection