<?php

namespace App\Http\Controllers\Admin;

use App\Generalsetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LoyalityHistory;

class LoyalityProgramController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function settings()
    {
        $gc = Generalsetting::findOrFail(1);
        $loyality_percentage = $gc->loyality_percentage;
        return view('admin.loyality_program.settings',compact('loyality_percentage'));
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'loyality_percentage' => 'required|numeric|min:0|max:100'
        ]);

        $gc = Generalsetting::findOrFail(1);
        $gc->loyality_percentage = $request->loyality_percentage;
        $gc->save();

        $request->session()->flash('success', 'Data Updated Successfully.');
        return redirect()->back();
    }

    public function history() {
        $histories = LoyalityHistory::orderBy('id','desc')->paginate(20);
        return view('admin.loyality_program.history',compact('histories'));

    }
}
