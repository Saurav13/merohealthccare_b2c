@extends('layouts.front1')
@section('title', 'Appilcation Received')
@section('content')

    <div class="container" style="margin-bottom: 30px;">
        <div class="card card-pricing">
            <div class="card-body ">
                <div class="icon icon-info">
                    <i class="material-icons">done_outline</i>
                </div>
                <h3 class="card-title">Success !</h3>
                <p class="card-description">
                    Your appilcation has been received. Thank you for your application. We will contact u soon.
                </p>
                <a href="/" class="btn btn-info btn-round"><i class="material-icons">home</i> Home</a>
            </div>
        </div>
    </div>

@endsection
