@extends('layouts.front1')
@section('title', 'FAQs')
@section('content')
<style>
  span{
    font-family: 'Muli' !important;
}
</style>

    <div class="container" style="margin-bottom: 30px;">
        <div id="collapse">
            <div class="title">
                <h3>Frequently Asked Questions</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="accordion" role="tablist">
                        <div class="card card-collapse">
                            <div class="card-header" role="tab" id="heading{{ $fq->id }}">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse{{ $fq->id }}" aria-expanded="true"
                                        aria-controls="collapseOne" class="">
                                        {{ $fq->title }}
                                        <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse{{ $fq->id }}" class="collapse" role="tabpanel"
                                aria-labelledby="heading{{ $fq->id }}" data-parent="#accordion" style="">
                                <div class="card-body">
                                    {!! $fq->text !!}
                                </div>
                            </div>
                        </div>

                        @foreach ($faqs as $faq)
                            <div class="card card-collapse">
                                <div class="card-header" role="tab" id="heading{{ $faq->id }}">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapse{{ $faq->id }}" aria-expanded="true"
                                            aria-controls="collapseOne" class="">
                                            {{ $faq->title }}
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse{{ $faq->id }}" class="collapse" role="tabpanel"
                                    aria-labelledby="heading{{ $faq->id }}" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        {!! $faq->text !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
