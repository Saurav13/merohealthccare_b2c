@extends('layouts.front1')
@section('title', 'Blog Posts')
@section('content')

    <div class="section section-blog">
        <div class="container">
            <h2 class="section-title">Latest Blogs</h2>
            <div class="row">
                @foreach ($blogs as $blogg)
                    <div class="col-md-4">
                        <div class="card card-blog">
                            <div class="card-header card-header-image">
                                <a href="#pablo">
                                    <img src="{{ asset('assets/images/' . $blogg->photo) }}" alt="blog image">
                                </a>
                                <div class="colored-shadow"
                                    style="background-image: url('{{ asset('assets/images/' . $blogg->photo) }}'); opacity: 1;">
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-rose">{{ date('jS M, Y', strtotime($blogg->created_at)) }}
                                </h6>
                                <h4 class="card-title">
                                    <a
                                        href="{{ route('front.blogshow', $blogg->slug) }}">{{ strlen($blogg->title) > 80 ? substr($blogg->title, 0, 80) . '...' : $blogg->title }}</a>
                                </h4>
                                <p class="card-description">
                                    {{ substr(strip_tags($blogg->details), 0, 80) }}...<a
                                        href="{{ route('front.blogshow', $blogg->slug) }}">Read more</a>
                                </p>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="text-center">
        {!! $blogs->links('layouts.partials.pagination') !!}
    </div>

@endsection
