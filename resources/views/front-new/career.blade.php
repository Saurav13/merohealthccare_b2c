@extends('layouts.front1')
@section('title', 'Career')
@section('content')

<style>
  span{
    font-family: 'Muli' !important;
}
</style>
    <div class="container" style="padding:20px;">
        <div class="row">
            <div class="col-lg-6 g-mb-50 g-mb-0--lg">
                <img class="img-fluid" style="margin-top:50px;" src="{{ url('assets/images/logo.jpg') }}" style=""
                    alt="Image Description">
            </div>

            @php
                $description = App\CareerDescription::get();
                $hirings = App\CareerHiring::get();
                $openings = App\CareerOpening::get();
            @endphp

            <div class="col-lg-6 align-self-center">
                @foreach ($description as $dd)
                    @if ($dd->points == null)
                        <header class="">
                            <h2 class="h3"><strong>{{ $dd->title }}</strong></h2>
                        </header>

                        <p class="">{{ $dd->description }}</p>
                    @endif
                @endforeach

                <ul class="list-unstyled">
                    @foreach ($description as $dd)
                        @if ($dd->description == null && $dd->title == null && $dd->points != null)
                            <li class="d-flex g-mb-10">
                                <i class="material-icons">done</i>
                                {{ $dd->points }}
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <section class="g-flex-centered"
        style="background-image: radial-gradient(circle farthest-side at 110% 0, #18ba9b, #2385aa);background-repeat: no-repeat;">
        <div class="container">
            @foreach ($hirings as $h)
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <div class="u-heading-v2-5--bottom g-brd-primary">
                            <h2 class="u-heading-v2__title text-uppercase" style="color: aliceblue">{{ $h->title }}</h2>
                        </div>
                        <p class="lead mb-0 g-line-height-2" style="color: aliceblue">{{ $h->description }}</p>
                    </div>

                    <div class="col-md-6 align-self-center g-py-20" style="padding: 40px 0px;">
                        <img class="w-100 rounded"
                            src="{{ $h->image ? asset('assets/images/' . $h->image) : 'http://fulldubai.com/SiteImages/noimage.png' }}"
                            alt="Iamge Description">
                    </div>
                </div>
            @endforeach
        </div>
    </section>


    <div class="container" style="padding: 50px 20px;">
        <h3 class="title text-center">Current Openings</h3>
        <div class="row g-mx-minus-10 g-mb-50">
            @foreach ($openings as $h)

                <div class="col-md-8 col-lg-8 g-px-10 ml-md-auto mr-md-auto">
                    <!-- Article -->
                    <article class="media">
                        <!-- Article Image -->
                        <div style="max-width:100px;margin-right:15px;">
                            <img class="d-flex w-100"
                                src="{{ $h->image ? asset('assets/images/' . $h->image) : 'http://fulldubai.com/SiteImages/noimage.png' }}"
                                alt="Image Description">
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Info -->
                        <div class="media-body align-self-center">
                            <h4 class="h5 g-mb-7">
                                <a class="g-color-black g-color-primary--hover g-text-underline--none--hover"
                                    href="#">{{ $h->title }}</a>
                            </h4>
                            <a class="btn btn-primary pull-right" style="border-radius:30px;" data-toggle="collapse"
                                href="#{{ $h->id }}" role="button" aria-expanded="false"
                                aria-controls="collapseExample">
                                <i class="fa fa-list"></i>
                            </a>

                            <!-- Article Footer -->
                            <footer class="d-flex justify-content-between g-font-size-16">
                                <span class="g-color-black g-line-height-1"></span>
                                <ul class="list-inline g-color-gray-light-v2 g-font-size-14 g-line-height-1">

                                    <div class="media-body col-4">
                                        <span class="text-rose g-font-weight-500 g-font-size-40 g-line-height-0_7"
                                            style="font-size:40px;">{{ date('d', strtotime($h->created_at)) }}<span
                                                class="g-line-height-0_7 text-primary"
                                                style="font-size:30px">{{ date('M ', strtotime($h->created_at)) }}
                                            </span></span>
                                        <span
                                            class="g-line-height-0_7 text-primary">{{ date('Y', strtotime($h->created_at)) }}
                                        </span>
                                    </div>
                                </ul>
                            </footer>
                        </div>

                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-md-8 col-lg-8 g-px-10 ml-md-auto mr-md-auto">
                    <div class="collapse" id="{{ $h->id }}">
                        <article id="myDIV"
                            class="media g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-pa-10 g-mb-20">
                            <div class="card card-body" style="border-color: #fff;">
                                {!! $h->description !!}

                                <h4 style="" class="text-center"><i class="icon-share-alt"></i> Share with:</h4>

                                <div class="sharethis-inline-share-buttons pull-left">
                                    <style>
                                        #st-el-3 .st-btns {
                                            bottom: 56px;
                                            left: 0;
                                            margin: 100px auto 0;
                                            max-width: 90%;
                                            position: absolute;
                                            right: 0;
                                            text-align: center;
                                            top: 10px !important;
                                            z-index: 20;
                                            overflow-y: auto;
                                        }

                                    </style>
                                </div>
                            </div>

                        </article>

                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="container" style="padding: 50px 20px;">
        <div class="container">
            <h3 class="title text-center">Candidate Application</h3>
            @include('includes.form-error')
            <form class=" text-left" action="{{ route('admin-career-candidates-submit') }}" method="POST"
                enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">First Name *</label>
                            <input id="inputGroup1_1" name="first_name" class="form-control form-control-md rounded-0"
                                type="text" placeholder="First Name" required>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Middle Name</label>
                            <input id="inputGroup1_1" name="middle_name" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Middle name">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Last Name *</label>
                            <input id="inputGroup1_1" name="last_name" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Last Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Email *</label>
                            <input id="inputGroup1_1" name="email" class="form-control form-control-md rounded-0"
                                type="email" placeholder="yourmail@example.com" required>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Portfolio Website (eg-LinkedIn)</label>
                            <input id="inputGroup1_1" name="portfolio" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Portfolio Website">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Apply Position *</label>
                            <input id="inputGroup1_1" name="position" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Apply Position" required>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Salary Requirements *</label>
                            <input id="inputGroup1_1" name="salary_requirements"
                                class="form-control form-control-md rounded-0" type="text" placeholder="Salary Expectation"
                                required>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">When can you start? </label>
                            <input id="inputGroup1_1" name="start" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Joining Date">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Phone *</label>
                            <input id="inputGroup1_1" name="phone" class="form-control form-control-md rounded-0" type="tel"
                                placeholder="Eg. 9841000000" required>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1">Last Company Name (if any)</label>
                            <input id="inputGroup1_1" name="last_company" class="form-control form-control-md rounded-0"
                                type="text" placeholder="Last Company Name">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <div class="g-mb-20">
                            <label class="g-mb-10" for="">Upload your CV/Resume *</label>
                            <input name="cv" class=" " type="file" required>
                            <small class="form-text text-muted g-font-size-default g-mt-10">
                                <strong>Note: </strong>&nbsp; PDF, DOCX.
                            </small>
                        </div>
                    </div>


                </div>

                <div class="form-group g-mb-20">
                    <label class="g-mb-10" for="inputGroup2_2">Comments</label>
                    <textarea name="comments" id="inputGroup2_2" class="form-control form-control-md rounded-0" rows="3"
                        placeholder="If any"></textarea>
                </div>
                <div class="text-center">
                    <button class="js-fancybox-media btn btn-xl btn-primary g-rounded-30 text-center" type="submit">
                        <i class="material-icons" aria-hidden="true">send</i>
                        Send Application
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
