@extends('layouts.front1')
@section('title', 'Upload Prescription Success')
@section('content')

    <div class="container" style="margin-bottom: 30px;">
        <div class="card card-pricing">
            <div class="card-body ">
                <div class="icon icon-info">
                    <i class="material-icons">done_outline</i>
                </div>
                <h3 class="card-title">Successfully Uploaded Prescription</h3>
                <p class="card-description">
                    Thank you for your order and we will be in touch with you.
                </p>
                <a href="/" class="btn btn-info btn-round"><i class="material-icons">home</i> Home</a>
            </div>
        </div>
    </div>

@endsection
