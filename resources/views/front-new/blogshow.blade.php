@extends('layouts.front1')
@section('title', $blog->title . ' - Blog')

@section('content')

<style>
    span{
      font-family: 'Muli' !important;
  }
  </style>

    <div class="container" style="margin-bottom:30px;">
        <div class="row">
            <div class="col-md-8">
                <h3 class="title">{{ $blog->title }}</h3>
                <h5>· {{ $blog->created_at->diffForHumans() }} · {{ $blog->views }} views</h5>
                <img class="img-raised rounded img-fluid" style="width:100%" alt="{{ $blog->title }}"
                    src="{{ asset('assets/images/' . $blog->photo) }}">
                <p style="margin-top:10px;"> {!! $blog->details !!}</p>
                <h4 style="margin-top:2rem;" class="text-center">Share with</h4>
                {{-- <div id="shareRoundIcons" class="text-center" >

                </div> --}}
                <div class="sharethis-inline-share-buttons">
                    <style>
                        #st-el-3 .st-btns {
                            bottom: 56px;
                            left: 0;
                            margin: 100px auto 0;
                            max-width: 90%;
                            position: absolute;
                            right: 0;
                            text-align: center;
                            top: 10px !important;
                            z-index: 20;
                            overflow-y: auto;
                        }

                    </style>
                </div>

                @if ($blog->filename != null)
                    @php
                        $decoded = json_decode($blog->filename, true);
                    @endphp

                    <div class="container" style="margin-top:10px;">
                        <span><strong style="font-size:15px;margin-top:15px;"> Attachments :</strong></span>
                    </div>
                    @foreach ((array) $decoded as $d)
                        <a class="btn btn-primary" href="{{ route('blog-file', $d) }}" target="_blank" download><i
                                class="fa fa-file"></i> {{ str_limit($d, $limit = 10, $end = '...') }}</a>
                    @endforeach
                @endif
            </div>
            <div class="col-md-4 ml-auto" style="margin-top:30px;">
                <div class="card card-contact">

                    <div class="card-header card-header-raised card-header-info text-center">
                        <h4 class="card-title">Recent Posts</h4>
                    </div>
                    <div class="card-body">
                        @foreach ($lblogs->where('id', '!=', $blog->id) as $lblog)
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img-raised rounded img-fluid"
                                        src="{{ asset('assets/images/' . $lblog->photo) }}" alt="{{ $lblog->title }}">
                                </div>
                                <div class="col-md-8">
                                    <a
                                        href="{{ route('front.blogshow', $lblog->slug) }}">{{ strlen($lblog->title) > 30 ? substr($lblog->title, 0, 30) . '...' : $lblog->title }}</a>
                                    <h6 class="card-category text-rose">{{ date('d M Y', strtotime($lblog->created_at)) }}
                                    </h6>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
