@extends('layouts.front1')
@section('title', 'Ask A Doctor')
@section('content')

    <div class="container" style="margin-bottom:20px;">
        <h3 class="title">Available Doctors</h3>
        <div class="row">
            @foreach ($doctors as $doctor)
                <div class="col-md-4">
                    <div class="card card-blog">
                        <div class="card-header card-header-image">
                            <a
                                href="{{ route('user-askdoctor-doctor', ['id' => $doctor->id, 'slug' => str_slug($doctor->name, '-')]) }}">
                                <img style="height:300px;"
                                    src="{{ $doctor->photo ? asset('assets/images/' . $doctor->photo) : 'http://fulldubai.com/SiteImages/noimage.png' }}"
                                    alt="{{ $doctor->name }}">
                            </a>
                            <div class="colored-shadow"
                                style="background-image: url('{{ $doctor->photo ? asset('assets/images/' . $doctor->photo) : 'http://fulldubai.com/SiteImages/noimage.png' }}">
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-rose">{{ $doctor->post }}</h6>
                            <h4 class="card-title">
                                <a
                                    href="{{ route('user-askdoctor-doctor', ['id' => $doctor->id, 'slug' => str_slug($doctor->name, '-')]) }}">{{ $doctor->name }}</a>
                            </h4>
                            <p class="card-description">
                                {{ $doctor->description }}
                            </p>
                            <a href="{{ route('user-askdoctor-doctor', ['id' => $doctor->id, 'slug' => str_slug($doctor->name, '-')]) }}"
                                type="button" class="btn btn-info btn-block">Ask<div class="ripple-container"></div></a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endsection
