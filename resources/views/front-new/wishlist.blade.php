@extends('layouts.front1')
@section('title', $lang->wish_head)
@section('content')

    @php
    $i = 1;
    $j = 1;
    $now = Carbon\Carbon::now()->format('Y/m/d H:i');

    @endphp


    <div class="">
        <div class="container">
            <div class="row">
                @include('includes.catalog1')
                <div class="col-lg-9 col-sm-12 col-md-8">
                    <div class="card bg-info">
                        <div class="card-body">
                            <h2 class="card-title text-center h3" style="font-weight:300"><i class="icon-magnifier"></i>
                                <strong style="font-weight:700">Wishlist</strong></h2>
                            <button id="catalog-mobile-filter" type="button" class="btn btn-warning btn-round pull-right"
                                data-toggle="modal" data-target="#catalogFilter">
                                <i class="material-icons">filter_alt</i>
                            </button>
                            {{-- <p class="text-center" style="font-size:14px;color:yellow ">({{count($sproducts)}} Product(s) Found ! )</p> --}}
                        </div>
                    </div>


                    <div class="row">
                        @forelse($wproducts as $prod)
                            <div class="col-md-6" style="padding: 10px;">

                                @php
                                    $name = str_replace(' ', '-', $prod->name);
                                    $stk = (string) $prod->stock;
                                @endphp
                                <div class="item">

                                    <div class="container">

                                        @if ($gs->sign == 0)
                                            @if ($prod->discount_percent != 0)
                                                <div class="discount_badge">
                                                    <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span>
                                                    </p>
                                                </div>
                                            @endif
                                        @endif
                                        @if ($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                                            <div class="offer_timer_div js-countdown" data-end-date="{{ $prod->sale_to }}"
                                                data-month-format="%m" data-days-format="%D" data-hours-format="%H"
                                                data-minutes-format="%M" data-seconds-format="%S">
                                                <div class="offer_subdiv_primary">
                                                    <i class="material-icons">av_timer</i>
                                                </div>
                                                <div class="timer_alignment">
                                                    <div class="js-cd-days offer_subdiv_white"
                                                        style="color:rgb(214, 57, 57) !important;">
                                                        <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                                    </div>
                                                    <span style="font-size:15px;">D</span>
                                                </div>
                                                <div class="timer_alignment offer_subdiv_primary">
                                                    <div class="js-cd-hours ">
                                                        <p> <span>23</span> <span class="offer_time_unit">H<span></p>
                                                    </div>
                                                    <span style="font-size:15px;">H</span>
                                                </div>

                                                <div class="timer_alignment">
                                                    <div class="js-cd-minutes offer_subdiv_white">
                                                        <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                                    </div>
                                                    <span style="font-size:15px;">M</span>
                                                </div>

                                                <div class="timer_alignment offer_subdiv_primary">
                                                    <div class="js-cd-seconds">
                                                        <p> <span>02</span> <span class="offer_time_unit">S</span></p>
                                                    </div>
                                                    <span style="font-size:15px;">S</span>
                                                </div>
                                            </div>
                                        @endif
                                        @if ($prod->features != null && $prod->colors != null)
                                            @php
                                                $title = explode(',', $prod->features);
                                                $details = explode(',', $prod->colors);
                                            @endphp

                                        @endif
                                        <input type="hidden" value="{{ $prod->id }}">

                                        <a
                                            href="{{ route('front.product', ['id' => $prod->id, 'slug' => str_slug($name, '-')]) }}">

                                            <div class="featured_product_image_div hover-zoom">
                                                <img class="w-100 card-product-image"
                                                    src="{{ asset('assets/images/' . $prod->photo) }}"
                                                    alt="{{ $prod->name }}" />
                                                @if ($prod['requires_prescription'])
                                                    <h6 class="badge badge-pill badge-rose"
                                                        style="font-size:11px;transform: rotate(-90deg);position: absolute;top:32%;left:-10%;">
                                                        Prescriptionated
                                                    </h6>
                                                @endif
                                            </div>


                                            <div class="featured_product_detail_div">

                                                <h4 class="featured_product_name">
                                                    <a href="{{ route('front.product', ['id' => $prod->id, 'slug' => str_slug($name, '-')]) }}"
                                                        class="text-center">
                                                        {{ strlen($prod->name) > 38 ? substr($prod->name, 0, 38) . '...' : ucwords(strtolower($prod->name)) }}
                                                    </a>
                                                </h4>
                                                <p class="featured_product_subtitle">
                                                    {{ strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name, 0, 25))) . '...' : ucwords(strtolower(substr($prod->company_name, 0, 25))) }}
                                                </p>
                                                <p class="featured_product_price">{{ $curr->sign }}
                                                    {{ round($prod->cprice * $curr->value, 2) }}
                                                    @if ($prod->pprice != null && $prod->pprice != 0 && $prod->pprice > $prod->cprice)
                                                        <span class="featured_product_price_old">{{ $curr->sign }}
                                                            {{ round($prod->pprice * $curr->value, 2) }}</span>
                                                        <span>-{{ $prod->discount_percent }}%</span>
                                                </p>

                                                @endif
                                                <div class="text-center">
                                                    <input type="hidden" value="{{ $prod->id }}">
                                                    @if ($stk == '0')
                                                        <span class="hovertip addcart" rel-toggle="tooltip"
                                                            title="{{ $lang->hcs }}"><a class="btn btn-rose"
                                                                href="javascript:;" style="cursor: no-drop;"
                                                                title="Out Of Stock"><i
                                                                    class="material-icons">remove_shopping_cart</i> Out Of
                                                                Stock</a></span>
                                                    @else
                                                        <span class="hovertip addcart" rel-toggle="tooltip"
                                                            title="{{ $lang->hcs }}"><a
                                                                class="productDetails-addCart-btn btn btn-primary" id="addcrt"
                                                                href="javascript:;" style="cursor: pointer;"><i
                                                                    class="material-icons">shopping_cart</i> Add to
                                                                Cart</a></span>
                                                    @endif
                                                </div>


                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-md-12">
                                <div class="card bg-warning mt-0">
                                    <div class="card-body ">
                                        <div class="text-center">
                                            <h2 class="h1"><i class="fa fa-exclamation"></i> Sorry</h2>
                                            <p class="lead mb-5"> No product available or within selected price range !</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforelse


                    </div>

                    <div class="col-md-12 text-center">
                        @if (isset($min) || isset($max))
                            {!! $wproducts->appends(['min' => $min, 'max' => $max])->links('layouts.partials.pagination') !!}
                        @else
                            {!! $wproducts->links('layouts.partials.pagination') !!}
                        @endif

                    </div>

                </div>
            </div>

        </div>
    </div>

    <div>
        @include('includes.catalog-filter')
    </div>
@endsection

@section('scripts')
@if (isset($min) || isset($max))
    <script type="text/javascript">
        var min = $("#price-min").val();
        var max = $("#price-max").val();
        $('.sortbyradiobutton').change(function() {
            var sort = $('input[name=sortBy]:checked').val();
            window.location = "{{ url('/user/wishlists') }}/" + sort + "?min=" + min + "&max=" + max;
        });

    </script>
@else
    <script type="text/javascript">
        $('.sortbyradiobutton').change(function() {
            var sort = $('input[name=sortBy]:checked').val();
            window.location = "{{ url('/user/wishlists') }}/" + sort;
        });

    </script>
@endif



<script type="text/javascript">
    $("#ex2").slider({});
    $("#ex2").on("slide", function(slideRange) {
        var totals = slideRange.value;
        var value = totals.toString().split(',');
        $("#price-min").val(value[0]);
        $("#price-max").val(value[1]);
    });

</script>

<script type="text/javascript">
    $("#sortby").change(function() {
        var sort = $("#sortby").val();
        window.location = "{{ url('/user/wishlists') }}/" + sort;
    });

</script>

<script type="text/javascript">
    $('.removewish').click(function() {
        $(this).parent().parent().parent().parent().hide();
        var pid = $(this).parent().find('input[type=hidden]').val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/json/removewish') }}",
            data: {
                id: pid
            },
            success: function(data) {
                $.notify("{{ $gs->wish_remove }}", "success");
            }
        });
        return false;
    });

</script>

{{-- <script>
  $(document).on("click", ".addcart" , function(){
      var pid = $(this).parent().find('input[type=hidden]').val();
          $.ajax({
                  type: "GET",
                  url:"{{URL::to('/json/addcart')}}",
                  data:{id:pid},
                  success:function(data){
                      if(data == 0)
                      {
                          $.notify("{{$gs->cart_error}}","error");
                      }
                      else
                      {
                      $(".empty").html("");
                      $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                      $(".cart-quantity").html(data[2]);
                      var arr = $.map(data[1], function(el) {
                      return el });
                      $(".cart").html("");
                      for(var k in arr)
                      {
                          var x = arr[k]['item']['name'];
                          var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                          var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                          $(".cart").append(
                          '<div class="single-myCart">'+
          '<p class="cart-close" onclick="remove('+arr[k]['item']['id']+')"><i class="fa fa-close"></i></p>'+
                          '<div class="cart-img">'+
                  '<img src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                          '</div>'+
                          '<div class="cart-info">'+
      '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'" style="color: black; padding: 0 0;">'+'<h5>'+p+'</h5></a>'+
                      '<p>{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> '+measure+'</p>'+
                      @if ($gs->sign == 0)
                      '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                      @else
                      '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                      @endif
                      '</div>'+
                      '</div>');
                        }
                      $.notify("{{$gs->cart_success}}","success");
                      }
                  },
                  error: function(data){
                      if(data.responseJSON)
                      $.notify(data.responseJSON.error,"error");
                      else
                      $.notify('Something went wrong',"error");

                  }
            });
      return false;
  });



  </script> --}}
<script>
    $(document).on("click", ".removecart", function(e) {
        $(".addToMycart").show();
    });

</script>

<script src="{{ asset('frontend-assets/main-assets/assets/vendor/jquery.countdown.min.js') }}"></script>

<!-- JS Unify -->
<script src="{{ asset('frontend-assets/main-assets/assets/js/components/hs.countdown.js') }}"></script>

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function() {
        // initialization of countdowns
        var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
            yearsElSelector: '.js-cd-years',
            monthElSelector: '.js-cd-month',
            daysElSelector: '.js-cd-days',
            hoursElSelector: '.js-cd-hours',
            minutesElSelector: '.js-cd-minutes',
            secondsElSelector: '.js-cd-seconds'
        });
    });

</script>




@endsection
