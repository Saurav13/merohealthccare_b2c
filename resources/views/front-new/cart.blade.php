@extends('layouts.front1')
@section('title', 'My Cart')

@section('content')

    <div class="container">
        <button class="btn btn-info btn-round btn-raised" style="margin-top:20px;"><i
                class="material-icons">shopping_cart</i> Cart</button>

        <div class="row">
            <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                <div class="table-responsive" style="margin-top:25px;">
                    <table class="table table-shopping">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Name</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (Session::has('cart'))
                                @foreach ($products as $product)
                                    @php
                                        $p = App\Product::findOrFail($product['item']['id']);
                                        $prod = App\Product::findOrFail($product['item']['id']);
                                        $price = $product['item']['pprice'] ? $product['item']['pprice'] : $product['item']['cprice'];
                                    @endphp
                                    <tr id="del{{ $product['item']['id'] }}">
                                        <td>
                                            <div class="img-container" style="max-height:500px">
                                                <a href="{{ route('front.product', [$product['item']['id'], $product['item']['slug']]) }}"
                                                    style="font-weight: 500; font-size:15px;" target="_blank">
                                                    <img src="{{ asset('assets/images/' . $product['item']['photo']) }}"
                                                        alt="{{ $product['item']['name'] }}">
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ route('front.product', [$product['item']['id'], $product['item']['slug']]) }}"
                                                style="font-weight: 500; font-size:15px;" target="_blank">
                                                {{ ucwords(strtolower($product['item']['name'])) }}
                                                @if ($p->vat_status == 1)
                                                    <span style="font-style: italic;">*</span>
                                                @endif
                                            </a>
                                            <h6 style="text-transform:capitalize;">Varient : {{ $p->sub_title }}</h6>
                                            <h6>{{ $p->product_quantity }}</h6>


                                        </td>

                                        <input type="hidden" id="stock{{ $product['item']['id'] }}"
                                            value="{{ $product['stock'] }}">

                                        <td class="td-number text-center">

                                            @if ($product['item']['pprice'])
                                                <del style="font-size:14px;color:red">
                                                    @if ($gs->sign == 0)
                                                        {{ $curr->sign }}{{ round($price * $curr->value, 2) }}
                                                    @else
                                                        {{ round($price * $curr->value, 2) }}{{ $curr->sign }}
                                                    @endif
                                                </del>
                                            @endif
                                            <br>
                                            @if ($product['item']['cprice'])
                                                <span style="font-size:16px;">
                                                    {{ $curr->sign }}{{ $product['item']['cprice'] }}
                                                </span>
                                            @endif
                                        </td>
                                        <td class="td-number text-center">

                                            <div class="btn-group">
                                                <input type="hidden" value="{{ $product['item']['id'] }}">
                                                <button class="btn btn-round btn-info btn-sm quantity-btn reducing"
                                                    style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;">
                                                    <i class="material-icons">remove</i> </button>
                                                <span class="" style="font-size:15px;padding-left:5px;padding-right:5px;"
                                                    id="qty{{ $product['item']['id'] }}">{{ $product['qty'] }}</span>
                                                <button class="btn btn-round btn-info btn-sm quantity-btn adding"> <i
                                                        class="material-icons">add</i> </button>

                                            </div>
                                        </td>
                                        <td class="td-number text-center" style="font-size:16px;">
                                            @if ($gs->sign == 0)
                                                {{ $curr->sign }}<span
                                                    id="prc{{ $product['item']['id'] }}">{{ round($product['price'] * $curr->value, 2) }}</span>
                                                {{-- <span style="font-size:12px;color:red" id="reduced{{ $product['item']['id'] }}">
                            @if ($product['item']['cprice'] != $product['item']['price'])
                              <del>{{$curr->sign}}{{ round($price * $product['qty'] * $curr->value, 2) }}</del>
                            @endif
                          </span> --}}
                                            @else
                                                <span
                                                    id="prc{{ $product['item']['id'] }}">{{ round($product['price'] * $curr->value, 2) }}</span>{{ $curr->sign }}
                                                {{-- <span style="font-size:12px;color:red" id="reduced{{ $product['item']['id'] }}">
                            @if ($product['item']['cprice'] != $product['item']['price'])
                              <del>{{ round($product['item']['cprice'] * $product['qty'] * $curr->value, 2) }}{{$curr->sign}}</del>
                            @endif
                          </span> --}}
                                            @endif
                                        </td>
                                        <td class="td-actions">
                                            <button class="removeTest btn btn-rose" rel="tooltip" data-placement="right"
                                                title="" data-original-title="Remove item"
                                                onclick="remove({{ $product['item']['id'] }})">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                            @else
                                <tr>
                                    <td colspan="9">
                                        <div class="text-center mx-auto g-max-width-645 g-mb-30 g-mt-30">
                                            @include('includes.form-success')
                                            <h2
                                                class="g-color-primary g-font-weight-700 g-font-size-80 g-line-height-1 text-uppercase mb-3">
                                                <i class="icon-exclamation"></i>
                                            </h2>
                                            <h2 class="h2 g-color-black mb-4"><i class="fa fa-exclamation"
                                                    aria-hidden="true"></i> Empty Cart</h2>
                                            <p class="lead g-color-gray-dark-v4 mb-0"><i class="fa fa-plus"></i> Add Item in
                                                Cart</p>

                                        </div>
                                    </td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 ml-auto mr-auto">
                <div class="card card-pricing card-info bg-info">
                    <div class="card-body">

                        <button class="btn btn-white btn-block"><i class="material-icons">shopping_cart</i> Cart
                            Summary</button>
                        <h4 class="card-title h3 card-header card-header-info text-center"
                            style="margin-left:2px;margin-right:2px;margin-top:10px;">
                            @if (Session::has('cart'))
                                <strong style="font-weight:800;">{{ $curr->sign }}</strong><span
                                    class="total g-font-weight-800" id="grandtotal" style="font-size:40px;font-weight:800;">
                                    {{ round($totalPrice * $curr->value, 2) }}</span>
                            @else
                                <strong style="font-weight:800;">{{ $curr->sign }}</strong><span
                                    class="total g-font-weight-800" id="grandtotal" style="font-size:40px;font-weight:800;">
                                    0.00</span>

                            @endif
                        </h4>



                    </div>
                </div>
                <div style="margin-bottom:30px;">
                    <a href="{{ route('front.index') }}" class="btn btn-block btn-warning btn-raised "
                        style="text-decoration: none !important;" rel="tooltip" data-placement="bottom" title=""
                        data-original-title="Shop More Products">
                        <i class="material-icons">keyboard_arrow_left</i>Shop more
                    </a>
                    <a href="{{ route('front.checkout') }}" class="btn btn-block btn-success btn-raised "
                        style="text-decoration: none !important;" rel="tooltip" data-placement="bottom" title=""
                        data-original-title="Proceed to Checkout Page">
                        <i class="material-icons">shopping_cart</i> Checkout <i
                            class="material-icons">keyboard_arrow_right</i>
                    </a>
                </div>
            </div>

        </div>
    </div>


@endsection


@section('scripts')
    <script type="text/javascript">
        $(document).on("click", ".adding", function() {
            var pid = $(this).parent().find('input[type=hidden]').val();
            var stck = $("#stock" + pid).val();
            var qty = $("#qty" + pid).html();
            if (stck != "") {
                var stk = parseInt(stck);
                if (qty <= stk) {
                    qty++;
                    $("#qty" + pid).html(qty);
                }
            } else {
                qty++;
                $("#qty" + pid).html(qty);
            }
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/json/addbyone') }}",
                data: {
                    id: pid
                },
                success: function(data) {
                    if (data == 0) {
                        $.notify("{{ $gs->cart_error }}", "error");
                    } else {
                        $(".total").html((data[0] * {{ $curr->value }}).toFixed(2));
                        $(".cart-quantity").html(data[3]);
                        $("#cqty" + pid).val("1");
                        $("#prc" + pid).html((data[2] * {{ $curr->value }}).toFixed(2));
                        $("#prct" + pid).html((data[2] * {{ $curr->value }}).toFixed(2));
                        $("#cqt" + pid).html(data[1]);
                        $("#qty" + pid).html(data[1]);
                        if (data[2] == data[4]) $('#reduced' + id).html('')
                        else {
                            if ({{ $gs->sign == 0 }}) $('#reduced' + pid).html(
                                "<del>{{ $curr->sign }}" + (data[4] * data[1] *
                                    {{ $curr->value }}).toFixed(2) + "</del>");
                            else $('#reduced' + pid).html("<del>" + (data[4] * data[1] *
                                    {{ $curr->value }}).toFixed(2) +
                                "{{ $curr->sign }}</del>");
                        }
                    }
                },
                error: function(data) {
                    qty--;
                    $("#qty" + pid).html(qty);

                    if (data.responseJSON)
                        $.notify(data.responseJSON.error, "error");
                    else
                        $.notify('Something went wrong', "error");

                }
            });
        });

        $(document).on("click", ".reducing", function(e) {
            var id = $(this).parent().find('input[type=hidden]').val();
            var stck = $("#stock" + id).val();
            var qty = $("#qty" + id).html();
            qty--;
            if (qty < 1) {
                $("#qty" + id).html("1");
            } else {
                $("#qty" + id).html(qty);
                $.ajax({
                    type: "GET",
                    url: "{{ URL::to('/json/reducebyone') }}",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        $(".total").html((data[0] * {{ $curr->value }}).toFixed(2));
                        $(".cart-quantity").html(data[3]);
                        $("#cqty" + id).val("1");
                        $("#prc" + id).html((data[2] * {{ $curr->value }}).toFixed(2));
                        $("#prct" + id).html((data[2] * {{ $curr->value }}).toFixed(2));
                        $("#cqt" + id).html(data[1]);
                        $("#qty" + id).html(data[1]);
                        if (data[2] == data[4]) $('#reduced' + id).html('')
                        else {
                            if ({{ $gs->sign == 0 }}) $('#reduced' + id).html(
                                "<del>{{ $curr->sign }}" + (data[4] * data[1] *
                                    {{ $curr->value }}).toFixed(2) + "</del>");
                            else $('#reduced' + id).html("<del>" + (data[4] * data[1] *
                                    {{ $curr->value }}).toFixed(2) +
                                "{{ $curr->sign }}</del>");
                        }
                    },
                    error: function(data) {
                        qty++;
                        $("#qty" + id).html(qty);

                        if (data.responseJSON)
                            $.notify(data.responseJSON.error, "error");
                        else
                            $.notify('Something went wrong', "error");

                    }
                });
            }
        });

    </script>

    <script type="text/javascript">
        $(document).on("click", ".delcart", function() {
            $(this).parent().parent().hide();
        });

    </script>
    <script>
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    </script>
@endsection
