@extends('layouts.front1')
@section('title',$product->name)

@section('content')

@php
$i=1;
$j=1;
$now = Carbon\Carbon::now()->format('Y/m/d H:i');
$product->pprice = $product->pprice ? : $product->cprice;
$product->cprice = $product->getPrice(1);
$stk = (string)$product->stock;
@endphp


<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css'>

<style>
  span{
    font-family: 'Muli' !important;
}
</style>

  <div class="container">
        <nav aria-label="breadcrumb" role="navigation" >
            <ol class="breadcrumb" style="font-size:14px;">
            @if($product->requires_prescription)
            <li class="breadcrumb-item"> <a class="underline" href="{{route('front.index')}}">{{ucfirst(strtolower($lang->home))}}</a></li>
            <li class="breadcrumb-item"><a class="underline" href="javascript:;" style="cursor:default">{{$product->categories()->first()->cat_name}}</a></li>
            @if(count($product->subcategories) > 0)
            <li class="breadcrumb-item"><a class="underline" href="javascript:;" style="cursor:default">{{$product->subcategories()->first()->sub_name}}</a></li>
            @endif
            @if(count($product->childcategories) > 0)
            <li class="breadcrumb-item"><a class="underline" href="javascript:;" style="cursor:default">{{$product->childcategories()->first()->child_name}}</a></li>
            @endif
            <li class="breadcrumb-item"><a class="underline" style="color:#e91e63 !important;" href="{{route('front.product',['id1' => $product->id , 'id2' =>$product->slug])}}">{{$product->name}}</a></li>
            {{-- <span>{{$product->name}}</span> --}}
          @else
          <li class="breadcrumb-item"> <a class="underline" href="{{route('front.index')}}">{{ucfirst(strtolower($lang->home))}}</a></li>
          <li class="breadcrumb-item"> <a class="underline" href="{{route('front.category',$product->categories()->first()->cat_slug)}}">{{$product->categories()->first()->cat_name}}</a></li>
            @if(count($product->subcategories) > 0)
            <li class="breadcrumb-item"> <a class="underline" href="{{route('front.subcategory',$product->subcategories()->first()->sub_slug)}}">{{$product->subcategories()->first()->sub_name}}</a></li>
            @endif
            @if(count($product->childcategories) > 0)
            <li class="breadcrumb-item"><a class="underline" href="{{route('front.childcategory',$product->childcategories()->first()->child_slug)}}">{{$product->childcategories()->first()->child_name}}</a></li>
            @endif
            <li class="breadcrumb-item"> <a class="underline" style="color:#e91e63 !important;" href="{{route('front.product',['id1' => $product->id , 'id2' => $product->slug])}}">{{$product->name}}</a></li>
          @endif
            </ol>
            
        </nav>
  </div>


  <div class="container">
        <div class="row">
            <div class="col-md-6">
              <section id="detail">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12 ml-auto mr-auto">
                 <!-- Product Images & Alternates -->
                        <div class="product-images demo-gallery">
                          <!-- Begin Product Images Slider -->
                          <div class="main-img-slider">
                             <a data-fancybox="gallery" data-src="{{asset('assets/images/'.$product->photo)}}" data-caption="{{$product->name}}"><img src="{{asset('assets/images/'.$product->photo)}}" class="img-fluid"></a>
                             @foreach($product->galleries as $gallery)
                              <a data-fancybox="gallery" data-src="{{$gallery->photo ? asset('assets/images/gallery/'.$gallery->photo):asset('assets/images/default.png')}}" data-caption="Merohealthcare Product Gallery"><img src="{{$gallery->photo ? asset('assets/images/gallery/'.$gallery->photo):asset('assets/images/default.png')}}" class="img-fluid"></a>
                            @endforeach
                          </div>
                        <!-- End Product Images Slider -->
                      
                        <!-- Begin product thumb nav -->
                        <ul class="thumb-nav">
                          <li><img src="{{asset('assets/images/'.$product->photo)}}"></li>
                          @foreach($product->galleries as $gallery)
                            <li><img src="{{$gallery->photo ? asset('assets/images/gallery/'.$gallery->photo):asset('assets/images/default.png') }}"></li>
                          @endforeach
                        </ul>
                        <!-- End product thumb nav -->
                      </div>
                      <!-- End Product Images & Alternates -->
                      
                    </div>
                  </div>
                </div>
              </section>
              <div class="card">
                <form class="form" style="margin-bottom:1.25rem;">
                  <div class="card-header card-header-info text-center" style="padding:1px 15px;">
                    <h4 class="card-title">Want to know about this product ? </h4>
                    <div class="social-line" style="padding-top:0px ;">
                      <a href="{{route('user-askdoctor-index')}}" class="btn btn-rose btn-round" style="text-decoration:none !important;" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Ask your queries?">
                        <i class="material-icons">help</i> Ask a Doctor
                      </a>
                      {{-- <span>Product Views :</span> --}}
                      <a href="#" class="btn btn-success btn-round" style="text-decoration:none !important;" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Product Views">
                        <i class="material-icons">remove_red_eye</i> {{$product->views}}
                      </a>
                   
                    </div>
                  </div>
               
                </form>
              </div>

            </div>

            <div class="col-md-6">
              <h1 class="h4" style="font-size:25px;font-weight:700;">{{$product->name}}</h1>
              <p style="font-size:15px;">{{$product->company_name}}</p>

              @if($product['requires_prescription'])
              <span class="badge badge-pill badge-rose" style="font-size:13px;">Prescription Required</span>
              @endif

              @if($product->highlights != null || $product->highlights != " " )
                <h2 class="h5">Products Highlights</h2>
                <p style="font-size:14px;font-family:'Muli' !important; ">{!!$product->highlights !!}</p>
              @endif

       

              <h2 class="h5">Available Variants</h2>
              @foreach($similars as $similar)
                  @if($similar->id == $product->id)
                    <button class="btn btn-info" style="font-size:16px;padding:5px 10px;">{{$product->sub_title}}</button>
                  @else
                    <a href="{{ route('front.product',[$similar->id,$similar->slug]) }}" class="btn" style="font-size:16px;padding:5px 10px;">{{ $similar->sub_title }}</a>
                  @endif
                @endforeach

              <h2 class="h5">Price</h2>
              <p style="font-size:15px;">
                {{-- Rs. 560/ <span style="color: orangered">per piece</span> --}}
                @if($product->user_id != 0)
                @php
                $price = $product->cprice + $gs->fixed_commission + ($product->cprice/100) * $gs->percentage_commission ;
                @endphp
               <span style="font-size: 25px;color:#2385aa;font-weight:700;">{{round($price * $curr->value,2)}} </span><span>/ {{$product->product_quantity}}</span>
                @if($product->pprice != null && $product->pprice != 0  && $product->pprice > $product->cprice)
                  @php
                    $pprice = $product->pprice + $gs->fixed_commission + ($product->pprice/100) * $gs->percentage_commission ;
                  @endphp
                  <span style="display:inline; color:green;"><del style="color:red;">{{$curr->sign}}{{round($pprice * $curr->value,2)}} </del> -{{ $product->discount_percent }}% </span>
                @endif
              @else
                <span style="font-size:25px;color:#2385aa; font-weight:700;">{{round($product->cprice * $curr->value,2)}}</span> <span>/ {{$product->product_quantity}}</span>
                @if($product->pprice != null && $product->pprice != 0  && $product->pprice > $product->cprice)
              <span style="display:inline; color:green;"> <del style="color:red;">{{$curr->sign}}{{round($product->pprice * $curr->value,2)}}</del> -{{ $product->discount_percent }}%</span>
                @endif

              @endif
              </p>


              @if($product->sale_from && $product->sale_from <= $now && $product->sale_to >= $now)
              <h2 class="h5">Discount Valid Till</h2>
              <!--  Starting of countdown area   -->

              {{-- <div class="js-countdown u-countdown-v3 g-line-height-1_2 g-color-black text-uppercase" > --}}
                <div class="js-countdown u-countdown-v3 g-line-height-1_2 g-color-black text-uppercase" data-end-date="{{$product->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S" >
                <div class="d-inline-block g-mb-5">
                  <div class="js-cd-days g-font-size-16 mb-0" id="days" style="color:green;font-weight:400;font-size:22px;">00</div>
                  <h6 class="" style="color:green; border:0px;font-size:14px;">Days</h6>
                </div>

                <div class="hidden-down d-inline-block align-top g-font-size-20 g-mt-0">:</div>

                <div class="d-inline-block g-mx-10 g-mb-5">
                  <div class="js-cd-hours g-font-size-16 mb-0" id="hours" style="font-weight:400;font-size:22px;">00</div>
                  <h6 class="" style="font-size:14px;">Hours</h6>
                </div>

                <div class="hidden-down d-inline-block align-top g-font-size-20 g-mt-0">:</div>

                <div class="d-inline-block g-mx-10 g-mb-5">
                  <div class="js-cd-minutes g-font-size-16 mb-0" id="minutes" style="font-weight:400;font-size:22px;">00</div>
                  <h6 class="" style="font-size:14px;">Minutes</h6>
                </div>

                <div class="hidden-down d-inline-block align-top g-font-size-20 g-mt-0">:</div>

                <div class="d-inline-block g-mx-10 g-mb-5">
                  <div class="js-cd-seconds g-font-size-16 mb-0" id="seconds" style="color:red;font-weight:400;font-size:22px;">00</div>
                  <h6 class="" style="color:red;font-size:14px;">Seconds</h6>
                </div>
              </div>
                  <!--  Ending of countdown area   -->
              @endif
            

              <h2 class="h5">Quantity </h2> 
             
              <div style="display: flex;">


              <div class="btn-group">
                <button type="button" class="btn btn-round btn-info btn-xs btn-number" data-type="minus" data-field="quant[2]"> <i class="material-icons">remove</i> <div class="ripple-container"></div></button>
                <input id="myqty" type="number" step="1" name="quant[2]" class="form-control input-number" placeholder="Regular" style="color: #2385aa !important; width:50px; font-size:27px;font-weight:700;padding-top: 0px;" onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');" value="1" min="1" max="1000">
                <button type="button" class="btn btn-round btn-info btn-xs btn-number" data-type="plus" data-field="quant[2]"> <i class="material-icons">add</i> <div class="ripple-container"></div></button>
              </div>
             
              </div>
            

              @if($stk == "0")
              <a class="btn btn-rose" href="javascript:;" style="cursor: no-drop;padding: 12px 60px;" title="Add to cart"><i class="material-icons">remove_shopping_cart</i> Out Of Stock</a>
              @else
              <button class="btn btn-primary productDetails-addCart-btn btn-round" id="addcrt_single" href="javascript:;" style="cursor: pointer;border-radius:5px; padding: 12px 60px;" title="Add to cart" ><i class="material-icons">shopping_cart</i> Add To Cart</button>
              @endif
             

              <input type="hidden" id="pid" value="{{$product->id}}">

              @php
              $user = Auth::guard('user')->user();
              @endphp

              @if(Auth::guard('user')->check())
              <a class="btn btn-rose btn-fab btn-round" href="javascript:;" id="wish" style="cursor: pointer;">
                <i class="material-icons">favorite</i>
              <div class="ripple-container"></div></a>
              @else
              <a class="btn btn-rose btn-fab btn-round" href="{{route('user-login')}}" title="Add to wishlist"  rel="tooltip" title="" data-original-title="Add to wishlist">
                <i class="material-icons">favorite</i>
              <div class="ripple-container"></div></a>
              @endif
               @if($product->user_id != 0)
               <h4 class="h4"><b><i class="icon-puzzle"></i> Seller :</b> {{$product->user->name}}</h4>
               @endif

              @if($product->adv_price)
                @foreach($product->prices as $pro)
                @php
                $product->price = $product->getTotalPrice($pro->min_qty)
                @endphp
                <div class="card ">
                  <div class="card-body">
                    <h5 class="card-category card-category-social">
                      <i class="material-icons">view_comfy</i> Combo Packs
                    </h5>
                    <h5 class="card-title">
                    Pack of {{ $pro->min_qty }}
                    </h5>
      
                    <div class="card-stats">
                      <div class="author">
                            @if($gs->sign == 0)
                            <h3 class="card-title productDetails-price h4 text-primary">
                              {{$curr->sign}}
                              @if($product->user_id != 0)
                                @php
                                $price = $product->price + $gs->fixed_commission + ($product->price/100) * $gs->percentage_commission ;
                                @endphp
                                {{round($price * $curr->value,2)}}
                              @else
                                {{round($product->price * $curr->value,2)}}
                              @endif

                              @if($product->price != round($product->cprice*$pro->min_qty,2))
                                <span style="color:green;"><del style="color:red; font-size:14px;"> {{$curr->sign}}{{round($product->cprice *$pro->min_qty* $curr->value,2)}} </del></span>
                              @endif
                            </h3>
                          @else
                            <h3 class="card-title productDetails-price h4">
                              @if($product->user_id != 0)
                                @php
                                $price = $product->price + $gs->fixed_commission + ($product->price/100) * $gs->percentage_commission ;
                                @endphp
                                {{round($price * $curr->value,2)}}
                              @else
                                {{round($product->price * $curr->value,2)}}
                              @endif
                              {{$curr->sign}}

                              @if($product->price != round($product->cprice*$pro->min_qty,2))
                                <span style="color:green;"><del style="color:red;">{{round($product->cprice *$pro->min_qty * $curr->value,2)}}{{$curr->sign}}</del></span>
                              @endif
                            </h3>
                          @endif
                    
                     
                      </div>
                      <div class="stats ml-auto">
                        @if(!$product->requires_prescription)
                          @if($stk >= $pro->min_qty)
                            <button class="productDetails-addCart-btn btn btn-info" style="border-radius:30px;" onclick="addToCart({{ $pro->min_qty }})">Buy Pack</button>
                          @else
                            <button class="productDetails-addCart-btn btn btn-warning" style="border-radius:30px;" disabled>Unavailable</button>
                          @endif
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              @endif


            </div>


        </div>
        <hr>

        <div class="row">
          <div class="col-md-3">
            <ul class="nav nav-pills nav-pills-icons flex-column" role="tablist">
             
              <li class="nav-item">
                <a class="nav-link active show" href="#dashboard-2" role="tab" data-toggle="tab" aria-selected="true" style="text-decoration: none !important;">
                  <i class="material-icons">dashboard</i>
                  Full Description
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#schedule-2" role="tab" data-toggle="tab" aria-selected="false" style="text-decoration: none !important;">
                  <i class="material-icons">question_answer</i>
                  Review
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-8">
            <div class="tab-content">
              <div class="tab-pane active show" id="dashboard-2">
                <div class="full-description" {!! $lang->rtl == 1 ? 'dir="rtl"' : ''!!} style="font-family:'Muli' !important">{!! $product->description !!}</div>
               
              </div>
              <div class="tab-pane" id="schedule-2">
                <div>
                  @if(Auth::guard('user')->check())

                    @if(Auth::guard('user')->user()->orders()->count() > 0)
                      <h5>{{$lang->fpr}}</h5>
                      {{-- <hr> --}}
                      @include('includes.form-success')
                      {{-- <p class="product-reviews">
                          <div class="review-star">
                            <div class='starrr' id='star1'></div>
                              <div>
                                  <span class='your-choice-was' style='display: none;'>
                                    {{$lang->dofpl}}: <span class='choice'></span>.
                                  </span>
                              </div>
                          </div>
                      </p> --}}
                      <form class="product-review-form" action="{{route('front.review.submit')}}" method="POST">
                          {{ csrf_field() }}
                          <input type="hidden" name="user_id" value="{{Auth::guard('user')->user()->id}}">
                            <input type="hidden" name="rating" id="rate" value="5">
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            <div class="form-group">
                              <textarea name="review" id="" rows="5" placeholder="{{$lang->suf}}" class="form-control" style="resize: vertical;border-radius:10px;" required></textarea>
                            </div>
                        <div class="form-group text-center">
                          <input name="btn" type="submit" class="btn-review btn btn-primary btn-rounded" value="Submit Review">
                        </div>
                      </form>
                    @else
                      <h5>{{ $lang->product_review }}.</h5>
                    @endif

                      <h5>{{$lang->dttl}}: </h5>

                    @forelse($product->reviews as $review)
                      <div class="review-rating-description">
                        <div class="row">
                          <div class="col-md-3 col-sm-3">
                            <p>
                              @if($user->is_provider == 0)
                              <img style="position:relative;bottom:5px;width: 25px; height: 25px; border-radius:30px;" src="{{ $user->photo ? asset('assets/images/'.$user->photo) :asset('assets/images/user.png')}}" alt="profile no image"></i>
                          @else
                          <img style="position:relative;bottom:5px;width: 25px; height: 25px; border-radius:30px;" src="{{ $user->photo ? $user->photo :asset('assets/images/user.png')}}" alt="profile no image"></i>
                          @endif
                               {{$review->user->name}}</p>
                            {{-- <p class="product-reviews">
                              <div class="ratings">
                                <div class="empty-stars"></div>
                                <div class="full-stars" style="width:{{$review->rating*20}}%"></div>
                              </div>
                          </p> --}}
                            <em style="font-size:14px !important;"><i class="fa fa-clock-o"></i> {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $review->review_date)->diffForHumans()}}</em>
                          </div>
                          <div class="col-md-9 col-sm-9">
                            <p >{{$review->review}}</p>
                          </div>
                        </div>
                      </div>
                    @empty
                      <div class="row">
                          <div class="col-md-12">
                              <h5>{{$lang->md}}</h5>
                          </div>
                      </div>
                    @endforelse


                  @else


                      <div class="col-lg-12 pt-50">
                        <div class="blog-comments-area product">
                          <br/>
                          <h5 class="text-center">
                            <a href="{{route('user-login')}}"><i class="icon-user"></i> {{$lang->comment_login}}</a> {{ $lang->to_review }}
                           </h5>

                        </div>
                      </div>

                        <h5>{{$lang->dttl}}: </h5>

                      @forelse($product->reviews as $review)
                        <div class="review-rating-description">
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <p>{{$review->user->name}}</p>
                              {{-- <p class="product-reviews">
                                <div class="ratings">
                                  <div class="empty-stars"></div>
                                  <div class="full-stars" style="width:{{$review->rating*20}}%"></div>
                                </div>
                            </p> --}}
                              <p>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $review->review_date)->diffForHumans()}}</p>
                            </div>
                            <div class="col-md-9 col-sm-9">
                              <p>{{$review->review}}</p>
                            </div>
                          </div>
                        </div>
                      @empty
                        <div class="row">
                            <div class="col-md-12">
                                <h6>{{$lang->md}}</h6>
                            </div>
                        </div>
                      @endforelse
                
                  @endif

                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="sharethis-inline-share-buttons" style="margin-top:12px;">
              <style>
                #st-el-3 .st-btns {
                  bottom: 56px;
                  left: 0;
                  margin: 100px auto 0;
                  max-width: 90%;
                  position: absolute;
                  right: 0;
                  text-align: center;
                  top: 10px !important;
                  z-index: 20;
                  overflow-y: auto;
               
              }
              </style>
            </div>

            @php
            $offers = App\Offer::orderBy('id','desc')->get();
            @endphp
            <div class="card card-pricing">
              <div class="card-body">
                <h3 class="card-title"><i class="material-icons">star_border</i>Additional Offers</h3>
                <ul style="max-width:100%;">
                  @foreach($offers as $offer)
                  <li style="color:black;font-family:'Muli';font-size:15px;"><i class="material-icons text-success">check</i>  <strong>{{$offer->title}}</strong> {{$offer->description}}</li>
                  @endforeach
                </ul>
                <div class="rotating-card-container manual-flip" style="margin-bottom: 30px;">
                  <div class="card card-rotate bg-success">
                    <div class="front" style="width: 350px;">
                      <div class="card-body">
                        <h5 class="card-category card-category-social text-center" style="font-weight:700;">
                           Are you a <strong>BUSINESS</strong> or Want to buy in <strong>BULK</strong> ?
                        </h5>
                        
                      
                       
                        <div class="stats text-center">
                          <button type="button" name="button" class="btn btn-warning btn-fill btn-round btn-rotate">
                            <i class="material-icons">done_outline</i> Yes ...
                          <div class="ripple-container"></div></button>
                        </div>
                      </div>
                    </div>
                    <div class="back" style="width: 350px;">
                      <div class="card-body">
                        <br>
                        <h5 class="card-title">
                          Check out our B2B store
                        </h5>
                       
                        <div class="stats text-center">
                          <a href="https://business.merohealthcare.com/" target="__blank" class="btn btn-rose btn-round">
                            <i class="material-icons">subject</i> Business Merohealthcare
                          </a>
                     
                        </div>
                        <div class="stats text-center">
                        <button class="btn btn-warning btn-round btn-sm btn-rotate">
                          <i class="material-icons">refresh</i> Back...
                        <div class="ripple-container"></div></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             
            </div>
            @php
            $vendor_product = App\Product::where('name','=',$product->name)->where('status','=',1)->orderBy('cprice','asc')->get();
        // dd($vendor_product->count());
          @endphp
          

        </div>

        
        </div>
  </div>




    @php
    // $related = $product->childcategories()->first()->products()->where('status','=',1)->where('products.id','!=',$product->id)->distinct()->get();
    $related = collect();
    $title = $lang->amf;
    if($product->requires_prescription && $product->sub_title){
      $title = 'Alternate Brands';
      foreach($product->childcategories()->where('status',1)->get() as $cat){
        $related = $related->merge($cat->products()->where('status','=',1)->where('products.id','!=',$product->id)->where('products.sub_title',$product->sub_title)->distinct()->get());
      }

    }else{
      foreach($product->childcategories()->where('status',1)->get() as $cat){
        $related = $related->merge($cat->products()->where('status','=',1)->where('products.id','!=',$product->id)->distinct()->get());
      }
    }
    $related = $related->unique('id')->take(10);

    @endphp


    @if($related->count() > 0)
    <div class="section">
      <div class="">
      <div class="featured_product_div">
        <div class="featured_product_header">
            <h3 class="featured_product_title">{{ $title }}</h3>
        </div>
        <div class="featured_product_body" >
            <div class="owl-carousel">
              
                    @foreach($related as $prod)
                    @php
                        $name = str_replace(" ","-",$prod->name);
                    @endphp
                    @php
                        $prod->pprice = $prod->pprice ? : $prod->cprice;
                        $prod->cprice = $prod->getPrice(1);
                        $stk = (string)$prod->stock;
                    @endphp
                    <div class="item">
                        <div class="container">
                            @if($gs->sign == 0)
                                @if($prod->discount_percent != 0)
                                    <div class="discount_badge">
                                        <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span></p>
                                    </div>
                                @endif
                            @endif
                            @if($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                            <div class="offer_timer_div js-countdown" data-end-date="{{$prod->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
                                <div class="offer_subdiv_primary">
                                    <i class="material-icons">av_timer</i>
                                </div>
                                <div class="timer_alignment">
                                    <div class="js-cd-days offer_subdiv_white" style="color:rgb(214, 57, 57) !important;">
                                        <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                    </div>
                                    <span style="font-size:15px;">D</span>
                                </div>
                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-hours ">
                                        <p> <span>23</span>  <span class="offer_time_unit">H<span></p>
                                    </div>
                                    <span style="font-size:15px;">H</span>
                                </div>

                                <div class="timer_alignment">
                                    <div class="js-cd-minutes offer_subdiv_white">
                                        <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                    </div>
                                    <span style="font-size:15px;">M</span>
                                </div>

                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-seconds">
                                        <p> <span>02</span>  <span class="offer_time_unit">S</span></p>
                                    </div>
                                    <span style="font-size:15px;">S</span>
                                </div>
                            </div>
                            @endif
                            @if($prod->features!=null && $prod->colors!=null)
                            @php
                            $title = explode(',', $prod->features);
                            $details = explode(',', $prod->colors);
                            @endphp

                            @endif
                            <input type="hidden" value="{{$prod->id}}">
                            <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}">
                            <div class="featured_product_image_div hover-zoom">
                            <img class="w-100 card-product-image" src="{{asset('assets/images/'.$prod->photo)}}" alt="{{$prod->name}}" />

                            </div>
                            <div class="featured_product_detail_div">
                                <h4 class="featured_product_name">
                                        <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}" class="text-center">
                                                {{strlen($prod->name) > 38 ? substr($prod->name,0,38)."..." : ucwords(strtolower($prod->name))}}
                                        </a>
                                        </h4>
                                <p class="featured_product_subtitle">{{strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name,0,25))).'...' : ucwords(strtolower(substr($prod->company_name,0,25)))}}</p>
                                <p class="featured_product_price">{{$curr->sign}}
                                        {{round($prod->cprice * $curr->value,2)}}
                                        @if($prod->pprice != null && $prod->pprice != 0  && $prod->pprice > $prod->cprice)
                                            <span class="featured_product_price_old">{{$curr->sign}} {{round($prod->pprice * $curr->value,2)}}</span> <span>-{{ $prod->discount_percent }}%</span></p>

                                    @endif

                                    <div class="text-center">
                                        <input type="hidden" value="{{$prod->id}}">
                                        @if($stk == "0")
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="btn btn-rose" href="javascript:;" style="cursor: no-drop;" title="Out Of Stock"><i class="material-icons">remove_shopping_cart</i> Out Of Stock</a></span>
                                        @else
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="productDetails-addCart-btn btn btn-primary" id="addcrt" href="javascript:;" style="cursor: pointer;"><i class="material-icons">shopping_cart</i> Add to Cart</a></span>
                                        @endif
                                    </div>
                            </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
      </div>
      </div>
    </div>
  @endif



@endsection

@section('js')

<script>
  $(document).on('click', '.dropdown-menu', function (e) {
      e.stopPropagation();
  });
  $(document).ready(function () {
      $(".owl-carousel").owlCarousel({
          autoPlay: 3000,
          items: 7,
          itemsDesktop: [1199, 3],
          itemsDesktopSmall: [979, 3],
          center: true,
          nav: true,
          loop: true,
          responsive: {
              600: {
                  items: 1
              }

          }
      });
  });

</script>

{{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js'></script> --}}
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/popper.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js'></script>
<script>

  $(document).ready(function() {
    $("#flexiselDemo1").flexisel({
      visibleItems: 4,
      itemsToScroll: 1,
      animationSpeed: 400,
      enableResponsiveBreakpoints: true,
      responsiveBreakpoints: {
        portrait: {
          changePoint: 480,
          visibleItems: 3
        },
        landscape: {
          changePoint: 640,
          visibleItems: 3
        },
        tablet: {
          changePoint: 768,
          visibleItems: 3
        }
      }
    });
  });
</script>

<script>
  /*--------------*/



// Main/Product image slider for product page
$('#detail .main-img-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  arrows: true,
  fade:true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 300,
  lazyLoad: 'ondemand',
  asNavFor: '.thumb-nav',
  prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable">Previous</span></div>',
  nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">Next</span></div>'
});
// Thumbnail/alternates slider for product page
$('.thumb-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  centerPadding: '0px',
  asNavFor: '.main-img-slider',
  dots: false,
  centerMode: false,
  draggable: true,
  speed:200,
  focusOnSelect: true,
  prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable">Previous</span></div>',
  nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">Next</span></div>'  
});


//keeps thumbnails active when changing main image, via mouse/touch drag/swipe
$('.main-img-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
  //remove all active class
  $('.thumb-nav .slick-slide').removeClass('slick-current');
  //set active class for current slide
  $('.thumb-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');  
});
  </script>
{{-- @endsection


@section('scripts') --}}

{{-- @if($product->sale_from && $product->sale_from <= $now && $product->sale_to >= $now)
  <script type="text/javascript">
    function makeTimer() {

      var endTime = new Date("{{ $product->sale_to }}");
        endTime = (Date.parse(endTime) / 1000);

        var now = new Date();
        now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        if(timeLeft<0) return;

        var days = Math.floor(timeLeft / 86400);
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

        $("#days").html(days);
        $("#hours").html(hours);
        $("#minutes").html(minutes);
        $("#seconds").html(seconds);

    }

    setInterval(function() { makeTimer(); }, 1000);

  </script>
@endif --}}

<style type="text/css">
 img#imageDiv {
    height: 460px;
    width: 460px;
  }
  @media only screen and (max-width: 768px) {

  img#imageDiv {
      height: 280px;
      width: 280px;
    }

      }
  @media only screen and (max-width: 767px) {
  .product-review-carousel-img
  {
    max-width: 300px;
    margin: 30px auto;
  }
 img#imageDiv {
    height: 300px;
    width: 300px;
  }

    }
</style>

<script type="text/javascript">

  function productGallery(file){
    var image = $("#"+file).attr('src');
    $('#imageDiv').attr('src',image);
    $('.zoomImg').attr('src',image);
  }


    // var size = $(this).html();
    // $('#size').val(size);

    $('#star1').starrr({
        rating: 5,
        change: function(e, value){
            if (value) {
                $('.your-choice-was').show();
                $('.choice').text(value);
                $('#rate').val(value);
            } else {
                $('.your-choice-was').hide();
            }
        }
    });

</script>

<script type="text/javascript">
    var sizes = "";
    var colors = "";
    var stock = $("#stock").val();

  //   $(document).on("click", ".psize" , function(){
  //    $('.psize').removeClass('pselected-size');
  //    $(this).addClass('pselected-size');
  //    sizes = $(this).html();
  // });

    $(document).on("click", ".pcolor" , function(){
     $('.pcolor').removeClass('pselected-color');
     $(this).addClass('pselected-color');
     colors = $(this).html();
  });

    $(document).on("click", "#qsub" , function(){
         var qty = $("#qval").html();
         qty--;
         if(qty < 1)
         {
         $("#qval").html("1");
         }
         else{
         $("#qval").html(qty);
         }
    });
    $(document).on("click", "#qadd" , function(){
        var qty = $("#qval").html();
        if(stock != "")
        {
        var stk = parseInt(stock);
          if(qty < stk)
          {
             qty++;
             $("#qval").html(qty);
          }

        }
        else{
         qty++;
         $("#qval").html(qty);
        }

    });

    $(document).on("click", "#addcrt_single" , function(){
      var qty = document.getElementById("myqty").value;
      $(".empty").html("");
      addToCart(qty);

    });

    function addToCart(qty){
      var pid = $("#pid").val();

      $.ajax({
          type: "GET",
          url:"{{URL::to('/json/addnumcart')}}",
          data:{id:pid,qty:qty,size:sizes,color:colors},
          success:function(data){
              if(data == 0)
              {
                $.notify("{{$gs->cart_error}}","error");
              }
              else{
                $(".empty").html("");
                $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                $(".cart-quantity").html(data[2]);
                var arr = $.map(data[1], function(el) {
                return el
              });
              $(".cart").html("");
              for(var k in arr)
              {
                  var x = arr[k]['item']['name'];
                  var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                  var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                  $(".cart").append(
                                '<div class="single-myCart" style="margin: 10px !important;">'+
                                    '<div class=" g-brd-none g-px-20">'+
                                        '<div class="row no-gutters g-pb-5">'+
                                            '<div class="col-4 pr-3 cart_image_center">'+
                                                    '<img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                            '</div>'+
                                            '<div class="col-6">'+
                                                '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'">'+'<h6 class="card-title">'+p+'</h6></a>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> <span>'+measure+'</span></p>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">'+
                                                    @if($gs->sign == 0)
                                                    '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                                    @else
                                                    '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                                    @endif
                                                '</p>'+
                                            '</div>'+
                                                '<div class="col-2">'+
                                                '<button style="margin-top:10px;padding:12px;" type="button" class="cart-close btn btn-danger" onclick="remove('+arr[k]['item']['id']+')"><i class="material-icons">clear</i></button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');
                }
                $.notify("{{$gs->cart_success}}","success");
                $("#qval").html("1");
              }
          },
          error: function(data){
            if(data.responseJSON)
                $.notify(data.responseJSON.error,"error");
            else
              $.notify('Something went wrong',"error");

          }
      });
    }

</script>


    <script>
        $(document).on("click", "#wish" , function(){
            var pid = $("#pid").val();
            $.ajax({
                    type: "GET",
                    url:"{{URL::to('/json/wish')}}",
                    data:{id:pid},
                    success:function(data){
                        if(data == 1)
                        {
                            $.notify("{{$gs->wish_success}}","success");
                        }
                        else {
                            $.notify("{{$gs->wish_error}}","error");
                        }
                    },
                    error: function(data){
                      if(data.responseJSON)
                        $.notify(data.responseJSON.error,"error");
                      else
                        $.notify('Something went wrong',"error");

                    }
              });

            return false;
        });
    </script>
    <script>
        $(document).on("click", "#favorite" , function(){
          $("#favorite").hide();
            var pid = $("#fav").val();
            $.ajax({
                    type: "GET",
                    url:"{{URL::to('/json/favorite')}}",
                    data:{id:pid},
                    success:function(data){
                      $('.product-headerInfo__btns').html('<a class="headerInfo__btn colored"><i class="fa fa-check"></i> {{ $lang->product_favorite }}</a>');
                    },
                    error: function(data){
                      if(data.responseJSON)
                        $.notify(data.responseJSON.error,"error");
                      else
                        $.notify('Something went wrong',"error");

                    }
              });

        });
    </script>



<script type="text/javascript">
//*****************************COMMENT******************************
        $("#cmnt").submit(function(){
          var uid = $("#user_id").val();
          var pid = $("#product_id").val();
          var cmnt = $("#txtcmnt").val();
          $("#txtcmnt").prop('disabled', true);
          $('.btn blog-btn comments').prop('disabled', true);
     $.ajax({
            type: 'post',
            url: "{{URL::to('json/comment')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'uid'   : uid,
                'pid'   : pid,
                'cmnt'  : cmnt
                  },
            success: function(data) {
              $("#comments").prepend(
                    '<div id="comment'+data[3]+'">'+
                        '<div class="row single-blog-comments-wrap">'+
                            '<div class="col-lg-12">'+
                              '<h4><a class="comments-title">'+data[0]+'</a></h4>'+
                                '<div class="comments-reply-area">'+data[1]+'</div>'+
                                 '<p id="cmntttl'+data[3]+'">'+data[2]+'</p>'+
                                '<div class="replay-form">'+
                    '<p class="text-right"><input type="hidden" value="'+data[3]+'"><button class="replay-btn">{{$lang->reply_button}} <i class="fa fa-reply-all"></i></button><button class="replay-btn-edit">{{$lang->edit_button}} <i class="fa fa-edit"></i></button><button class="replay-btn-delete">{{$lang->remove}} <i class="fa fa-trash"></i></button>'+
                    '</p>'+'<form action="" method="POST" class="comment-edit">'+
                                      '{{csrf_field()}}'+
                                '<input type="hidden" name="comment_id" value="'+data[3]+'">'+
                                      '<div class="form-group">'+
                            '<textarea rows="2" id="editcmnt'+data[3]+'" name="text" class="form-control"'+
                            'placeholder="{{$lang->edit_comment}}" style="resize: vertical;" required=""></textarea>'+
                                      '</div>'+
                                      '<div class="form-group">'+
                    '<button type="submit" class="btn btn-no-border hvr-shutter-out-horizontal">{{$lang->update_comment}}</button>&nbsp;'+
                        '<button type="button" class="btn btn-no-border hvr-shutter-out-horizontal cancel">{{$lang->cancel_edit}}</button>'+
                                      '</div>'+
                                    '</form>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                      '</div>');
                    $("#comment"+data[3]).append('<div id="replies'+data[3]+'" style="display: none;"></div>');
                     $("#replies"+data[3]).append('<div class="rapper" style="display: none;"></div>');
                     $("#replies"+data[3]).append('<form action="" method="POST" class="reply" style="display: none;">'+
                      '{{csrf_field()}}'+
                      '<input type="hidden" name="comment_id" id="comment_id'+data[3]+'" value="'+data[3]+'">'+
                      '<input type="hidden" name="user_id" id="user_id'+data[4]+'" value="'+data[4]+'">'+
                        '<div class="form-group">'+
                          '<textarea rows="2" name="text" id="txtcmnt'+data[3]+'" class="form-control"'+ 'placeholder="{{$lang->write_reply}}" required="" style="resize: vertical;"></textarea>'+
                        '</div>'+
                      '<div class="form-group">'+
                        '<button type="submit" class="btn btn-no-border hvr-shutter-out-horizontal">{{$lang->reply_button}}</button>'+
                      '</div>'+'</form>');






                      //-----------Replay button details-----------
              if (data[5] > 1){
                $("#cmnt-text").html("{{ $lang->comments }}(<span id='cmnt_count'>"+ data[5]+"</span>)");
              }
              else{
                $("#cmnt-text").html("{{ $lang->comment }} (<span id='cmnt_count'>"+ data[5]+"</span>)");
              }
              $("#txtcmnt").prop('disabled', false);
              $("#txtcmnt").val("");
              $('.btn blog-btn comments').prop('disabled', false);
            },
            error: function(data){
              if(data.responseJSON)
                $.notify(data.responseJSON.error,"error");
              else
                $.notify('Something went wrong',"error");

            }
        });
          return false;
        });
//*****************************COMMENT ENDS******************************
</script>

<script type="text/javascript">

//***************************** REPLY TOGGLE******************************
          $(document).on("click", ".replay-form p button.view-replay-btn" , function(){
          var id = $(this).parent().next().find('input[name=comment_id]').val();
          $("#replies"+id+" .rapper").show();
          $("#replies"+id).show();
          });

          $(document).on("click", ".replay-form p button.replay-btn, .replay-form p button.subreplay-btn" , function(){
          var id = $(this).parent().find('input[type=hidden]').val();
          $("#replies"+id).show();
          $("#replies"+id).find('.reply').show();
          $("#replies"+id).find('.reply textarea').focus();
          });
//*****************************REPLY******************************
          $(document).on("submit", ".reply" , function(){
          var uid = $(this).find('input[name=user_id]').val();
          var cid = $(this).find('input[name=comment_id]').val();
          var rpl = $(this).find('textarea').val();
          $(this).find('textarea').prop('disabled', true);
          $('.btn btn-no-border hvr-shutter-out-horizontal').prop('disabled', true);
     $.ajax({
            type: 'post',
            url: "{{URL::to('json/reply')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'uid'   : uid,
                'cid'   : cid,
                'rpl'  : rpl
                  },
            success: function(data) {
              $("#replies"+cid).prepend('<div id="reply'+data[3]+'">'+
                        '<div class="row single-blog-comments-wrap replay">'+
                            '<div class="col-lg-12">'+
                              '<h4><a class="comments-title">'+data[0]+'</a></h4>'+
                                '<div class="comments-reply-area">'+data[1]+'</div>'+
                                 '<p id="rplttl'+data[3]+'">'+data[2]+'</p>'+
                                '<div class="replay-form">'+
                    '<p class="text-right"><input type="hidden" value="'+cid+'"><button class="subreplay-btn">{{$lang->reply_button}} <i class="fa fa-reply-all"></i></button><button class="replay-btn-edit1">{{$lang->edit_button}} <i class="fa fa-edit"></i></button><button class="replay-btn-delete1">{{$lang->remove}} <i class="fa fa-trash"></i></button></p>'+
                                    '<form action="" method="POST" class="reply-edit">'+
                                      '{{csrf_field()}}'+
                                  '<input type="hidden" name="reply_id" value="'+data[3]+'">'+
                                      '<div class="form-group">'+
                                    '<textarea rows="2" id="editrpl'+data[3]+'" name="text" class="form-control"'+ 'placeholder="{{$lang->edit_reply}}"  style="resize: vertical;" required=""></textarea>'+
                                      '</div>'+
                                      '<div class="form-group">'+
                                      '<button type="submit" class="btn btn-no-border hvr-shutter-out-horizontal">'+'{{$lang->update_comment}}</button>&nbsp;'+
                                      '<button type="button" class="btn btn-no-border hvr-shutter-out-horizontal cancel">{{$lang->cancel_edit}}</button>'+
                                      '</div>'+
                                    '</form>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '</div>');
                      //-----------REPLY button details-----------
              $("#txtcmnt"+cid).prop('disabled', false);
              $("#txtcmnt"+cid).val("");
              $('.btn btn-no-border hvr-shutter-out-horizontal').prop('disabled', false);
            },
            error: function(data){
              if(data.responseJSON)
                $.notify(data.responseJSON.error,"error");
              else
                $.notify('Something went wrong',"error");

            }
        });
          return false;
        });
//*****************************REPLY ENDS******************************

</script>



<script>

  $(document).on("click", ".replay-btn-edit" , function(){
          var id = $(this).parent().find('input[type=hidden]').val();
          var txt = $("#cmntttl"+id).html();
          $(this).parent().parent().parent().find('.comment-edit textarea').val(txt);
          $(this).parent().parent().parent().find('.comment-edit').toggle();
  });
  $(document).on("click", ".cancel" , function(){
          $(this).parent().parent().hide();
  });
  //*****************************SUB REPLY******************************
          $(document).on("submit", ".comment-edit" , function(){
          var cid = $(this).find('input[name=comment_id]').val();
          var text = $(this).find('textarea').val();
           $(this).find('textarea').prop('disabled', true);
          $('.hvr-shutter-out-horizontal').prop('disabled', true);
     $.ajax({
            type: 'post',
            url: "{{URL::to('json/comment/edit')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'cid'   : cid,
                'text'  : text
                  },
            success: function(data) {
              $("#cmntttl"+cid).html(data);
              $("#editcmnt"+cid).prop('disabled', false);
              $("#editcmnt"+cid).val("");
              $('.hvr-shutter-out-horizontal').prop('disabled', false);
            },
            error: function(data){
              if(data.responseJSON)
                $.notify(data.responseJSON.error,"error");
              else
                $.notify('Something went wrong',"error");

            }
        });
          return false;
        });

</script>

<script type="text/javascript">
  $(document).on("click", ".replay-btn-delete" , function(){
              var id = $(this).parent().next().find('input[name=comment_id]').val();
              $("#comment"+id).hide();
              var count = parseInt($("#cmnt_count").html());
              count--;
              if(count <= 1)
              {
              $("#cmnt-text").html("COMMENT (<span id='cmnt_count'>"+ count+"</span>)");
              }
              else
              {
              $("#cmnt-text").html("COMMENTS (<span id='cmnt_count'>"+ count+"</span>)");
              }
     $.ajax({
            type: 'get',
            url: "{{URL::to('json/comment/delete')}}",
            data: {'id': id}
        });
  });
</script>


<script type="text/javascript">
  $(document).on("click", ".replay-btn-edit1" , function(){
          var id = $(this).parent().parent().parent().find('.reply-edit input[name=reply_id]').val();
          var txt = $("#rplttl"+id).html();
          $(this).parent().parent().parent().find('.reply-edit textarea').val(txt);
          $(this).parent().parent().parent().find('.reply-edit').toggle();
          var txt = $("#cmntttl"+id).html();
  });

  //*****************************SUB REPLY******************************
          $(document).on("submit", ".reply-edit" , function(){
          var cid = $(this).find('input[name=reply_id]').val();
          var text = $(this).find('textarea').val();
           $(this).find('textarea').prop('disabled', true);
          $('.hvr-shutter-out-horizontal').prop('disabled', true);
     $.ajax({
            type: 'post',
            url: "{{URL::to('json/reply/edit')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'cid'   : cid,
                'text'  : text
                  },
            success: function(data) {
              $("#rplttl"+cid).html(data);
              $("#editrpl"+cid).prop('disabled', false);
              $("#editrpl"+cid).val("");
              $('.hvr-shutter-out-horizontal').prop('disabled', false);
            },
            error: function(data){
              if(data.responseJSON)
                $.notify(data.responseJSON.error,"error");
              else
                $.notify('Something went wrong',"error");

            }
        });
          return false;
        });

</script>

<script type="text/javascript">
  $(document).on("click", ".replay-btn-delete1" , function(){
              var id = $(this).parent().next().find('input[name=reply_id]').val();
              $("#reply"+id).hide();
     $.ajax({
            type: 'get',
            url: "{{URL::to('json/reply/delete')}}",
            data: {'id': id}
        });
  });
</script>

<script >
    $(document).on('ready', function () {
      // initialization of tabs
      $.HSCore.components.HSTabs.init('[role="tablist"]');
    });

    $(window).on('resize', function () {
      setTimeout(function () {
        $.HSCore.components.HSTabs.init('[role="tablist"]');
      }, 200);
    });
  </script>

  <script>
  $('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }


});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    </script>

<script  src="{{asset('frontend-assets/main-assets/assets/vendor/jquery.countdown.min.js')}}"></script>

<!-- JS Unify -->
<script  src="{{asset('frontend-assets/main-assets/assets/js/components/hs.countdown.js')}}"></script>

<!-- JS Plugins Init. -->
<script >
  $(document).on('ready', function () {
    // initialization of countdowns
    var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
      yearsElSelector: '.js-cd-years',
      monthElSelector: '.js-cd-month',
      daysElSelector: '.js-cd-days',
      hoursElSelector: '.js-cd-hours',
      minutesElSelector: '.js-cd-minutes',
      secondsElSelector: '.js-cd-seconds'
    });
  });
</script>

<script type="text/javascript">
  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  function IsNumeric(e) {
      var keyCode = e.which ? e.which : e.keyCode
      var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
      document.getElementById("error").style.display = ret ? "none" : "inline";
      return ret;
  }
</script>

<script type="text/javascript">
  function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script src="jquery.js"></script>
<script src="jssocials.min.js"></script>
<script>
    $("#shareRoundIcons").jsSocials({
      showLabel: false,
      showCount: false,
        shares: ["facebook", "whatsapp", "twitter", "linkedin", "email"]
    });
</script>

<script>
$(function() {

  var native_width = 0;
  var native_height = 0;
  var mouse = {x: 0, y: 0};
  var magnify;
  var cur_img;

  var ui = {
    magniflier: $('.magniflier')
  };

  // Add the magnifying glass
  if (ui.magniflier.length) {
    var div = document.createElement('div');
    div.setAttribute('class', 'glass');
    ui.glass = $(div);

    $('body').append(div);
  }


  // All the magnifying will happen on "mousemove"

  var mouseMove = function(e) {
    var $el = $(this);

    // Container offset relative to document
    var magnify_offset = cur_img.offset();

    // Mouse position relative to container
    // pageX/pageY - container's offsetLeft/offetTop
    mouse.x = e.pageX - magnify_offset.left;
    mouse.y = e.pageY - magnify_offset.top;

    // The Magnifying glass should only show up when the mouse is inside
    // It is important to note that attaching mouseout and then hiding
    // the glass wont work cuz mouse will never be out due to the glass
    // being inside the parent and having a higher z-index (positioned above)
    if (
      mouse.x < cur_img.width() &&
      mouse.y < cur_img.height() &&
      mouse.x > 0 &&
      mouse.y > 0
      ) {

      magnify(e);
    }
    else {
      ui.glass.fadeOut(100);
    }

    return;
  };

  var magnify = function(e) {

    // The background position of div.glass will be
    // changed according to the position
    // of the mouse over the img.magniflier
    //
    // So we will get the ratio of the pixel
    // under the mouse with respect
    // to the image and use that to position the
    // large image inside the magnifying glass

    var rx = Math.round(mouse.x/cur_img.width()*native_width - ui.glass.width()/2)*-1;
    var ry = Math.round(mouse.y/cur_img.height()*native_height - ui.glass.height()/2)*-1;
    var bg_pos = rx + "px " + ry + "px";

    // Calculate pos for magnifying glass
    //
    // Easy Logic: Deduct half of width/height
    // from mouse pos.

    // var glass_left = mouse.x - ui.glass.width() / 2;
    // var glass_top  = mouse.y - ui.glass.height() / 2;
    var glass_left = e.pageX - ui.glass.width() / 2;
    var glass_top  = e.pageY - ui.glass.height() / 2;
    //console.log(glass_left, glass_top, bg_pos)
    // Now, if you hover on the image, you should
    // see the magnifying glass in action
    ui.glass.css({
      left: glass_left,
      top: glass_top,
      backgroundPosition: bg_pos
    });

    return;
  };

  $('.magniflier').on('mousemove', function() {
    ui.glass.fadeIn(200);

    cur_img = $(this);

    var large_img_loaded = cur_img.data('large-img-loaded');
    var src = cur_img.data('large') || cur_img.attr('src');

    // Set large-img-loaded to true
    // cur_img.data('large-img-loaded', true)

    if (src) {
      ui.glass.css({
        'background-image': 'url(' + src + ')',
        'background-repeat': 'no-repeat'
      });
    }

    // When the user hovers on the image, the script will first calculate
    // the native dimensions if they don't exist. Only after the native dimensions
    // are available, the script will show the zoomed version.
    //if(!native_width && !native_height) {

      if (!cur_img.data('native_width')) {
        // This will create a new image object with the same image as that in .small
        // We cannot directly get the dimensions from .small because of the
        // width specified to 200px in the html. To get the actual dimensions we have
        // created this image object.
        var image_object = new Image();

        image_object.onload = function() {
          // This code is wrapped in the .load function which is important.
          // width and height of the object would return 0 if accessed before
          // the image gets loaded.
          native_width = image_object.width;
          native_height = image_object.height;

          cur_img.data('native_width', native_width);
          cur_img.data('native_height', native_height);

          //console.log(native_width, native_height);

          mouseMove.apply(this, arguments);

          ui.glass.on('mousemove', mouseMove);
        };


        image_object.src = src;

        return;
      } else {

        native_width = cur_img.data('native_width');
        native_height = cur_img.data('native_height');
      }
    //}
    //console.log(native_width, native_height);

    mouseMove.apply(this, arguments);

    ui.glass.on('mousemove', mouseMove);
  });

  ui.glass.on('mouseout', function() {
    ui.glass.off('mousemove', mouseMove);
  });

});
</script>

<script type="text/javascript">
  $(".ss").keyup(function() {
     var search = $(this).val();
     if(search == ""){
         $(".header-searched-item-list-wrap-mobile").hide();
     }
     else {
      // console.log("hello1st");
         $.ajax({
                 type: "GET",
                 url:"{{URL::to('/json/mobilesuggest')}}",
                 data:{search:search},
                 success:function(data){
                     if(!$.isEmptyObject(data))
                     {
                         $(".header-searched-item-list-wrap-mobile").show();
                         $(".header-searched-item-list-wrap-mobile ul").html("");
                         // var arr = $.map(data, function(el) {
                         //     return el
                         // });
                         // for(var k in arr)
                         // {
                         //     var x = arr[k]['name'];
                         //     var p = x.length  > 50 ? x.substring(0,50)+'...' : x;

                         //     $(".header-searched-item-list-wrap ul").append('<li><a href="{{url('/')}}/product/'+arr[k]['id']+'/'+arr[k]['name']+'">'+p+'</a></li>');
                         // }
                        //  console.log(data);
                         $(".header-searched-item-list-wrap-mobile ul").append(data.html);

                     }
                     else{
                         $(".header-searched-item-list-wrap-mobile").hide();
                        //  console.log('unsucess');
                     }
                 }
               })

     }
  });
 </script>

 <script>
 $(".toggle_btn").click(function(){
  $(this).toggleClass("active");
 $(".wrapper ul").toggleClass("active");

 if($(".toggle_btn").hasClass("active")){
   $(".toggle_text").text("less");
 }
 else{
   $(".toggle_text").text("... more");
 }
});
</script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>




@endsection
