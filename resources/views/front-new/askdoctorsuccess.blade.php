@extends('layouts.front1')
@section('title', 'Ask Doctor')
@section('content')

    <div class="container" style="margin-bottom: 30px;">
        <div class="card card-pricing">
            <div class="card-body ">
                <div class="icon icon-info">
                    <i class="material-icons">done_outline</i>
                </div>
                <h3 class="card-title">Success !</h3>
                <p class="card-description">
                    Your queries have been sent. Check your mail box eventually to for reply. Thank you.
                </p>
                <a href="/" class="btn btn-info btn-round"><i class="material-icons">home</i> Home</a>
            </div>
        </div>
    </div>

@endsection
