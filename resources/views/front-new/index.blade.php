@extends('layouts.front1')
@section('title','Online Pharmacy in Nepal | Buy Medicine and Healthcare Products')
@section('content')

<style>
 /* .featured_product_image_div>img{
    border: 2px solid #d9d9d9;
    border-radius: 7px;
 } */
 .featured_product_body{
 }
 .featured_product_div{
    margin: 1rem 8%;

 }
 .featured_product_body .container{
     padding-top: 1rem;
     padding-bottom: 1rem;
     border: 2px solid #d9d9d9;
    border-radius: 7px;
    /* margin: 0 10px; */
 }
 .featured_product_title{
     text-align: left;
     font-size:25px;
 }
 .item{
    margin-right: 12px;
 }
</style>
    
@php
$i=1;
$j=1;
$now = Carbon\Carbon::now()->format('Y/m/d H:i');
// dd($now);
@endphp

@if($gs->slider == 1 && count($sliders) > 0)

    <div class="">
        <div id="carouselIndicators" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="5000">
            @php
                $i=0;
            @endphp
            <ol class="carousel-indicators">
                @foreach($sliders as $sld)
                    <li data-target="#carouselIndicators" data-slide-to="{{$i++}}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach($sliders as $slider)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <div style="height:22rem;background:url({{ asset('assets/images/'.$slider->photo) }});background-position: center;background-size: cover">
                            <a style="position: absolute; top: 29px;
                                left: 50%;
                                -ms-transform: translate(-50%, -50%);
                                transform: translate(-50%, -50%);" 
                                href="{{ $slider->action_link }}" class="btn btn-rose btn-round btn-md">{{ $slider->action_name }}</a>
                            
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@else
    <div style="background-image: linear-gradient(to top, #fff1eb 0%, #ace0f9 100%);">
        <div class="container">
            <div class="row" style="height: 100%">
                <div class="col-md-6">
                    <div class="container main_section_right">
                    
                        <h3 class="main_section_title r_sm_centered"><span style="color: #043892;">Mero</span><span
                                style="color: #db153b;">health</span><span style="color: #2385aa;">care</span></h3>
                        <h1 class="main_section_subtitle r_sm_centered">My Healthcare Partner</h1>
                        <h2 class="instructions_title">We are now available at 
                        </h2>
                        <div>
                            <a href="https://play.google.com/store/apps/details?id=com.merohealth&hl=en" target="__blank">
                                <img src="playstore.png" style="height:47px;width:145px;margin-bottom:5px;" />
                            </a>
                            <a href="https://apps.apple.com/us/app/mero-health-care/id1544033535" target="__blank">
                                <img src="appstore.jpg" style="height:47px;width:145px;margin-left:3px;margin-bottom:5px;" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="container main_section_right mb-4" >
                        <h2 class="instructions_title ">How to buy medicines? </h2>
                        <div class="instructions_div">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card mt-0">
                                        <div class="steps_inner_div">
                                            <div class="step_icon">
                                                <span class="material-icons">search</span>
                                            </div>
                                            <div class="step_text_div">
                                                <h3 class="step_title">Step 1.</h3>
                                                <p class="step_desc">Search by medicine or company name.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card mt-0">
                                        <div class="steps_inner_div">
                                            <div class="step_icon">
                                                <span class="material-icons-outlined">medical_services</span>
                                            </div>
                                            <div class="step_text_div">
                                                <h3 class="step_title">Step 2.</h3>
                                                <p class="step_desc">Choose your medicine.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card mt-0">
                                        <div class="steps_inner_div">
                                            <div class="step_icon">
                                                <span class="material-icons">add_shopping_cart</span>
                                            </div>
                                            <div class="step_text_div">
                                                <h3 class="step_title">Step 3.</h3>
                                                <p class="step_desc">Add medicines to the cart.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card mt-0">
                                        <div class="steps_inner_div">
                                            <div class="step_icon">
                                                <span class="material-icons-outlined"> local_shipping</span>
                                            </div>
                                            <div class="step_text_div">
                                                <h3 class="step_title">Step 4.</h3>
                                                <p class="step_desc">Checkout and confirm your order.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endif

<div class="main">
    <div id="accordion" role="tablist">
        <div class="card card-collapse" style="background-image: linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%);">
            <div class="card-header pt-0 offer_div" role="tab" id="headingOne" style="background-image: linear-gradient(-225deg, #fef0eb 0%, #fef1ea 100%) !important">
                <h5 class="">
                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        GET <span style="color:red">5%</span> ADDITIONAL DISCOUNT <span
                            style="color:#ca4243">(View)</span>
                        <i class="material-icons">keyboard_arrow_down</i>
                    </a>
                </h5>
            </div>
            <div id="collapseOne" class="collapse " role="tabpanel" aria-labelledby="headingOne"
                data-parent="#accordion">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 align-self-center g-py-20">
                                <div class="banner">
                                    <div class="line">
                                        <span>GET <b style="color: red;">5%</b></span>
                                    </div>
                                    <div class="line">
                                        <span>ADDITIONAL</span>
                                    </div>
                                    <div class="line">
                                        <span>DISCOUNT</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 align-self-center g-py-20">

                                <div class="container">
                                    <div class="progress-steps">
                                        <div class="complete">
                                            <h3>
                                                <span class="number">1</span>
                                                Sign up
                                            </h3>
                                            Register with us
                                        </div>
                                        <div class="complete">
                                            <h3>
                                                <span class="number">2</span>
                                                Add Product in Cart
                                            </h3>
                                            Start Shopping
                                        </div>
                                        <div class="complete">
                                            <h3>
                                                <span class="number">3</span>
                                                Checkout
                                            </h3>
                                            Proceed Checkout
                                        </div>
                                        <div class="complete">
                                            <h3>
                                                <span class="number">4</span>
                                                Apply Coupon Code (MERO)
                                            </h3>
                                            Get discount on your first purchase
                                        </div>
                                        <div class="complete" style="margin-bottom:25px;">
                                            <h3>
                                                <span class="number">5</span>
                                                Get Your discount
                                            </h3>

                                            <a href="https://www.merohealthcare.com/discount-offers"
                                                class="btn btn-md btn-rose g-mr-10 g-mb-15">
                                                <span class="u-btn-skew__inner">Read More</span>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @php
    $now = Carbon\Carbon::now()->format('Y/m/d H:i');
    @endphp

    

    <div class="featured_product_div">
        <div class="featured_product_header">
            <h3 class="featured_product_title">Featured</h3>
        </div>
        <div class="featured_product_body">
            <div class="owl-carousel">
            @foreach($fproducts as $prod)
                @php
                    $name = str_replace(" ","-",$prod->name);
                    $stk = (string)$prod->stock;
          
                @endphp
                @php
                    $prod->pprice = $prod->pprice ? : $prod->cprice;
                    $prod->cprice = $prod->getPrice(1);
                @endphp
                <div class="item">
                    <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}">
                    <div class="container">
                        @if($gs->sign == 0)
                            @if($prod->discount_percent != 0)
                                <div class="discount_badge">
                                    <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span></p>
                                </div>
                            @endif
                        @endif
                        
                        @if($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                        <div class="offer_timer_div js-countdown" data-end-date="{{$prod->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
                            <div class="offer_subdiv_primary">
                                <i class="material-icons">av_timer</i>
                            </div>
                            <div class="timer_alignment">
                                <div class="js-cd-days offer_subdiv_white" style="color:rgb(214, 57, 57) !important;">
                                    <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                </div>
                                <span style="font-size:15px;">D</span>
                            </div>
                            <div class="timer_alignment offer_subdiv_primary">
                                <div class="js-cd-hours ">
                                    <p> <span>23</span>  <span class="offer_time_unit">H<span></p>
                                </div>
                                <span style="font-size:15px;">H</span>
                            </div>

                            <div class="timer_alignment">
                                <div class="js-cd-minutes offer_subdiv_white">
                                    <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                </div>
                                <span style="font-size:15px;">M</span>
                            </div>

                            <div class="timer_alignment offer_subdiv_primary">
                                <div class="js-cd-seconds">
                                    <p> <span>02</span>  <span class="offer_time_unit">S</span></p>
                                </div>
                                <span style="font-size:15px;">S</span>
                            </div>
                        </div>
                        @endif
                        @if($prod->features!=null && $prod->colors!=null)
                        @php
                        $title = explode(',', $prod->features);
                        $details = explode(',', $prod->colors);
                        @endphp

                        @endif
                        <input type="hidden" value="{{$prod->id}}">
                        
                            
                        <div class="featured_product_image_div hover-zoom" >
                            <img class="card-product-image" src="{{asset('assets/images/'.$prod->photo)}}" alt="{{$prod->name}}" />
                        
                        </div>
        
                        <div class="featured_product_detail_div">
                            <h4 class="featured_product_name">
                                    <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}" class="text-center">
                                            {{strlen($prod->name) > 38 ? substr($prod->name,0,38)."..." : ucwords(strtolower($prod->name))}}
                                    </a>
                                    </h4>
                            <p class="featured_product_subtitle">{{strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name,0,25))).'...' : ucwords(strtolower(substr($prod->company_name,0,25)))}}</p>
                            <p class="featured_product_price">{{$curr->sign}}
                                    {{round($prod->cprice * $curr->value,2)}}
                                    @if($prod->pprice != null && $prod->pprice != 0  && $prod->pprice > $prod->cprice)
                                        <span class="featured_product_price_old">{{$curr->sign}} {{round($prod->pprice * $curr->value,2)}}</span> <span>-{{ $prod->discount_percent }}%</span></p>

                                @endif
                            <div class="text-center">
                                <input type="hidden" value="{{$prod->id}}">
                                @if($stk == "0")
                                <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="btn btn-rose" href="javascript:;" style="cursor: no-drop;" title="Out Of Stock"><i class="material-icons">remove_shopping_cart</i> Out Of Stock</a></span>
                                @else
                                <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="productDetails-addCart-btn btn btn-primary" id="addcrt" href="javascript:;" style="cursor: pointer;"><i class="material-icons">shopping_cart</i> Add to Cart</a></span>
                                @endif
                            </div>

                        </div>
                        </a>
                    </div>
                </div>
            @endforeach

            </div>
        </div>
    </div>

    <div class="featured_product_div">
        <div class="featured_product_header">
            <h3 class="featured_product_title">Best Seller</h3>
        </div>
        <div class="featured_product_body" >
            <div class="owl-carousel">
                @foreach($beproducts as $prod)
                    @php
                        $name = str_replace(" ","-",$prod->name);
                        $stk = (string)$prod->stock;
                    @endphp
                    @php
                        $prod->pprice = $prod->pprice ? : $prod->cprice;
                        $prod->cprice = $prod->getPrice(1);
                    @endphp
                    <div class="item">
                        <div class="container">
                            @if($gs->sign == 0)
                                @if($prod->discount_percent != 0)
                                    <div class="discount_badge">
                                        <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span></p>
                                    </div>
                                @endif
                            @endif
                            @if($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                            <div class="offer_timer_div js-countdown" data-end-date="{{$prod->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
                                <div class="offer_subdiv_primary">
                                    <i class="material-icons">av_timer</i>
                                </div>
                                <div class="timer_alignment">
                                    <div class="js-cd-days offer_subdiv_white" style="color:rgb(214, 57, 57) !important;">
                                        <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                    </div>
                                    <span style="font-size:15px;">D</span>
                                </div>
                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-hours ">
                                        <p> <span>23</span>  <span class="offer_time_unit">H<span></p>
                                    </div>
                                    <span style="font-size:15px;">H</span>
                                </div>

                                <div class="timer_alignment">
                                    <div class="js-cd-minutes offer_subdiv_white">
                                        <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                    </div>
                                    <span style="font-size:15px;">M</span>
                                </div>

                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-seconds">
                                        <p> <span>02</span>  <span class="offer_time_unit">S</span></p>
                                    </div>
                                    <span style="font-size:15px;">S</span>
                                </div>
                            </div>
                            @endif
                            @if($prod->features!=null && $prod->colors!=null)
                            @php
                            $title = explode(',', $prod->features);
                            $details = explode(',', $prod->colors);
                            @endphp

                            @endif
                            <input type="hidden" value="{{$prod->id}}">
                            <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}">
                            <div class="featured_product_image_div hover-zoom">
                            <img class="card-product-image" src="{{asset('assets/images/'.$prod->photo)}}" alt="{{$prod->name}}" />

                            </div>
                            <div class="featured_product_detail_div">
                                <h4 class="featured_product_name">
                                        <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}" class="text-center">
                                                {{strlen($prod->name) > 38 ? substr($prod->name,0,38)."..." : ucwords(strtolower($prod->name))}}
                                        </a>
                                        </h4>
                                <p class="featured_product_subtitle">{{strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name,0,25))).'...' : ucwords(strtolower(substr($prod->company_name,0,25)))}}</p>
                                <p class="featured_product_price">{{$curr->sign}}
                                        {{round($prod->cprice * $curr->value,2)}}
                                        @if($prod->pprice != null && $prod->pprice != 0  && $prod->pprice > $prod->cprice)
                                            <span class="featured_product_price_old">{{$curr->sign}} {{round($prod->pprice * $curr->value,2)}}</span> <span>-{{ $prod->discount_percent }}%</span></p>

                                    @endif

                                    <div class="text-center">
                                        <input type="hidden" value="{{$prod->id}}">
                                        @if($stk == "0")
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="btn btn-rose" href="javascript:;" style="cursor: no-drop;" title="Out Of Stock"><i class="material-icons">remove_shopping_cart</i> Out Of Stock</a></span>
                                        @else
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="productDetails-addCart-btn btn btn-primary" id="addcrt" href="javascript:;" style="cursor: pointer;"><i class="material-icons">shopping_cart</i> Add to Cart</a></span>
                                        @endif
                                    </div>
                            </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    <div class="featured_product_div">
        <div class="featured_product_header">
            <h3 class="featured_product_title">Top Rated</h3>
        </div>
        <div class="featured_product_body">
            <div class="owl-carousel">
                    @foreach($tproducts as $prod)
                    @php
                        $name = str_replace(" ","-",$prod->name);
                        $stk = (string)$prod->stock;
                    @endphp
                    @php
                        $prod->pprice = $prod->pprice ? : $prod->cprice;
                        $prod->cprice = $prod->getPrice(1);
                    @endphp
                    <div class="item">
                        <div class="container">
                            @if($gs->sign == 0)
                                @if($prod->discount_percent != 0)
                                    <div class="discount_badge">
                                        <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span></p>
                                    </div>
                                @endif
                            @endif
                            @if($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                            <div class="offer_timer_div js-countdown" data-end-date="{{$prod->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
                                <div class="offer_subdiv_primary">
                                    <i class="material-icons">av_timer</i>
                                </div>
                                <div class="timer_alignment">
                                    <div class="js-cd-days offer_subdiv_white" style="color:rgb(214, 57, 57) !important;">
                                        <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                    </div>
                                    <span style="font-size:15px;">D</span>
                                </div>
                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-hours ">
                                        <p> <span>23</span>  <span class="offer_time_unit">H<span></p>
                                    </div>
                                    <span style="font-size:15px;">H</span>
                                </div>

                                <div class="timer_alignment">
                                    <div class="js-cd-minutes offer_subdiv_white">
                                        <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                    </div>
                                    <span style="font-size:15px;">M</span>
                                </div>

                                <div class="timer_alignment offer_subdiv_primary">
                                    <div class="js-cd-seconds">
                                        <p> <span>02</span>  <span class="offer_time_unit">S</span></p>
                                    </div>
                                    <span style="font-size:15px;">S</span>
                                </div>
                            </div>
                            @endif
                            @if($prod->features!=null && $prod->colors!=null)
                            @php
                            $title = explode(',', $prod->features);
                            $details = explode(',', $prod->colors);
                            @endphp

                            @endif
                            <input type="hidden" value="{{$prod->id}}">
                            <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}">
                            <div class="featured_product_image_div hover-zoom">
                                <img class="card-product-image" src="{{asset('assets/images/'.$prod->photo)}}" alt="{{$prod->name}}" />

                            </div>
                            <div class="featured_product_detail_div">
                                <h4 class="featured_product_name">
                                        <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}" class="text-center">
                                                {{strlen($prod->name) > 38 ? substr($prod->name,0,38)."..." : ucwords(strtolower($prod->name))}}
                                        </a>
                                        </h4>
                                <p class="featured_product_subtitle">{{strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name,0,25))).'...' : ucwords(strtolower(substr($prod->company_name,0,25)))}}</p>
                                <p class="featured_product_price">{{$curr->sign}}
                                        {{round($prod->cprice * $curr->value,2)}}
                                        @if($prod->pprice != null && $prod->pprice != 0  && $prod->pprice > $prod->cprice)
                                            <span class="featured_product_price_old">{{$curr->sign}} {{round($prod->pprice * $curr->value,2)}}</span> <span>-{{ $prod->discount_percent }}%</span></p>

                                    @endif
                                    <div class="text-center">
                                        <input type="hidden" value="{{$prod->id}}">
                                        @if($stk == "0")
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="btn btn-rose" href="javascript:;" style="cursor: no-drop;" title="Out Of Stock"><i class="material-icons">remove_shopping_cart</i> Out Of Stock</a></span>
                                        @else
                                        <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="productDetails-addCart-btn btn btn-primary" id="addcrt" href="javascript:;" style="cursor: pointer;"><i class="material-icons">shopping_cart</i> Add to Cart</a></span>
                                        @endif
                                      
                                    </div>

                            </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    {{-- <div class="featured_product_div">
        <div class="featured_product_header">
            <h3 class="featured_product_title">Hot Sales</h3>
        </div>
        <div class="featured_product_body" style="height: 23rem;">
            <div class="owl-carousel">
            @foreach($hproducts->take(7) as $prod)
                @php
                    $name = str_replace(" ","-",$prod->name);
                @endphp
                @php
                    $prod->pprice = $prod->pprice ? : $prod->cprice;
                    $prod->cprice = $prod->getPrice(1);
                @endphp
                <div class="item" style="height:23rem;">
                    <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}">
                    <div class="container">
                        @if($gs->sign == 0)
                            @if($prod->discount_percent != 0)
                                <div class="discount_badge">
                                    <p class="my-0 pa-0">{{ $prod->discount_percent }}%<br><span>save</span></p>
                                </div>
                            @endif
                        @endif
                        
                        @if($prod->sale_from && $prod->sale_from <= $now && $prod->sale_to >= $now)
                        <div class="offer_timer_div js-countdown" data-end-date="{{$prod->sale_to}}" data-month-format="%m" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
                            <div class="offer_subdiv_primary">
                                <i class="material-icons">av_timer</i>
                            </div>
                            <div class="timer_alignment">
                                <div class="js-cd-days offer_subdiv_white" style="color:rgb(214, 57, 57) !important;">
                                    <p> <span>20</span> <span class="offer_time_unit">D</span></p>
                                </div>
                                <span style="font-size:15px;">D</span>
                            </div>
                            <div class="timer_alignment offer_subdiv_primary">
                                <div class="js-cd-hours ">
                                    <p> <span>23</span>  <span class="offer_time_unit">H<span></p>
                                </div>
                                <span style="font-size:15px;">H</span>
                            </div>

                            <div class="timer_alignment">
                                <div class="js-cd-minutes offer_subdiv_white">
                                    <p> <span>59</span> <span class="offer_time_unit">M</span></p>
                                </div>
                                <span style="font-size:15px;">M</span>
                            </div>

                            <div class="timer_alignment offer_subdiv_primary">
                                <div class="js-cd-seconds">
                                    <p> <span>02</span>  <span class="offer_time_unit">S</span></p>
                                </div>
                                <span style="font-size:15px;">S</span>
                            </div>
                        </div>
                        @endif
                        @if($prod->features!=null && $prod->colors!=null)
                        @php
                        $title = explode(',', $prod->features);
                        $details = explode(',', $prod->colors);
                        @endphp

                        @endif
                        <input type="hidden" value="{{$prod->id}}">
                        
                            
                        <div class="featured_product_image_div hover-zoom" >
                            <img class=" card-product-image" src="{{asset('assets/images/'.$prod->photo)}}" alt="{{$prod->name}}" />
                        
                        </div>
        
                        <div class="featured_product_detail_div">
                            <h4 class="featured_product_name">
                                    <a href="{{route('front.product',['id' => $prod->id, 'slug' => str_slug($name,'-')])}}" class="text-center">
                                            {{strlen($prod->name) > 38 ? substr($prod->name,0,38)."..." : ucwords(strtolower($prod->name))}}
                                    </a>
                                    </h4>
                            <p class="featured_product_subtitle">{{strlen($prod->company_name) > 25 ? ucwords(strtolower(substr($prod->company_name,0,25))).'...' : ucwords(strtolower(substr($prod->company_name,0,25)))}}</p>
                            <p class="featured_product_price">{{$curr->sign}}
                                    {{round($prod->cprice * $curr->value,2)}}
                                    @if($prod->pprice != null && $prod->pprice != 0  && $prod->pprice > $prod->cprice)
                                        <span class="featured_product_price_old">{{$curr->sign}} {{round($prod->pprice * $curr->value,2)}}</span> <span>-{{ $prod->discount_percent }}%</span></p>

                                @endif
                            <div class="text-center">
                                <input type="hidden" value="{{$prod->id}}">
                                <span class="hovertip addcart" rel-toggle="tooltip" title="{{$lang->hcs}}"><a class="productDetails-addCart-btn btn btn-primary" id="addcrt" href="javascript:;" style="cursor: pointer;"><i class="material-icons">shopping_cart</i> Add to Cart
                                </a>
                            </div>

                        </div>
                        </a>
                    </div>
                </div>
            @endforeach

            </div>
        </div>
    </div> --}}

  <br>

</div>

@endsection
<style>
    .owl-item{
        /* margin: 0 12px */
    }
    .owl-buttons {
        position: absolute;     
        right: 10px;
        top: -50px;
        display: flex;

    }
    .owl-prev{
        margin-right: 0.5rem;
    }
    .owl-buttons i {
        top: 12px !important;
        font-size: 25px !important;
        font-weight: 700;
    }
</style>
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>

<script>
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            // autoPlay: 3000,
            items: 5,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            center: true,
            navigation: true,
            loop: true,
            navigationText: [
                '<button type="button" role="presentation" class="class btn btn-sm btn-primary" style="border-radius: 50%; height:35px; width: 35px; padding: 0"><i class="material-icons">chevron_left</i></button>',
                '<button type="button" role="presentation" class="class btn btn-sm btn-primary" style="border-radius: 50%; height:35px; width: 35px; padding: 0"><i class="material-icons">chevron_right</i></button>'
            ],
            responsive: {
                600: {
                    items: 1
                }

            }
        });
    });

</script>

@endsection
  
