@extends('layouts.front1')
@section('title','Upload Prescription')
@section('content')

<style>
    .info {
    max-width: 360px;
    margin: 0 auto;
    padding: 0px 0 30px;
}
</style>

<div class="header-filter" filter-color="info" style="background-image: url('https://demos.creative-tim.com/material-kit-pro/assets/img/bg7.jpg'); background-size: cover; background-position: top center;">
    <div class="container" style="padding-bottom:30px;">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="card card-signup">
            <h3 class="card-title text-center">
                <div class="icon icon-info">
                    <i class="material-icons" style="font-size:35px;">assignment</i>
                  </div>
                  Send us your Prescription</h3>
            <div class="card-body">
              <div class="row">
                <div class="col-md-5 mr-auto">
                    @include('includes.form-error')
                    <form method="POST" action="{{route('prescription-upload')}}" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <select class="form-control" name="type" hidden>
                            <option value="medicine" disabled>Select your prescription type</option>
                            <option value="medicine" >Medicine</option>
                           
                          </select>

                      <div class="form-group bmd-form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">face</i>*
                            </span>
                          </div>
                          <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Full Name..." required>
                        </div>
                      </div>
                      <div class="form-group bmd-form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">mail</i>*
                            </span>
                          </div>
                          <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email..." required>
                        </div>
                      </div>

                      <div class="form-group bmd-form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">phone</i>*
                            </span>
                          </div>
                          <input type="tel" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Phone Eg.9841000000" required>
                        </div>
                      </div>

                      <div class="form-group bmd-form-group is-filled">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">location_on</i>*
                            </span>
                          </div>
                    
                          <input placeholder="Address" class="form-control" name="location" value="" id="geolocation" style="resize: vertical;" autocomplete="off" required/>
                          {{-- <input id="latlong" type="hidden" name="latlong" value=""> --}}
                          <span class="btn btn-info" style="border-radius:30px;padding:7px;" onclick="$('#model-type').val('');$('.locationModal').modal('show');"> <i class="material-icons">location_on</i></span>
                          
                        </div>
                        <h6 class="text-center" style="color: brown">Delivery within nepal only</h6>
                      </div>

                      <div class="form-group bmd-form-group is-filled">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">notes</i>
                            </span>
                          </div>
                          <textarea class="form-control" name="additional_info" placeholder="Notes...(if any)" rows="2" id="exampleInputTextarea">{{old('additional_info')}}</textarea>
                        </div>
                      </div>

                      <div class="container text-center">
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                          <div class="fileinput-new thumbnail img-circle img-raised" style="margin-top:5px; ">
                            <img src="https://image.freepik.com/free-vector/medical-prescription-concept_108855-1752.jpg" alt="...">
                              </div>
                          <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                          <div>
                              <h6 class="text-danger">Prescription Required</h6>
                          <span class="btn btn-raised btn-round btn-warning btn-file">
                              <span class="fileinput-new">Select Prescription</span>
                              <span class="fileinput-exists">Change</span>
                              <input type="file" name="filenames[]" multiple required/>
                          </span>
                              <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                              <i class="fa fa-times"></i> Remove</a>
                          </div>
                      </div>
                      </div>

                    
                   
                      <div class="text-center">
                        <button type="submit" id="upload_prescription_submit" class="btn btn-info btn-block"><span><i class="fa fa-spinner fa-spin" id="loader" style="display:none ;"></i></span> Submit</button>
                      </div>
                    </form>
                  </div>
                <div class="col-md-5 ml-auto">
                  <div class="info info-horizontal">
                    <div class="icon icon-info">
                      <i class="material-icons">assignment</i>
                    </div>
                    <div class="description">
                      <h4 class="info-title">Upload Prescription</h4>
                      <p class="description">
                        Upload image of prescription given by your doctor.
                      </p>
                    </div>
                  </div>
                  <div class="info info-horizontal">
                    <div class="icon icon-info">
                      <i class="material-icons">search</i>
                    </div>
                    <div class="description">
                      <h4 class="info-title">Analyze </h4>
                      <p class="description">
                        We analyze your prescription and process your order.
                      </p>
                    </div>
                  </div>
                  <div class="info info-horizontal">
                    <div class="icon icon-info">
                      <i class="material-icons">delivery_dining</i>
                    </div>
                    <div class="description">
                      <h4 class="info-title">Delivery </h4>
                      <p class="description">
                        We deliver your medicine at your doorsteps.
                      </p>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <div class="modal fade locationModal" ng-app="locationSelector" ng-controller="LocationController" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="margin-top: 0;">
        <div class="modal-header text-center" style="border-bottom: none;padding-bottom: 0">
            <h4><strong>SET A LOCATION</strong></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="fa fa-times"></i>
            </button>
        </div>
        <div style="padding-left:25px;padding-right:25px;">
        <p>Drag the pin to your exact location
      Or, Simply type your address below.</h5>
        </p>
        </div>
  
        <div class="modal-body text-center">
            <input type="hidden" id="model-type" value="" />
            <div class="input-group g-pb-13 g-px-0 g-mb-10">
  
                <input 
                  places-auto-complete size=80
                  types="['establishment']"
                  component-restrictions="{country:'np'}"
                  on-place-changed="placeChanged()"
                  id="googleLocation"
                  {{-- ng-model="address.Address" --}}
                  class="form-control" type="text" placeholder="Select Area" autocomplete="off">
  
                <button class="btn btn-primary rounded-0" type="button" ng-click="getLocation()"><i class="fa fa-crosshairs"></i></button>
            </div>
            <p ng-if="error" style="color:red;text-align: left">@{{ error }}</p>
  
            <ng-map center="[27.7041765,85.3044636]" zoom="15" draggable="true">
                <marker position="27.7041765,85.3044636" title="Drag Me!" draggable="true" on-dragend="dragEnd($event)"></marker>
            </ng-map>
        </div>
        <div class="modal-footer" style="border-top: none; text-align: center; display: block;">
          <button type="button" ng-disabled="!isValidGooglePlace" class="btn btn-primary" style="width:100%" ng-click="confirmLocation()">Confirm</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script src="/assets/front/js/ng-map.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyAcvyYLSF2ngh8GM7hX7EQ3dIcQGbGnx5Q&libraries=places"></script>
<script src="/assets/front/js/location.js"></script>


<script>
  $('#upload_prescription_submit').on('click',function(){
    var x = document.getElementById("loader");
    // console.log('click');
    x.style.display = "";
});

</script>
    
@endsection