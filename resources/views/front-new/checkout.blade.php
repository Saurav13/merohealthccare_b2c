@extends('layouts.front1')
@section('title','Checkout')

@section('content')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<style>
    .table-shopping .td-number {
      text-align: right;
      min-width: 45px;
  }
  .table-shopping>thead>tr>th {
      font-size: 16px;
      text-transform: capitalize;
      font-family: 'Muli';
  }
  .table{
    font-family: 'Muli';
  }
  .form-control::placeholder{
    color:#999 !important;
  }
  </style>

@php
$user = Auth::guard('user')->user();
@endphp

<form name="Form" action="{{ route('payment.submit') }}" method="post" id="payment_form" onsubmit="return validateForm()" enctype="multipart/form-data" >
  {{csrf_field()}}
<div class="container">
    <button class="btn btn-info btn-round btn-raised" style="margin-top:20px;"><i class="material-icons">shopping_cart</i> Checkout</button>
    <div class="row">
        <div class="col-lg-8 col-md-12 ml-auto mr-auto">
        <div class="table-responsive" style="margin-top:25px;">
            <table class="table table-shopping">
            <thead>
                <tr >
                <th>Product</th>
                <th>Name</th>
                <th class="text-center">Price</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Amount</th>
                </tr>
            </thead>
            <tbody>
                @php
                $priceDiscount = 0;
                $vat_sum = 0;
                @endphp
            @foreach($products as $id => $product)
                @php
                   
                    $price = $product['item']['pprice'] ? $product['item']['pprice']: $product['item']['cprice'];
                    $vat = App\Product::findORFail($product['item']['id']);

                @endphp
              <tr>
                <td>
                
                    <div class="img-container" style="max-height:500px">
                    <a href="{{ route('front.product',[$product['item']['id'],$product['item']['slug']]) }}" style="font-weight: 500; font-size:15px;" target="_blank">
                       <img style="height:65px;width:auto;" src="{{asset('assets/images/'.$product['item']['photo'])}}" alt="{{ $product['item']['name'] }}"><br>
                    </a>
                    </div>
                </td>
                <td>
                  <a href="{{ route('front.product',[$product['item']['id'],$product['item']['slug']]) }}" style="font-weight: 500; font-size:15px;" target="_blank">
                    {{ ucwords(strtolower($product['item']['name'])) }}
                    @if($vat->vat_status == 1)
                    <span style="font-style: italic;">*</span>
                     @endif
                 </a>
                <h6 style="text-transform:capitalize;">Varient : {{$vat->sub_title}}</h6>
                <h6>{{$vat->product_quantity}}</h6>
                </td>

                <input type="hidden" id="stock{{$product['item']['id']}}" value="{{$product['stock']}}">

                <td class="td-number text-center">
                  @if($product['item']['cprice'])
                  <span style="font-size:16px;">
                    {{$curr->sign}}{{ $product['item']['cprice'] }}
                  </span>
                  @endif
                  @if($product['item']['pprice'])
                  <del style="font-size:14px;color:red">
                    @if($gs->sign == 0)
                    {{$curr->sign}}{{ round($price * $curr->value, 2) }}
                    @else
                    {{ round($price * $curr->value, 2) }}{{$curr->sign}}
                    @endif
                    </del>
                    @endif
                </td>
                <td class="td-number text-center">
                  <span class="badge badge-pill badge-info" style="font-size:15px;" id="qty{{$product['item']['id']}}">{{ $product['qty'] }}</span>
                
                </td>
                <td class="td-number text-center" style="font-size:16px;">
                     @if($gs->sign == 0)
                          {{$curr->sign}}<span id="prc{{$product['item']['id']}}">{{ round($product['price'] * $curr->value, 2) }}</span>
                      
                        @else
                          <span id="prc{{$product['item']['id']}}">{{ round($product['price'] * $curr->value, 2) }}</span>{{$curr->sign}}
                         
                        @endif
                </td>

                @php
                  $priceDiscount += round($price * $product['qty'] * $curr->value , 2) - round($product['price'] * $curr->value , 2);
                @endphp
             
              </tr>
              @php
              if($vat->vat_status == 1){
              $vat_price = $price * $product['qty'] * (100/($gs->vat+100)) * $curr->value;
              $vat_sum = $vat_sum + $vat_price;
              }
          @endphp
              @endforeach

            </tbody>
            </table>
        </div>
        </div>
        <div class="col-lg-4 col-md-12 ml-auto mr-auto">
            <div class="card card-pricing card-info bg-info" style="margin-bottom:10px ">
                <div class="card-body">
                  <button class="btn btn-white btn-block"><i class="material-icons">shopping_cart</i> Checkout Summary</button>
                  {{-- <h4 class="card-title h3 card-header card-header-info text-center" style="margin-left:2px;margin-right:2px;margin-top:10px;"> 
                   
                    <strong style="font-weight:800;">{{$curr->sign}}</strong><span class="total g-font-weight-800" id="grandtotal" style="font-size:40px;font-weight:800;">123123</span>
              
                   
                  </h4> --}}
                  
                  

                      <div class="container" style="color: white; font-size:16px; margin-top:15px;">
                      <div style="display: flex; justify-content:space-between !important;"> 
                          <span>Subtotal</span>
                          <span>{{$curr->sign}} {{round(($totalPrice + $priceDiscount - $shipping_cost)* $curr->value,2)}}</span>
                      </div>

                      @if($priceDiscount > 0)
                        <div style="display: flex; justify-content:space-between !important;"> 
                          <span>Price Discount</span>
                          <span>- {{$curr->sign}}<span>{{ $priceDiscount }}</span>
                        </div>
                      @endif

                      <div style="display: none !important;" id="discount">
                      <div style="display: flex !important; justify-content:space-between !important;" > 
                        <span class="text-left">{{$lang->ds}} (<span id="sign"></span>)</span>
                        <span>- {{$curr->sign}}<span id="ds"></span></span>
                      </div>
                      </div>


                      @if($shipping_cost > 0)
                       
                          <div style="display: flex; justify-content:space-between !important;"> 
                            <span>Delivery Charge</span>
                            <span>{{$curr->sign}}<span id="ship-cost">{{round($shipping_cost * $curr->value,2)}}</span></span>
                        </div>
                      @endif

                        <div style="display: none" id="district">
                          <div style="display: flex !important; justify-content:space-between !important;"> 
                            <span>Courier Charge</span>
                            <span>{{$curr->sign}}<span id="cc"></span></span>
                          </div>
                        </div>


                      <hr>
                        <div style="display: flex; justify-content:space-between !important; font-weight:700;"> 
                            <span>GrandTotal</span>
                            <span>{{$curr->sign}}<span id="total-cost"> {{round($totalPrice * $curr->value ,2)}}</span></span>
                        </div>
                      </div>
                 
                    </div>
                 
                  </div>
                  
                  <a href="{{ route('front.cart')}}" class="btn btn-warning btn-block"><i class="material-icons">shopping_cart</i> Go to Cart</a>
                  @if(Auth::guard('user')->user())
                    <div class="form-group bmd-form-group is-filled">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">card_giftcard</i>
                          </span>
                        </div>

                        <input type="hidden" id="">
                        <input type="text" id="code" class="form-control" placeholder="Enter Coupon Code" style="text-transform: uppercase;">
                        <button id="coupon" type="submit" class="btn btn-rose btn-block"><i class="material-icons">check</i> Apply Code</button>
                      </div>
                    </div>
                    @else
                      <span><a class="underline" href="{{route('user-login')}}"> <i class="material-icons">person_outline</i>Login</a> to use Coupon Code</span>
                    @endif
           
            </div>     
    </div>

    <input type="hidden" id="shipping-cost" name="shipping_cost" value="{{round($shipping_cost * $curr->value,2)}}">
    <input type="hidden" name="dp" value="{{$digital}}">
    <input type="hidden" name="tax" value="{{$gs->tax}}">
    <input type="hidden" name="totalQty" value="{{$totalQty}}">
    <input type="hidden" name="total" id="grandtotal" value="{{round($totalPrice * $curr->value,2)}}">
    <input type="hidden" name="coupon_code" id="coupon_code" value="">
    <input type="hidden" name="coupon_discount" id="coupon_discount" value="0">
    <input type="hidden" name="coupon_id" id="coupon_id" value="">
    <input type="hidden" name="courier_charge" id="courier_charge" value="">


 <h3 class="title">Billing Details</h3>
 <div class="form-group" {!!$digital == 1 ? 'style="display:none;"' : ''!!}>
  <select class="form-control" onchange="sHipping(this)" id="shipop" name="shipping" hidden required="">
      <option value="shipto" selected="">{{$lang->ships}}</option>
      <option value="pickup">{{$lang->pickup}}</option>
  </select>
</div>

<div id="pick" style="display:none;">
  <div class="form-group">
      <select class="form-control" name="pickup_location">
          <option value="">{{$lang->pickups}}</option>
          @foreach($pickups as $pickup)
          <option value="{{$pickup->location}}">{{$pickup->location}}</option>
          @endforeach
      </select>
  </div>
</div>

@include('includes.form-error')
<div class="row">
  @if(Auth::guard('user')->check())
  @php
      $user = Auth::guard('user')->user();
  @endphp

  <div class="col-md-6"> 
    <div class="title">
      <h5>Personal Details</h5>
    </div>
      <div class="form-group bmd-form-group is-filled">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">face</i>*
            </span>
          </div>
          <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Full Name" required>
        </div>
      </div>
  
      <div class="form-group bmd-form-group is-filled ">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">phone</i>*
            </span>
          </div>
          <img src="https://upload.wikimedia.org/wikipedia/commons/9/9b/Flag_of_Nepal.svg" style="height: 1.5rem;margin-top:10px;margin-right:5px;"/>
          <input type="text" class="form-control" name="phone" value="{{$user->phone}}" placeholder="Phone Number eg:9841000000 " required>
        </div>
      </div>

      <div class="form-group bmd-form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">mail</i>*
            </span>
          </div>
          <input type="email" class="form-control" name="email" value="{{$user->email}}" placeholder="yourmail@example.com" required>
        </div>
      </div>

      <div class="form-group bmd-form-group is-filled">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">chrome_reader_mode</i>
            </span>
          </div>
          <input type="text" class="form-control" name="pan_number" value="{{$user->pan_vat}}" placeholder="PAN number (Optional)" >
        </div>
      </div>
      <div class="form-group bmd-form-group is-filled">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">notes</i>
            </span>
          </div>
          <textarea class="form-control" name="order_notes" placeholder="Notes Eg. Deliver after 2pm, Fast delivery.... (Optional)" rows="4" id="exampleInputTextarea">{{old('order_notes')}}</textarea>
        </div>
      </div>

 
  </div>

  <div class="col-md-6">
    <div class="title">
      <h5>Address Details</h5>
    </div>
    <div class="row">
      <div class="col-sm-6 col-xs-6 col-md-6">
          <div class="form-group bmd-form-group is-filled">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">home</i>
                </span>
              </div>
              <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="address_type" value="Home" {{ $user->address_type == 'Home' ? 'checked' : '' }}>
                    Home
                    <span class="circle">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
            </div>
          </div>
      </div>
      <div class="col-sm-6 col-xs-6 col-md-6">
          <div class="form-group bmd-form-group is-filled">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">corporate_fare</i>
                </span>
              </div>
              <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="address_type" value="Office" {{ $user->address_type == 'Office' ? 'checked' : '' }}>
                    Office
                    <span class="circle">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
            </div>
          </div>
      </div>
    </div>

    @php
    $districts = App\CourierCharge::where('status','=','Active')->get();

    @endphp
    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">map</i>*
          </span>
        </div>
        <select name="district" class="selectpicker district" data-live-search="true" data-style="btn-primary" required>
          <option disabled>Select District: {{count($districts)}} Available</option>
          @foreach($districts as $district)
            <option data-tokens="{{$district->is_charged}}">{{$district->district}}</option>
          @endforeach
        </select>
        <button type="button" class="btn btn-default" style="border-radius:30px;margin-left:10px;padding:0px 12px;" data-toggle="modal" data-target="#courierChargeModal">
          <i class="material-icons">info</i>
        </button>
        
      </div>
    </div>

    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">location_on</i>*
          </span>
        </div>
        <input placeholder="Address" class="form-control" name="address" value="{{$user->address ? $user->address : old('address') }}" id="geolocation" style="resize: vertical;" autocomplete="off" required/>
        <input id="shipping_latlong" type="hidden" name="shipping_latlong" value="">
        <span class="btn btn-info" style="border-radius:30px;padding:7px;" onclick="$('#model-type').val('shipping');$('.locationModal').modal('show');"> <i class="material-icons">location_on</i></span>
    
      </div>
      <h6 class="text-center" style="color: brown">Delivery within  <img src="https://upload.wikimedia.org/wikipedia/commons/9/9b/Flag_of_Nepal.svg" style="height: 1rem;margin-right:5px;margin-left:5px;"/> nepal only</h6>
    </div>

    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">flag</i>*
          </span>
        </div>
        <input type="text" class="form-control" name="customer_country" value="Nepal" placeholder="Country">
      </div>
    </div>


    @php
    $prescrition_count = 0;
    @endphp
    @foreach($products as $id => $product)
    @if($product['item']['requires_prescription'])
        @php
            $prescrition_count = $prescrition_count + 1;
        @endphp
    @else
    @endif
    @endforeach
    @if( $prescrition_count > 0)
      <div class="container text-center">
        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
          <div class="fileinput-new thumbnail img-circle img-raised" style="margin-top:5px; ">
            <img src="https://image.freepik.com/free-vector/medical-prescription-concept_108855-1752.jpg" alt="...">
              </div>
          <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
          <div>
              <h6 class="text-danger">Prescription Required</h6>
          <span class="btn btn-raised btn-round btn-warning btn-file">
              <span class="fileinput-new">Select Prescription</span>
              <span class="fileinput-exists">Change</span>
              <input type="file" name="filenames[]" multiple required/>
          </span>
              <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
              <i class="fa fa-times"></i> Remove</a>
          </div>
      </div>
      </div>
    @endif

    <div class="container" style="padding:15px;">
      <button class="btn btn-success btn-block">
        <i class="material-icons">payments</i> Proceed to Payment  <i class="material-icons">keyboard_arrow_right</i> </button>
    </div>
  </div>
  <input type="hidden" name="user_id" value="{{$user->id}}">
  @else

  <div class="col-md-6"> 
    <div class="title">
      <h5>Personal Details</h5>
    </div>
      <div class="form-group bmd-form-group is-filled">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">face</i>*
            </span>
          </div>
          <input type="text" class="form-control" name="name" placeholder="Full Name" value="{{ old('name') }}" required>
        </div>
      </div>
      <div class="form-group bmd-form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">phone</i>*
            </span>
          </div>
          <img src="https://upload.wikimedia.org/wikipedia/commons/9/9b/Flag_of_Nepal.svg" style="height: 1.5rem;margin-top:10px;margin-right:5px;"/>
          <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone Number eg:9841000000 " required>
        </div>
      </div>
      <div class="form-group bmd-form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">mail</i>*
            </span>
          </div>
          <input type="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="yourmail@example.com" required>
        </div>
      </div>

      <div class="form-group bmd-form-group ">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">chrome_reader_mode</i>
            </span>
          </div>
          <input type="text" class="form-control" value="{{ old('pan_number') }}" name="pan_number" placeholder="PAN number (Optional)" >
        </div>
      </div>
      <div class="form-group bmd-form-group is-filled">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="material-icons">notes</i>
            </span>
          </div>
          <textarea class="form-control" name="order_notes" placeholder="Notes Eg. Deliver after 2pm, Fast delivery.... (Optional)" rows="4" id="exampleInputTextarea">{{old('order_notes')}}</textarea>
        </div>
      </div>

 
  </div>

  <div class="col-md-6">
    <div class="title">
      <h5>Address Details</h5>
    </div>
    <div class="row">
      <div class="col-md-6">
          <div class="form-group bmd-form-group is-filled">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">home</i>
                </span>
              </div>
              <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="address_type" value="Home" checked>
                    Home
                    <span class="circle">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
            </div>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group bmd-form-group is-filled">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">corporate_fare</i>
                </span>
              </div>
              <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="address_type" value="Office">
                    Office
                    <span class="circle">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
            </div>
          </div>
      </div>
    </div>

    @php
        $districts = App\CourierCharge::where('status','=','Active')->get();
        
    
    @endphp
    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">map</i>*
          </span>
        </div>

        <select name="district" class="selectpicker district" data-live-search="true" data-style="btn-primary" required>
          <option disabled>Select District : {{count($districts)}} Available</option>
          @foreach($districts as $district)
            <option data-tokens="{{$district->is_charged}}">{{$district->district}}</option>
          @endforeach
        </select>
        <button class="btn btn-default" style="border-radius:30px;margin-left:10px;padding:0px 12px;" data-toggle="modal" data-target="#courierChargeModal">
          <i class="material-icons">info</i>
        </button>
    
    
      </div>
    </div>
    

    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">location_on</i>*
          </span>
        </div>

        <input placeholder="Address" class="form-control" name="address" value="{{ old('address') }}" id="geolocation" style="resize: vertical;" autocomplete="off" required/>
        <input id="latlong" type="hidden" name="latlong" value="">
        <span class="btn btn-info" style="border-radius:30px;padding:7px;" onclick="$('#model-type').val('');$('.locationModal').modal('show');"> <i class="material-icons">location_on</i></span>

        
        {{-- <input placeholder="{{$lang->doad}} " class="form-control" name="address" id="geolocation" style="resize: vertical;" onclick="$('#model-type').val('');$('.locationModal').modal('show');" autocomplete="off" />
        <input id="latlong" type="hidden" name="latlong" value=""> --}}

      </div>
      <h6 class="text-center" style="color: brown">Delivery within  <img src="https://upload.wikimedia.org/wikipedia/commons/9/9b/Flag_of_Nepal.svg" style="height: 1rem;margin-right:5px;margin-left:5px;"/> nepal only</h6>
    </div>

    <div class="form-group bmd-form-group is-filled">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="material-icons">flag</i>*
          </span>
        </div>
        <input type="text" class="form-control" name="customer_country" value="Nepal" placeholder="Country" required>
      </div>
    </div>


    @php
    $prescrition_count = 0;
    @endphp
    @foreach($products as $id => $product)
    @if($product['item']['requires_prescription'])
        @php
            $prescrition_count = $prescrition_count + 1;
        @endphp
    @else
    @endif
    @endforeach
    @if( $prescrition_count > 0)
      <div class="container text-center">
        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
          <div class="fileinput-new thumbnail img-circle img-raised" style="margin-top:5px; ">
            <img src="https://image.freepik.com/free-vector/medical-prescription-concept_108855-1752.jpg" alt="...">
              </div>
          <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
          <div>
              <h6 class="text-danger">Prescription Required</h6>
          <span class="btn btn-raised btn-round btn-warning btn-file">
              <span class="fileinput-new">Select Prescription</span>
              <span class="fileinput-exists">Change</span>
              <input type="file" name="filenames[]" multiple required/>
          </span>
              <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
              <i class="fa fa-times"></i> Remove</a>
          </div>
      </div>
      </div>
    @endif
    <div class="container" style="padding:15px;">
      <button class="btn btn-success btn-block">
        <i class="material-icons">payments</i> Proceed to Payment  <i class="material-icons">keyboard_arrow_right</i></button>
    </div>
 
  </div>
  <input type="hidden" name="user_id" value="0">
  @endif


</div>

</div>
</form>

<div class="modal fade locationModal" ng-app="locationSelector" ng-controller="LocationController" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 0;">
      <div class="modal-header text-center" style="border-bottom: none;padding-bottom: 0">
          <h4><strong>SET A LOCATION</strong></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
          </button>
      </div>
      <div style="padding-left:25px;padding-right:25px;">
      <p>Drag the pin to your exact location
    Or, Simply type your address below.</h5>
      </p>
      </div>

      <div class="modal-body text-center">
          <input type="hidden" id="model-type" value="" />
          <div class="input-group g-pb-13 g-px-0 g-mb-10">

              <input 
                places-auto-complete size=80
                types="['establishment']"
                component-restrictions="{country:'np'}"
                on-place-changed="placeChanged()"
                id="googleLocation"
                {{-- ng-model="address.Address" --}}
                class="form-control" type="text" placeholder="Select Area" autocomplete="off">

              <button class="btn btn-primary rounded-0" type="button" ng-click="getLocation()"><i class="fa fa-crosshairs"></i></button>
          </div>
          <p ng-if="error" style="color:red;text-align: left">@{{ error }}</p>

          <ng-map center="[27.7041765,85.3044636]" zoom="15" draggable="true">
              <marker position="27.7041765,85.3044636" title="Drag Me!" draggable="true" on-dragend="dragEnd($event)"></marker>
          </ng-map>
      </div>
      <div class="modal-footer" style="border-top: none; text-align: center; display: block;">
        <button type="button" ng-disabled="!isValidGooglePlace" class="btn btn-primary" style="width:100%" ng-click="confirmLocation()">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courierChargeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">About Courier Charge</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>The additional <strong>Courier Charge</strong> is applied outside Kathmandu Valley Districts <strong>(except Kathmandu, Lalitpur and Bhaktapur Districts.)</strong></p>
        <hr>
       <p> <strong>Note:</strong> Only online payements (Bank, Esewa, Khalti, FonePay, Card) are acceptable for delivery outside Kathmandu Valley excluding Cash on Delivery(COD) feature.</p>
        <hr>

        <p>Check Courier Charge List : 
          <a class="underline"  data-toggle="collapse" href="#courierChargeList" aria-expanded="false" aria-controls="collapseExample">
            Click Here
          </a>
 
      </p>
      <div class="collapse" id="courierChargeList">
        <div class="card card-body">
          @php
              $courier_charges = App\CourierCharge::where('status','=','Active')->orderBy('district')->get();
          @endphp

<table class="table">
  <thead>
      <tr>
      
          <th>District</th>
          <th>Courier Charge</th>
     
      </tr>
  </thead>
  <tbody>
  
        @foreach($courier_charges as $cc)
        <tr>
          <td>{{$cc->district}}</td>
          <td>Rs.{{$cc->courier_charge}}</td>
        </tr>
        @endforeach
         
    
  </tbody>
</table>

        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-rose" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Okay</button> --}}
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script type="text/javascript">
  $('.category-multiple').select2({
    placeholder: 'Select a Prescription file'
  });
</script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    <script src="/assets/front/js/ng-map.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyAcvyYLSF2ngh8GM7hX7EQ3dIcQGbGnx5Q&libraries=places"></script>
    <script src="/assets/front/js/location.js"></script>
    {{-- @if($user)
        <script>
            $('.selectFamily').select2({
                width: '100%'
            });
        </script>
    @endif --}}

    @if(count($errors->checkoutForm) > 0)
        <script>
            $(document).ready(function(){
                $.notify("Please provide all the Information.","error");
            });

        </script>
    @endif
    <script type="text/javascript">

        $("#coupon").click( function () {
            val = $("#code").val();
            discount = parseFloat($("#coupon_discount").val());
            $.ajax({
                    type: "GET",
                    url:"{{URL::to('/json/coupon')}}",
                    data:{code:val},
                    success:function(data){
                        if(data == 0)
                        {
                            $.notify("{{$gs->no_coupon}}","error");
                            $("#code").val("");
                        }
                        else if(data == 2)
                        {
                            $.notify("{{$gs->coupon_already}}","error");
                            $("#code").val("");
                        }
                        else
                        {
                            var shipcost = parseFloat($("#shipping-cost").val());

                            $("#coupon_code").val(data[1]);
                            $("#coupon_id").val(data[3]);
                            $("#coupon_discount").val(data[2]);
                            $("#discount").show("slow");
                            $("#ds").html(data[2]);
                            var total = parseFloat($("#total-cost").html());
                            $('#total-cost').html((data[0]+shipcost).toFixed(2))
                            // $("#ftotal").show("slow");
                            $("#sign").html(data[4]);
                            var x = $("#shipop").val();
                            // var y = data[0];
                            // $("#ft").html(y.toFixed(2));
                            $("#grandtotal").val(y);
                            $.notify("{{$gs->coupon_found}}","success");
                            $("#code").val("");
                            // $("#coupon-click1").hide();
                            // $("#coupon-click2").show();

                        }
                    },
                    error: function(data){
                        if(data.responseJSON)
                            $.notify(data.responseJSON.error,"error");
                        else
                            $.notify('Something went wrong',"error");

                    }
            });
            return false;
        });
    </script>

<script type="text/javascript">
$(document).ready(function(){

  $("select.district").change(function () {
    // console.log('clicked courier charge');
      val = $(this).children("option:selected").val();
      discount = $("#coupon_discount").val();
      // console.log(discount);
      $.ajax({
              type: "GET",
              url:"{{URL::to('/json/couriercharge')}}",
              data:{district:val,coupon:discount},
              success:function(data){
                // console.log('success');
                // console.log(data[1]);
                // console.log(data[2]);
                if(data[0] > 0){
                  $("#district").show("slow");
                  $("#cc").html(data[0]);
                  $('#total-cost').html((data[1]).toFixed(2))
                  $("#courier_charge").val(data[0]);
                }
                else if(data[0] == 0){
                  $("#district").hide("slow");
                  $("#cc").html(data[0]);
                  $('#total-cost').html((data[1]).toFixed(2))
                  $("#courier_charge").val(data[0]);
                }
      
              },
              error: function(data){
                  if(data.responseJSON)
                      $.notify(data.responseJSON.error,"error");
                  else
                      $.notify('Something went wrong',"error");
              }
      });
      return false;
  });
});
</script>

    <script type="text/javascript">

        function sHipping(val) {
            var shipcost = parseFloat($("#ship-cost").html());
            var totalcost = parseFloat($("#total-cost").html());
            // var finalcost = parseFloat($("#ft").html());
            var total = 0;
            var ft = 0;
            // var ck = $("#ft").html();
            if (val.value == "shipto") {

                total = shipcost + totalcost;
                $("#pick").hide();
                $("#ship-diff").show();
                $("#pick-info").hide();
                $("#shipshow").show();
                $("#shipping-cost").val(shipcost);
                $("#total-cost").html(total);
                $("#grandtotal").val(total);
                $("#shipto").find("input").prop('required',true);
                $("#pick").find("select").prop('required',false);
            }

            if (val.value == "pickup") {

                total = totalcost - shipcost;
                $("#pick").show();
                $("#pick-info").show();
                $("#ship-diff").hide();
                $("#shipshow").hide();
                $("#shipping-cost").val('0');
                $("#total-cost").html(total.toFixed(2));
                $("#grandtotal").val(total.toFixed(2));
                $("#shipto").find("input").prop('required',false);
                $("#pick").find("select").prop('required',true);

            }
        }
        $(document).on('click','#coupon-click1',function(){
            $('.coupon-code .form').slideToggle();
        });
        $(document).on('click','#coupon-click2',function(){
            $('.coupon-code .form').slideToggle();
        });

        $(document).on('change','.shippingCheck',function(){
            if(this.checked) {
                $(".shipping-details-area").show();
            }
            else
            {
                $(".shipping-details-area").hide();

            }
        });


    </script>


    @if(isset($checked))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#checkoutModal').modal('show');
        });

    </script>
    @endif

    <script type="text/javascript">

        // $(document).ready(function() {

        //  if (document.form.filename.value == "" && document.form.fileid.value="") {
        //     document.getElementById("textbox1").required = true;
        //   }
        // });



        $(document).ready(function() {
          $(".btn-success").click(function(){
              var html = $(".clone").html();
              $(".increment").after(html);
          });

          $("body").on("click",".btn-danger",function(){
              $(this).parents(".control-group").remove();
          });

});





      </script>
{{--
<script>
    var dropZoneId = "drop-zone";
      var buttonId = "clickHere";
      var mouseOverClass = "mouse-over";
    var dropZone = $("#" + dropZoneId);
     var inputFile = dropZone.find("input");
     var finalFiles = {};
    $(function() {



      var ooleft = dropZone.offset().left;
      var ooright = dropZone.outerWidth() + ooleft;
      var ootop = dropZone.offset().top;
      var oobottom = dropZone.outerHeight() + ootop;

      document.getElementById(dropZoneId).addEventListener("dragover", function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropZone.addClass(mouseOverClass);
        var x = e.pageX;
        var y = e.pageY;

        if (!(x < ooleft || x > ooright || y < ootop || y > oobottom)) {
          inputFile.offset({
            top: y - 15,
            left: x - 100
          });
        } else {
          inputFile.offset({
            top: -400,
            left: -400
          });
        }

      }, true);

      if (buttonId != "") {
        var clickZone = $("#" + buttonId);

        var oleft = clickZone.offset().left;
        var oright = clickZone.outerWidth() + oleft;
        var otop = clickZone.offset().top;
        var obottom = clickZone.outerHeight() + otop;

        $("#" + buttonId).mousemove(function(e) {
          var x = e.pageX;
          var y = e.pageY;
          if (!(x < oleft || x > oright || y < otop || y > obottom)) {
            inputFile.offset({
              top: y - 15,
              left: x - 160
            });
          } else {
            inputFile.offset({
              top: -400,
              left: -400
            });
          }
        });
      }

      document.getElementById(dropZoneId).addEventListener("drop", function(e) {
        $("#" + dropZoneId).removeClass(mouseOverClass);
      }, true);


      inputFile.on('change', function(e) {
        finalFiles = {};
        $('#filename').html("");
        var fileNum = this.files.length,
          initial = 0,
          counter = 0;

        $.each(this.files,function(idx,elm){
           finalFiles[idx]=elm;
        });

        for (initial; initial < fileNum; initial++) {
          counter = counter + 1;
          $('#filename').append('<div id="file_'+ initial +'"><span class="fa-stack fa-lg"><i class="fa fa-file fa-stack-1x "></i><strong class="fa-stack-1x" style="color:#FFF; font-size:12px; margin-top:2px;">' + counter + '</strong></span> ' + this.files[initial].name + '&nbsp;&nbsp;<span class="fa fa-times-circle fa-lg closeBtn" onclick="removeLine(this)" title="remove"></span></div>');
        }
      });



    })

    function removeLine(obj)
    {
      inputFile.val('');
      var jqObj = $(obj);
      var container = jqObj.closest('div');
      var index = container.attr("id").split('_')[1];
      container.remove();

      delete finalFiles[index];

    }

      </script> --}}

      <script>
        $(document).ready(function(){
          $("#proceed-payment").on("click", function(){
            $(".loading-icon").removeClass("hide");
            $(".arrow-right").addClass("hide");
            // $("#proceed-payment").attr("disabled", true);
            $(".btn-txt").text("Processing");
          });
        });
        </script>

        <script type="text/javascript">
          function validateForm() {
            var a = document.forms["Form"]["filenames[]"].value;
            var b = document.forms["Form"]["fileid[]"].value;

            if (a == "" && b == "") {
              alert("Please Select Prescription File");
              // document.getElementById("textbox1").required = true;
              return false;
            }

          }
        </script>

        <script>
          // $('.my-select').selectpicker();

          </script>

@endsection