@extends('layouts.front1')
@section('title', 'Proceed to Payment')

@section('content')

    <style>
        .billing-font {
            font-size: 14px;
        }

        #pay-btn:hover {
            background: #50c06a !important;
        }

        @media (min-width: 768px) {
            .col-md-8 {
                -ms-flex: 0 0 50%;
                flex: 0 0 65.67%;
                max-width: 65.67%;
            }
        }

        @media (min-width: 768px) {
            .col-md-4 {
                -ms-flex: 0 0 32.33%;
                flex: 0 0 32.33%;
                max-width: 32.33%;
            }
        }

        .hide {
            display: none;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .box {

            padding: 20px;
            display: none;
            margin-top: 20px;
        }

        .Cash {
            background: #f7f7f7
        }

        .input-container.disabled .radio-tile {
            background-color: #f5f1f1;
        }

        .radio-tile-group .input-container .radio-button:checked+.radio-tile small {
            color: white;
        }

        .input-container.disabled input {
            cursor: not-allowed!important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1168px;
            }
        }

    </style>

    <style>
        .table-shopping .td-number {
            text-align: right;
            min-width: 45px;
        }

        .table-shopping>thead>tr>th {
            font-size: 16px;
            text-transform: capitalize;
            font-family: 'Muli';
        }

        .table {
            font-family: 'Muli';
        }

    </style>

    @php
        $user = Auth::guard('user')->user();
        $cart = unserialize(bzdecompress(utf8_decode($order->cart)));
        $products = $cart->items;
    @endphp

    @php
    $esewa = App\PaymentGateway::where('title', '=', 'Esewa')->get();
    $khalti = App\PaymentGateway::where('title', '=', 'Khalti')->get();
    $fonepay = App\PaymentGateway::where('title', '=', 'FonePay')->get();
    $imepay = App\PaymentGateway::where('title', '=', 'IMEPay')->get();
    $iPay = App\PaymentGateway::where('title', '=', 'iPay')->first();
    @endphp


    <div class="container">

        <button class="btn btn-info btn-round btn-raised" style="margin-top:20px;"><i class="material-icons">payment</i>
            Payment<div class="ripple-container"></div></button>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="">
                    <h4 class="signIn-title text-center">Payment Options</h4>


                    <div class="container" style="border-radius:30px;">
                        <div class="radio-tile-group" name="method" id="paymentMethod">

                            @php
                                $courier_charge = $order->courier_charge;
                                // dd($courier_charge);
                            @endphp
                            @if ($order->courier_charge == null || ($order->courier_charge = 0))
                                <div class="input-container">
                                    <input id="Cash" class="radio-button" type="radio" name="radio" value="Cash" />
                                    <div class="radio-tile">
                                        <div class="icon walk-icon">

                                            <img style="width:40px"
                                                src="https://www.chemstar.com/wp-content/uploads/2017/09/service-round-icon-02-delivery.png" />
                                        </div>
                                        <label for="cod" class="radio-tile-label">Cash On Delivery</label>
                                    </div>
                                </div>
                            @endif

                            @if($user)
                                <div class="input-container {{ $user->current_balance < $order->pay_amount ? 'disabled' : '' }}">
                                    <input id="Loyality" class="radio-button" type="radio" name="radio" value="Loyality" {{ $user->current_balance < $order->pay_amount ? 'disabled' : '' }} />
                                    <div class="radio-tile">
                                        <div class="icon walk-icon">

                                            <img style="width:40px" src="https://images.squarespace-cdn.com/content/v1/560b6be1e4b08fff9b76ab9b/1597645166590-YEW8E1TWNXPQR7ONARBZ/ke17ZwdGBToddI8pDm48kAf-OpKpNsh_OjjU8JOdDKBZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwkCFOLgzJj4yIx-vIIEbyWWRd0QUGL6lY_wBICnBy59Ye9GKQq6_hlXZJyaybXpCc/Loyalty+program+1.png"/>
                                        </div>
                                        <label for="Loyality" class="radio-tile-label" >Loyality Points</label>
                                        <small style="font-size: 10px;line-height: 1;text-align: center;">Rs. {{ $user->current_balance }} Available</small>
                                    </div>
                                </div>
                            @endif

                            <div class="input-container">
                                <input id="Bank" class="radio-button" type="radio" name="radio" value="Bank" />
                                <div class="radio-tile">
                                    <div class="icon walk-icon">
                                        <img style="width:40px"
                                            src="http://icons.iconarchive.com/icons/graphicloads/folded/256/bank-folded-icon.png" />
                                    </div>
                                    <label for="cod" class="radio-tile-label">Bank </label>
                                </div>
                            </div>

                            @if ($khalti[0]->status == '1')
                                <div class="input-container">
                                    <input id="walk" class="radio-button" type="radio" name="radio" value="Khalti" />
                                    <div class="radio-tile">
                                        <div class="icon walk-icon">

                                            <img style="width:40px"
                                                src="https://lh3.googleusercontent.com/vtoxj9t4UWl6qxWUPGpv7ndJuJs_W3UTnQYpBwJ7xBMuRJ2TE6d71NrwWU6Nkbq0Zs8" />
                                        </div>
                                        <label for="walk" class="radio-tile-label">Khalti</label>
                                    </div>
                                </div>
                            @endif

                            @if ($esewa[0]->status == '1')
                                <div class="input-container">
                                    <input id="bike" class="radio-button" type="radio" name="radio" value="Esewa" />
                                    <div class="radio-tile">
                                        <div class="icon bike-icon">
                                            <img style="width:40px"
                                                src="https://otsnepal.com/wp-content/uploads/2020/05/unnamed.png" />
                                        </div>
                                        <label for="bike" class="radio-tile-label">E-sewa</label>
                                    </div>
                                </div>
                            @endif

                            @if ($fonepay[0]->status == '1')
                                <div class="input-container">
                                    <input id="drive" class="radio-button" type="radio" name="radio" value="FonePay" />
                                    <div class="radio-tile">
                                        <div class="icon car-icon">
                                            <img style="width:40px"
                                                src="https://media-exp1.licdn.com/dms/image/C510BAQE-gVAYFB4FUg/company-logo_200_200/0?e=2159024400&v=beta&t=oJy0sDDwzrx3cH5KCmGMYsy7FotUix4drGCTJX4kfAA" />
                                        </div>
                                        <label for="drive" class="radio-tile-label">fone pay</label>
                                    </div>
                                </div>
                            @endif

                            @if ($imepay[0]->status == '1')
                                <div class="input-container">
                                    <input id="fly" class="radio-button" type="radio" name="radio" value="IMEPay" />
                                    <div class="radio-tile">
                                        <div class="icon fly-icon">
                                            <img style="width:40px"
                                                src="https://www.merohealthcare.com/assets/images/1597057079117688840_355124495888664_8627824190237150414_n.jpg" />
                                        </div>
                                        <label for="fly" class="radio-tile-label">IME Pay</label>
                                    </div>
                                </div>
                            @endif

                            @if ($iPay->status)
                                <div class="input-container">
                                    <input id="iPay" class="radio-button" type="radio" name="radio" value="iPay" />
                                    <div class="radio-tile">
                                        <div class="icon fly-icon">
                                            <img style="width:40px"
                                                src="https://www.merohealthcare.com/assets/images/1599126542107-1079686_transparent-visa-mastercard-png-visa-and-mastercard-logo.png" />
                                        </div>
                                        <label for="iPay" class="radio-tile-label">iPay</label>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div id="gateway" style="display: none;"></div>

                    <div class="text-center" style="margin-top: 10px;">
                        <div class="form-group">
                            <button class="btn btn-md btn-success" id="pay-btn" type="button" disabled
                                title="Proceed to make Purchase"><i class="icon-check"></i> <span class="btn-txt"><i
                                        class="material-icons">check_circle</i> <span
                                        style="font-size:14px; font-weight:800;">Proceed to Pay (<span
                                            style="text-transform: capitalize;">{{ $order->currency_sign }}</span> <span
                                            id="total-cost">{{ round($order->pay_amount * $order->currency_value, 2) }}/-)</span></span></span></button>
                        </div>
                    </div>

                    <div class="Bank box" style="background: white;" id="scrollto">
                        <div class="row">
                            <div class="col-md-6">
                                <img style="width:100%"
                                    src="https://www.merohealthcare.com/assets/images/1600940568Screen%20Shot%202020-09-24%20at%2015.27.06.png" />
                            </div>
                            <div class="col-md-6">
                                <h6 style="font-weight:700; ">Please deposit the amount in bank details given below </h6>


                                <li class="media g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1"
                                    style="border-radius:30px;">
                                    <div class="d-flex g-mt-2 g-mr-15">
                                        <img style="width:75px;height:80px;"
                                            src="https://www.collegenp.com/uploads/2018/12/Sanima-Bank.jpg"
                                            alt="Image Description">
                                    </div>
                                    <div class="media-body">
                                        <div class="d-flex justify-content-between">
                                            <strong class="g-color-teal">Sanima Bank</strong>
                                            {{-- <span class="align-self-center small text-nowrap g-color-lightred">56 min ago</span> --}}
                                        </div>
                                        <span class="d-block"><strong>Account name : </strong> Web Health Company Pvt
                                            Ltd</span>
                                        <span class="d-block"><strong>AC. No.</strong> 909010020000028</span>
                                        <span class="d-block"><strong>Branch : </strong> Head Office</span>
                                    </div>
                                </li>

                                <div class="container" style="margin-top:10px;">

                                    <p><b>Payment Ways:</b><br />1. Pay through online bank transfer. <br />2. Or, Bank
                                        voucher deposit to nearest branch.</p>
                                    <i><b>Note: Show your transfer/voucher receipt after the delivery.</b></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <hr>
        <button class="btn btn-info btn-round btn-raised" style="margin-top:20px;"><i class="material-icons">list</i> Order
            Details<div class="ripple-container"></div></button>

        <div class="row">
            <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                <div class="card card-nav-tabs" style="margin-top:60px;">
                    <div class="card-header card-header-rose" style="font-size:20px; font-weight:500; box-shadow:1px 1px 0px 0px rgb(0 0 0 / 20%), -1px 1px 0px -11px rgb(233 30 99 / 60%);
                  } ">
                        Billing Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title">{{ $order->customer_name }}</h4>
                                <p class="card-text">{{ $order->customer_email }}</p>
                                <p class="card-text">{{ $order->customer_phone }}</p>
                            </div>
                            <div class="col-md-6">
                                <h4 class="card-title">Address</h4>
                                <p class="card-text">{{ $order->customer_address }}</p>
                                <p class="card-text">{{ $order->district }}, {{ $order->customer_country }}</p>

                            </div>
                        </div>

                    </div>
                </div>

                {{-- <div class="container" style="margin-bottom:20px;font-size:12px;background:cornsilk;border-radius:10px;box-shadow:0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%);" id="billDetail">
                <h3 class="title" style="padding-top :7px;margin-bottom: 5px">{{$lang->bdetails}}</h3>
                <span class="billing-font">{{ $order->customer_name }}</span><br>
                <span class="billing-font">{{ $order->customer_email }}</span><br>
                <span class="billing-font">{{ $order->customer_phone }}</span><br>
           
                <span class="billing-font">{{ $order->customer_address }}</span><br>
                <span class="billing-font">{{ $order->district }}</span> ,
                <span class="billing-font">{{ $order->customer_country }}</span><br>
                  </div> --}}
                <div class="table-responsive" style="margin-top:25px;">
                    <table class="table table-shopping">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Name</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Amount</th>
                            </tr>

                        </thead>
                        <tbody>
                            @php
                                $priceDiscount = 0;
                                $vat_sum = 0;
                            @endphp
                            @foreach ($products as $id => $product)
                                @php
                                    $price = $product['item']['pprice'] ? $product['item']['pprice'] : $product['item']['cprice'];
                                    $vat = App\Product::findORFail($product['item']['id']);
                                @endphp
                                <tr>
                                    <td>
                                        <div class="img-container" style="max-height:500px">
                                            <a href="{{ route('front.product', [$product['item']['id'], $product['item']['slug']]) }}"
                                                style="font-weight: 500; font-size:15px;" target="_blank">
                                                <img style="height:65px;width:auto;"
                                                    src="{{ asset('assets/images/' . $product['item']['photo']) }}"
                                                    alt="{{ $product['item']['name'] }}"><br>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="img-container" style="max-height:500px">
                                            <a href="{{ route('front.product', [$product['item']['id'], $product['item']['slug']]) }}"
                                                style="font-weight: 500; font-size:15px;" target="_blank">
                                                {{ ucwords(strtolower($product['item']['name'])) }}
                                                @if ($vat->vat_status == 1)
                                                    <span style="font-style: italic;">*</span>
                                                @endif
                                            </a>
                                            <h6 style="text-transform:capitalize;">Varient : {{ $vat->sub_title }}</h6>
                                            <h6>{{ $vat->product_quantity }}</h6>
                                        </div>
                                    </td>



                                    <input type="hidden" id="stock{{ $product['item']['id'] }}"
                                        value="{{ $product['stock'] }}">

                                    <td class="td-number text-center">
                                        @if ($product['item']['cprice'])
                                            <span style="font-size:16px;">
                                                {{ $curr->sign }}{{ $product['item']['cprice'] }}
                                            </span>
                                        @endif
                                        @if ($product['item']['pprice'])
                                            <del style="font-size:14px;color:red">
                                                @if ($gs->sign == 0)
                                                    {{ $curr->sign }}{{ round($price * $curr->value, 2) }}
                                                @else
                                                    {{ round($price * $curr->value, 2) }}{{ $curr->sign }}
                                                @endif
                                            </del>
                                        @endif
                                    </td>
                                    <td class="td-number text-center">
                                        <span class="badge badge-pill badge-info" style="font-size:15px;"
                                            id="qty{{ $product['item']['id'] }}">{{ $product['qty'] }}</span>

                                    </td>
                                    <td class="td-number text-center" style="font-size:16px;">
                                        @if ($gs->sign == 0)
                                            {{ $curr->sign }}<span
                                                id="prc{{ $product['item']['id'] }}">{{ round($product['price'] * $curr->value, 2) }}</span>

                                        @else
                                            <span
                                                id="prc{{ $product['item']['id'] }}">{{ round($product['price'] * $curr->value, 2) }}</span>{{ $curr->sign }}

                                        @endif
                                    </td>

                                    @php
                                        $priceDiscount += round($price * $product['qty'] * $curr->value, 2) - round($product['price'] * $curr->value, 2);
                                    @endphp

                                </tr>
                                @php
                                    if ($order->coupon_code != null) {
                                        $coupon_dis = App\Coupon::where('code', $order->coupon_code)->first();
                                    }
                                    // dd($product);
                                    if ($vat->vat_status == 1) {
                                        if ($order->coupon_code) {
                                            //percentage discount
                                            if ($coupon_dis->type == 0) {
                                                $vat_price = $product['price'] * (1 - $coupon_dis->price / 100) * $curr->value;
                                                $vat_sum = $vat_sum + $vat_price;
                                            }
                                            //price discount
                                            // elseif($coupon_dis->type == 1){
                                            //   $vat_price = $product['price'] - $coupon_dis->price * $curr->value;
                                            //   $vat_sum = $vat_sum + $vat_price;
                                            // }
                                            //  dd($vat_sum);
                                        } else {
                                            $vat_price = $product['price'] * $curr->value;
                                            $vat_sum = $vat_sum + $vat_price;
                                            // dd($vat_sum);
                                        }
                                    }
                                @endphp
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>


            <div class="col-lg-4 col-md-12 ml-auto mr-auto">
                <div class="card card-pricing card-info bg-info">
                    <div class="card-body">
                        <button class="btn btn-white btn-block"><i class="material-icons">shopping_cart</i> Order
                            Summary</button>

                        <div class="container" style="color: white; font-size:16px; margin-top:15px;">
                            <div style="display: flex; justify-content:space-between !important;">
                                <span>Subtotal</span>
                                <span>{{ $curr->sign }}{{ round(($order->pay_amount + $priceDiscount + $order->coupon_discount - $order->shipping_cost - $courier_charge) * $curr->value, 2) }}</span>
                            </div>

                            @if ($priceDiscount > 0)
                                <div style="display: flex; justify-content:space-between !important;">
                                    <span>Price Discount</span>
                                    <span>- {{ $curr->sign }}<span>{{ $priceDiscount }}</span>
                                </div>
                            @endif

                            @if ($order->coupon_code)
                                @php
                                    $coupon_code = App\Coupon::where('code', $order->coupon_code)->first();
                                    $sign = App\Currency::where('is_default', '=', 1)->first();
                                @endphp
                                <div style="display: flex; justify-content:space-between !important;" id="discount">
                                    <span class="text-left"> {{ $lang->ds }} (<span
                                            id="sign">{{ $order->coupon_code }}
                                            {{ $coupon_code->type == 0 ? $coupon_code->price . '%' : $sign->sign . round($coupon_code->price * $sign->value, 2) }}</span>)</span>
                                    <span> - {{ $order->currency_sign }}<span
                                            id="ds">{{ $order->coupon_discount }}</span></span>
                                </div>
                            @endif

                            @if ($order->shipping_cost > 0)
                                <div style="display: flex; justify-content:space-between !important;">
                                    <span>Delivery Charge</span>
                                    <span>{{ $curr->sign }}<span
                                            id="ship-cost">{{ round($order->shipping_cost * $curr->value, 2) }}</span></span>
                                </div>
                            @endif


                            @if ($courier_charge > 0)
                                <div style="display: flex; justify-content:space-between !important;">
                                    <span>Courier Charge</span>
                                    <span>{{ $curr->sign }} {{ $courier_charge }}</span></span>
                                </div>
                            @endif

                            <hr>
                            <div style="display: flex; justify-content:space-between !important; font-weight:700;">
                                <span>GrandTotal</span>
                                <span>{{ $order->currency_sign }}<span
                                        id="total-cost">{{ round($order->pay_amount * $order->currency_value, 2) }}</span></span>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="card card-pricing card-info bg-warning">
                    <div class="card-body">
                        <button class="btn btn-white btn-block"><i class="material-icons">text_snippet</i> Tax
                            Summary</button>
                        <div class="container">
                            <div style="display: flex; justify-content:space-between !important;">
                                <span>Taxable Amount</span>
                                <span>{{ $curr->sign }}<span
                                        id="">{{ round(($vat_sum / 1.13) * $curr->value, 2) }}</span>
                            </div>
                            <div style="display: flex; justify-content:space-between !important;">
                                <span>VAT({{ $gs->vat }}%)</span>
                                <span>{{ $curr->sign }}<span
                                        id="">{{ round(($vat_sum / 1.13) * 0.13 * $curr->value, 2) }}</span></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bank Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img style="width:100%"
                        src="https://www.merohealthcare.com/assets/images/1600940568Screen%20Shot%202020-09-24%20at%2015.27.06.png" />
                    <h6 style="font-weight:700; ">Please deposit the amount in bank details given below </h6>


                    <li class="media g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1" style="border-radius:15px;">
                        <div class="d-flex g-mt-2 g-mr-15">
                            <img style="width:75px;height:80px;"
                                src="https://www.collegenp.com/uploads/2018/12/Sanima-Bank.jpg" alt="Image Description">
                        </div>
                        <div class="media-body">
                            <div class="d-flex justify-content-between">
                                <strong class="g-color-teal">Sanima Bank</strong>
                                {{-- <span class="align-self-center small text-nowrap g-color-lightred">56 min ago</span> --}}
                            </div>
                            <span class="d-block"><strong>Account name : </strong> Web Health Company Pvt Ltd</span>
                            <span class="d-block"><strong>AC. No.</strong> 909010020000028</span>
                            <span class="d-block"><strong>Branch : </strong> Head Office</span>

                        </div>
                    </li>
                    <div class="container" style="margin-top:10px;">

                        <p><b>Payment Ways:</b><br />1. Pay through online bank transfer. <br />2. Or, Bank voucher deposit
                            to nearest branch.</p>
                        <i><b>Note: Show your transfer/voucher receipt after the delivery.</b></i>
                    </div>



                </div>
                <div class="text-center" style="padding:10px;">
                    <button type="button" class="btn btn-primary btn-block text-center" data-dismiss="modal">Got It</button>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://khalti.com/static/khalti-checkout.js"></script>

    <script>
        $('input[name="radio"]').change(function() {
            if ($(this).is(':checked') && $(this).val() == 'Bank') {
                $('#myModal').modal('show');
            }
        });

        // $(document).ready(function () {

        //     $('html, body').animate({
        //         scrollTop: $('#billDetail').offset().top
        //     }, 'slow');
        // });

    </script>

    <script type="text/javascript">
        var total = {{ $order->pay_amount }};
        var id = '{{ $order->order_number }}';
        // var discount = option.attr('discount');
        // $('#total-cost').html((total - discount).toFixed(2))
        $("#paymentMethod").on('click', function() {

            var option = $('input[name="radio"]:checked', this);
            var discount = option.attr('discount');
            var gateway = option.val();

            // $('#total-cost').html((total - discount).toFixed(2))
            $('#pay-btn').attr('disabled', 'disabled');

            if (discount > 0) {

                $("#gatewaydiscount").show("slow");
                $("#gatewayds").text(discount);

            } else {
                $("#gatewaydiscount").hide();

                $("#gatewayds").text(0);
            }

            // $('#total-cost').html((total - discount).toFixed(2))

            if (gateway == 'Bank') {
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');


                var btn = document.getElementById("pay-btn");
                btn.onclick = function(e) {
                    e.preventDefault();
                    var form =
                        '<form action="/checkout/{{ $order->order_number }}/gateway/bank" method="POST">{{ csrf_field() }}</form>';
                    $(form).appendTo('body').submit();
                }
                return;
            } else if (gateway == 'Cash') {
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');


                var btn = document.getElementById("pay-btn");
                btn.onclick = function(e) {
                    e.preventDefault();
                    var form =
                        '<form action="/checkout/{{ $order->order_number }}/gateway/cod" method="POST">{{ csrf_field() }}</form>';
                    $(form).appendTo('body').submit();
                }
                return;
            } else if(gateway == 'Loyality'){
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');


                var btn = document.getElementById("pay-btn");
                btn.onclick = function (e) {
                    e.preventDefault();
                    var form = '<form action="/checkout/{{$order->order_number}}/gateway/loyality" method="POST">{{ csrf_field() }}</form>';
                    $(form).appendTo('body').submit();
                }
                return;
            }else if (gateway == 'FonePay') {
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');

                var btn_fonepay = document.getElementById("pay-btn");
                btn_fonepay.onclick = function(e) {
                    e.preventDefault();
                    $('#pay-btn').attr('disabled', 'disabled');

                    $('#overlay').css('display', 'block');
                    $.ajax({
                        type: "POST",
                        url: "/checkout/" + id + "/gateway/fonepay",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(result) {
                            location.href = result.url;
                            $('#pay-btn').removeAttr('disabled');
                        },
                        error: function(data) {
                            $('#pay-btn').removeAttr('disabled');

                            $.notify("Something went wrong.", "error");
                        }
                    });
                }
            } else if (gateway == 'Esewa') {

                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');

                var btn_esewa = document.getElementById("pay-btn");
                btn_esewa.onclick = function(e) {
                    e.preventDefault();
                    $('#overlay').css('display', 'block');
                    $('#pay-btn').attr('disabled', 'disabled');

                    $.ajax({
                        type: "POST",
                        url: "/checkout/" + id + "/gateway/esewa",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(result) {
                            var html = '<form action="' + result.url + '" method="POST">' +
                                '  <input type="hidden" name="tAmt" value="' + result.tAmt + '">' +
                                '  <input type="hidden" name="amt" value="' + result.amt + '">' +
                                '  <input type="hidden" name="txAmt" value="' + result.txAmt +
                                '">' +
                                '  <input type="hidden" name="psc" value="' + result.psc + '">' +
                                '  <input type="hidden" name="pdc" value="' + result.pdc + '">' +
                                '  <input type="hidden" name="scd" value="' + result.scd + '">' +
                                '  <input type="hidden" name="pid" value="' + result.pid + '">' +
                                '  <input type="hidden" name="su" value="' + result.su + '">' +
                                '  <input type="hidden" name="fu" value="' + result.fu + '">' +
                                '</form>';

                            jQuery(html).appendTo('body').submit();
                            $('#pay-btn').removeAttr('disabled');
                        },
                        error: function(data) {
                            $('#pay-btn').removeAttr('disabled');

                            $.notify("Something went wrong.", "error");
                        }
                    });
                }
            } else if (gateway == 'IMEPay') {
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');

                var btn_imepay = document.getElementById("pay-btn");
                btn_imepay.onclick = function(e) {
                    e.preventDefault();
                    $('#overlay').css('display', 'block');
                    $('#pay-btn').attr('disabled', 'disabled');

                    $.ajax({
                        type: "POST",
                        url: "/checkout/" + id + "/gateway/imepay",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(result) {
                            var html = '<form action="' + result.url + '" method="POST">' +
                                '  <input type="hidden" name="TokenId" value="' + result.TokenId +
                                '">' +
                                '  <input type="hidden" name="MerchantCode" value="' + result
                                .MerchantCode + '">' +
                                '  <input type="hidden" name="RefId" value="' + result.RefId +
                                '">' +
                                '  <input type="hidden" name="TranAmount" value="' + result
                                .TranAmount + '">' +
                                '  <input type="hidden" name="Method" value="' + result.Method +
                                '"></input>' +
                                '  <input type="hidden" name="RespUrl" value="' + result.RespUrl +
                                '"></input>' +
                                '  <input type="hidden" name="CancelUrl" value="' + result
                                .CancelUrl + '"></input>' +
                                '</form>';
                            jQuery(html).appendTo('body').submit();
                            $('#pay-btn').removeAttr('disabled');
                        },
                        error: function(data) {
                            $('#pay-btn').removeAttr('disabled');

                            $.notify("Something went wrong.", "error");
                        }
                    });
                }
            } else if (gateway == 'Khalti') {
                $.ajax({
                    type: "POST",
                    url: "/checkout/" + id + "/gateway/khalti",
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        $('#gateway').html(data);
                        $("#gateway").show();
                        $('#pay-btn').removeAttr('disabled');
                    },
                    error: function(data) {
                        $('#gateway').html('');
                        $.notify("Something went wrong.", "error");
                    }
                });
            } else if (gateway == 'iPay') {
                $('#pay-btn').removeAttr('disabled');
                $('#gateway').html('');


                var btn = document.getElementById("pay-btn");
                btn.onclick = function(e) {
                    e.preventDefault();
                    var form =
                        '<form action="/checkout/{{ $order->order_number }}/gateway/card" method="POST">{{ csrf_field() }}</form>';
                    $(form).appendTo('body').submit();
                }
                return;
            } else
                $.notify('Select a valid payment gateway.', 'error')
        });

        var showErrors = function(response) {


            if (response.message) {
                $.notify(response.message, "error");
            } else if (response.error) {
                $.notify(response.error, "error");
            }
        }

    </script>

    <script>
        // $(document).ready(function(){
        //     $("select").change(function(){
        //         $(this).find("option:selected").each(function(){
        //             var optionValue = $(this).attr("value");
        //             if(optionValue){
        //                 $(".box").not("." + optionValue).hide();
        //                 $("." + optionValue).show();
        //             } else{
        //                 $(".box").hide();
        //             }
        //         });
        //     }).change();
        // });


        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                var inputValue = $(this).attr("value");
                var targetBox = $("." + inputValue);
                $(".box").not(targetBox).hide();
                $(targetBox).show();
            });
        });

    </script>

    <!-- JS Implementing Plugins -->
    <script src="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>

    <!-- JS Unify -->
    <script src="/frontend-assets/main-assets/assets/js/components/hs.popup.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function() {
            // initialization of popups
            //  $.HSCore.components.HSPopup.init('.js-fancybox');
        });

    </script>

    <script>
        $(document).ready(function() {
            $(".order-btn").on("click", function() {
                $(".loading-icon").removeClass("hide");
                $(".order-btn").attr("disabled", true);
                $(".btn-txt").text("Processing");
            });
        });

    </script>

    <script>
        $('input:radio[name="radio"]').on('change.firstChange',
            function() {
                $('#changes')[0].innerHTML += 'first ';
                $('input:radio[name="radio"]').off('change.firstChange');
                $('html, body').animate({
                    scrollTop: $("#scrollto").offset().top
                }, 1500);
            }
        );
        $('input:radio[name="radio"]').on('change.allChanges',
            function() {
                $('#changes')[0].innerHTML += 'changed ';
            }
        );

    </script>
@endsection
