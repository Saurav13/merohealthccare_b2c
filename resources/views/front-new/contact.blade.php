@extends('layouts.front1')
@section('title','Contact Us')

@section('styles')


@endsection

@section('content')

    <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3">
		<div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div><script>(function () {
        var setting = {"height":400,"width":1920,"zoom":16,"queryString":"Model Plaza, Kathmandu, Nepal","place_id":"ChIJ32SnsDkZ6zkRmtUfNzVitIY","satellite":false,"centerCoord":[27.719415558344217,85.3044467],"cid":"0x86b46235371fd59a","lang":"en","cityUrl":"/nepal/kathmandu-5536","cityAnchorText":"Map of Kathmandu, Lalitpur, Nepal","id":"map-9cd199b9cc5410cd3b1ad21cab2e54d3","embed_id":"215758"};
        var d = document;
        var s = d.createElement('script');
        s.src = 'https://1map.com/js/script-for-user.js?embed_id=215758';
        s.async = true;
        s.onload = function (e) {
          window.OneMap.initMap(setting)
        };
        var to = d.getElementsByTagName('script')[0];
        to.parentNode.insertBefore(s, to);
      })();</script><a href="https://1map.com/map-embed">1 Map</a></div>


    <!-- Starting of contact us area -->
    
    
    <div class="section-padding contact-area-wrapper">
        <div class="container">
            <div class="row">
                @if($lang->rtl == 1)
                <div class="col-md-4 col-md-offset-1 col-sm-5">
                    <div class="contact-info" style="margin-top:100px;">
                            @if($gs->street != null)   
                          <p class="contact-info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                {{$gs->street}}
                            </p>
                            @endif

                            @if($gs->phone != null || $gs->fax != null ) 
                            <p class="contact-info">

                                <i class="fa fa-phone" aria-hidden="true"></i>
                                @if($gs->phone != null && $gs->fax != null)
                                <a dir="rtl" href="tel:{{$gs->phone}}">{{$gs->phone}}</a>
                                <br>
                                <a dir="rtl" href="tel:{{$gs->fax}}">{{$gs->fax}}</a>
                                @elseif($gs->phone != null)
                            <a dir="rtl" href="tel:{{$gs->phone}}">{{$gs->phone}}</a>

                                @else($gs->fax != null)
                                <a dir="rtl" href="tel:{{$gs->fax}}">{{$gs->fax}}</a>
                                @endif

                            </p>
                            @endif

                            @if($gs->site != null || $gs->email != null )
                            <p class="contact-info">                               
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                @if($gs->site != null && $gs->email != null) 
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                                <br>
                                <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                                @elseif($gs->site != null)
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                                @else
                                <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                                @endif                                                                
                            </p>
                            @endif
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                        <h3 dir="{{$lang->rtl == 1 ? 'rtl':''}}">{{$ps->contact_title}}</h3>
                        {{-- <p  dir="{{$lang->rtl == 1 ? 'rtl':''}}">{!!$ps->contact_text!!}</p> --}}
                    <div class="comments-area">
                        @include('includes.form-success')
                        @include('includes.form-error')
                        <div class="comments-form">
                            <form action="{{route('front.contact.submit')}}" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                          

                                    <div class="col-md-6">
                                        <input name="name" placeholder="{{$lang->con}}" required="" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="phone" placeholder="{{$lang->cop}}" type="tel">
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <input name="email" placeholder="{{$lang->coe}}" required="" type="email">
                                        </div>
                                </div>
                                    <p><textarea name="text" id="comment" placeholder="{{$lang->cor}}" cols="30" rows="10" style="resize: vertical;" required=""></textarea></p>
                                    <div class="row">
                                        @if($lang->rtl == 1)
                                        <div class="col-md-2 col-md-offset-6 col-sm-2 col-sm-offset-4 col-xs-2 col-xs-offset-4">
                                            <span style="cursor: pointer; float: right;" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6">
                                            <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                        </div>
                                        @else
                                        <div class="col-md-4 col-sm-6 col-xs-6">
                                            <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <span style="cursor: pointer;" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
                                        </div>
                                        @endif
                                    </div>
                                    @if($lang->rtl == 1)
                                    <div class="row">
                                    <div class="col-md-4 col-md-offset-8 col-sm-6 col-sm-offset-6 col-xs-8 col-xs-offset-4">

                                            <input name="codes" placeholder="{{$lang->enter_code}}" required="" type="text">
                                           <input style="float: {{$lang->rtl == 1 ? 'right':''}};" name="contact_btn" value="{{$lang->sm}}" type="submit">
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-8">

                                            <input name="codes" placeholder="{{$lang->enter_code}}" required="" type="text">
                                           <input  name="contact_btn" value="{{$lang->sm}}" type="submit">
                                        </div>
                                    </div>
                                    @endif

                            </form>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-md-7 col-sm-7">
                        <h3 class="title" dir="{{$lang->rtl == 1 ? 'rtl':''}}">Send us a message</h3>
                        <p class="description">You can contact us with anything related to our Products. We'll get in touch with you as soon as possible.<br><br>
                        </p>
                        {{-- <p  dir="{{$lang->rtl == 1 ? 'rtl':''}}">{!!$ps->contact_text!!}</p> --}}
                    <div class="comments-area">
                        @include('includes.form-success')
                        @include('includes.form-error')
                        <div class="comments-form">
                            <form action="{{route('front.contact.submit')}}" method="POST">
                                {{csrf_field()}}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <div class="input-group">
                                              <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="material-icons">face</i>
                                                </span>
                                              </div>
                                              <input type="text" class="form-control" name="name" placeholder="Full Name" value="" required="">
                                            </div>
                                          </div>

                                        {{-- <input name="name" placeholder="{{$lang->con}}" required="" type="text"> --}}
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <div class="input-group">
                                              <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="material-icons">phone</i>
                                                </span>
                                              </div>
                                              <input type="tel" class="form-control" name="phone" placeholder="Phone" value="" required="">
                                            </div>
                                          </div>
                                        {{-- <input name="phone" placeholder="{{$lang->cop}}" type="tel"> --}}
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="material-icons">mail</i>
                                                    </span>
                                                  </div>
                                                  <input type="email" class="form-control" name="email" placeholder="yourmail@example.com" value="" required="">
                                                </div>
                                              </div>
                                            {{-- <input name="email" placeholder="{{$lang->coe}}" required="" type="email"> --}}
                                        </div>
                                </div>
                                <div class="form-group bmd-form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="material-icons">question_answer</i>
                                        </span>
                                      </div>
                                      <textarea class="form-control" name="text" id="comment" placeholder="Write a Reply..." rows="2" required=""></textarea>
                                    </div>
                                  </div>

                                    {{-- <p>
                                        <textarea name="text" id="comment" placeholder="{{$lang->cor}}" cols="30" rows="10" style="resize: vertical;" required=""></textarea>
                                    </p> --}}
                                    <div class="row">
                                        @if($lang->rtl == 1)
                                        <div class="col-md-4 col-md-offset-6 col-sm-2 col-sm-offset-4 col-xs-2 col-xs-offset-4">
                                          <div class="ml-5">
                                            <span style="cursor: pointer; float: right;" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
                                          </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-6">
                                          <div class="ml-5">
                                            <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-md-4 col-sm-6 col-xs-6">
                                          <div class="ml-5">
                                            <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                          </div>
                                        </div>
                                        <div class="col-md-4 col-sm-2 col-xs-2">
                                          <div class="ml-5">
                                            <span id="captcha" style="cursor: pointer;" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
                                          </div>
                                          </div>
                                        @endif
                                    </div>
                                    @if($lang->rtl == 1)
                                    <div class="row">
                                    <div class="col-md-4 col-md-offset-8 col-sm-6 col-sm-offset-6 col-xs-8 col-xs-offset-4 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                          </div>
                                            <input class="form-control" name="codes" placeholder="{{$lang->enter_code}}" required="" type="text">
                                        </div>
                                        <div class="pt-3 ml-5">
                                            <input class="btn btn-info" style="float: {{$lang->rtl == 1 ? 'right':''}};" name="contact_btn" value="{{$lang->sm}}" type="submit">
                                        </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-8">
                                        <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                          </div>
                                            <input class="form-control" name="codes" placeholder="{{$lang->enter_code}}" required="" type="text">
                                        </div>
                                        <div class="pt-3 ml-5">
                                            <input class="btn btn-info"  name="contact_btn" value="{{$lang->sm}}" type="submit" style="border-radius:30px;">
                                        </div>
                                          </div>
                                    </div>
                                    @endif

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ml-auto">
                    <div class="info info-horizontal">
                      <div class="icon icon-primary">
                        <i class="material-icons">pin_drop</i>
                      </div>
                      <div class="description">
                        <h4 class="info-title">Find us at the office</h4>
                        <p> {{$gs->street}}</p>
                        <p class="contact-info">                               
                           
                            @if($gs->site != null && $gs->email != null) 
                            <a href="{{$gs->site}}">{{$gs->site}}</a>
                            <br>
                            <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                            @elseif($gs->site != null)
                            <a href="{{$gs->site}}">{{$gs->site}}</a>
                            @else
                            <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                            @endif                                                                
                        </p>
                      </div>
                    </div>
                    <div class=" info-horizontal">
                      <div class="icon icon-primary">
                        <i class="material-icons">phone</i>
                      </div>
                      <div class="description">
                        <h4 class="info-title">Give us a ring</h4>
                        <p>{{$gs->phone}}</p>
                      </div>
                    </div>
                
                  </div>
                {{-- <div class="col-md-4 col-md-offset-1 col-sm-5">
                    
                    <div class="contact-info" style="margin-top:100px;">
                            @if($gs->street != null)   
                          <p class="contact-info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                {{$gs->street}}
                            </p>
                            @endif

                            @if($gs->phone != null || $gs->fax != null ) 
                            <p class="contact-info">

                                 <i class="fa fa-phone" aria-hidden="true"></i>
                                @if($gs->phone != null && $gs->fax != null)
                                <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a>
                                <br>
                                <a href="tel:{{$gs->fax}}">{{$gs->fax}}</a>
                                @elseif($gs->phone != null)
                            <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a>

                                @else($gs->fax != null)
                                <a href="tel:{{$gs->fax}}">{{$gs->fax}}</a>
                                @endif

                            </p>
                            @endif

                            @if($gs->site != null || $gs->email != null )
                            <p class="contact-info">                               
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                @if($gs->site != null && $gs->email != null) 
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                                <br>
                                <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                                @elseif($gs->site != null)
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                                @else
                                <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                                @endif                                                                
                            </p>
                            @endif
                    </div>
                </div> --}}
                @endif

            </div>
        </div>
    </div>
    <!-- Ending of contact us area -->

@endsection


@section('scripts')
    <script>
        $('.refresh_code').click(function () {
            $.get('{{url('contact/refresh_code')}}', function(data, status){
                $('#codeimg').attr("src","{{url('assets/images')}}/capcha_code.png?time="+ Math.random());
            });
        })
    </script>

<script>
  jQuery(function(){
     jQuery('#captcha').click();
  });
  </script>
@stop