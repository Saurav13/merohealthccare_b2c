@extends('layouts.front1')
@section('title', 'Ask A Doctor | Enquiry')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card card-blog">
                    <div class="card-header card-header-image">
                        <a href="#pablo">
                            <img class="img"
                                src="{{ $doctor->photo ? asset('assets/images/' . $doctor->photo) : 'http://fulldubai.com/SiteImages/noimage.png' }}"
                                alt="{{ $doctor->name }}">
                            <div class="card-title">
                                <span class="badge badge-pill badge-success" style="font-size:15px;"> <strong>NMC No :
                                        {{ $doctor->nmc }}</strong> </span>
                            </div>
                        </a>
                        <div class="colored-shadow"
                            style="background-image: url('https://merohealthcare.com/assets/images/1609151472WhatsApp Image 2020-12-28 at 4.03.42 PM.jpeg'); opacity: 1;">
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="card-category text-rose">{{ $doctor->post }}</h6>
                        <h4 class="card-title">
                            <a
                                href="{{ route('user-askdoctor-doctor', ['id' => $doctor->id, 'slug' => str_slug($doctor->name, '-')]) }}">{{ $doctor->name }}</a>
                        </h4>

                        <p class="card-description">
                            {{ $doctor->description }}
                        </p>
                    </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6">
                @if (!Auth::guard('user')->check())
                    <div class="card card-pricing bg-info">
                        <div class="card-body ">

                            <div class="icon icon-info">
                                <i class="material-icons">people</i>
                            </div>
                            <h3 class="card-title text-rose">Login Required</h3>
                            <p class="card-description">
                                Please Login to use ask doctor.
                            </p>
                            <a href="{{ route('user-login') }}" class="btn btn-warning btn-round">Login</a>
                        </div>
                    </div>
                @endif

                @if (Auth::guard('user')->check())
                    @php
                        $user = Auth::guard('user')->user();
                    @endphp

                    <div class="title">
                        <h5>Enquiry Details</h5>
                    </div>

                    @include('includes.form-error')

                    <form action="{{ route('user-askdoctor') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group bmd-form-group is-filled">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">mail</i>*
                                    </span>
                                </div>
                                <input type="email" class="form-control" name="email"
                                    value="{{ Auth::guard('user')->check() ? $user->email : old('email') }}"
                                    placeholder="yourmail@example.com" required="">
                            </div>
                        </div>

                        <div class="form-group bmd-form-group is-filled">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">phone</i>*
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="phone"
                                    value="{{ Auth::guard('user')->check() ? $user->phone : old('phone') }}"
                                    placeholder="Phone Number eg:9841000000 " required="">
                            </div>
                        </div>

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">notes</i>*
                                    </span>
                                </div>
                                <textarea class="form-control" name="query" placeholder="Write your queries here...."
                                    rows="2" required=""></textarea>
                            </div>
                        </div>

                        @include('includes.form-success')

                        <div class="title">
                            <h5>Enter Code </h5>
                        </div>
                        <img style="margin-left:20px;" id="codeimg" src="{{ url('assets/images') }}/capcha_code.png">
                        <span id="captcha" style="cursor: pointer;padding:10px;" class="refresh_code"><i
                                class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">keyboard</i>*
                                    </span>
                                </div>
                                <input class="form-control rounded-0 " name="codes" placeholder="{{ $lang->enter_code }}"
                                    required="" type="text">
                            </div>
                        </div>


                        <div class="text-center">
                            <input name="doctor_email" value="{{ $doctor->email }}" hidden>
                            <input name="doctor_name" value="{{ $doctor->name }}" hidden>
                            <input name="doctor_id" value="{{ $doctor->id }}" hidden>

                            <h6>Complete privacy and anonymity guaranteed • Quick responses </h6>
                            <button type="submit" class="btn btn-info"><i class="material-icons">send</i> Ask Question
                                Securely</button>
                        </div>
                    </form>
                @endif



            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $('.refresh_code').click(function() {
            $.get('{{ url('askdoctor/refresh_code') }}', function(data, status) {
                $('#codeimg').attr("src", "{{ url('assets/images') }}/capcha_code.png?time=" + Math
                .random());
            });
        })

    </script>
    <script>
        jQuery(function() {
            jQuery('#captcha').click();
        });

    </script>
@endsection
