@include('layouts.partials.header1')
@include('layouts.partials.navbar1')

@yield('content')

@include('layouts.partials.footer1')
@include('includes.form-error')
@yield('js')

@if($gs->is_talkto == 1)
    <!--Start of Tawk.to Script-->
    {!! $gs->talkto !!}
    <!--End of Tawk.to Script-->
@endif

@yield('scripts')

</body>
</html>
