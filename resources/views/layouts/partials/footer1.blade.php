
<style>
    .back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 20px;
    left: 20px;
    display:none;
    z-index:100000;
   
}
</style>

<a id="back-to-top" href="#" class="btn btn-info btn-lg btn-round back-to-top" style="padding:1.125rem;" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i class="material-icons">arrow_upward</i></a>

<div class="bg-primary">
    <div class="r_container_1" >
        <div class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="footer_info_div" >
                    <div class="info_icon" >
                        <i class="material-icons material-icons-outlined" style="font-size:20px;">local_shipping</i>
                    </div>
                    <div class="info_desc">
                        <h3>FREE DELIVERY</h3>
                        <p>Free Delivery on purchase over NPR 500 (inside Kathmandu Valley only).
                            Outside valley delivery charges are subjected by delivery place and purchase quantity.
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="footer_info_div">
                    <div class="info_icon">
                        <i class="material-icons material-icons-outlined" style="font-size:20px;">account_balance</i>
                    </div>
                    <div class="info_desc">
                        <h3>INCLUSIVE OF VAT</h3>
                        <p>
                            Prices are inclusive of VAT for product marked by symbol * on the product name
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="footer_info_div">
                    <div class="info_icon">
                        <i class="material-icons material-icons-outlined" style="font-size:20px;">card_giftcard</i>
                    </div>
                    <div class="info_desc">
                        <h3> GET 5% ADDITIONAL DISCOUNT</h3>
                        <p>
                            Sign up and get additional 5% discount on your first purchase using (MERO) code.
                            <a href="{{route('front.discount.offers')}}" style="color:rgb(255, 178, 89);">Read more...</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>

</div>

<footer class="footer footer-black   footer-big">
    <div class="r_container" >
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="footer_header">About Us</h5>
                    <p style=" text-align:justify; font-size: 14px;  color: white;">{{$gs->about}}</p>
                </div>
                <div class="col-md-2">
                    <h5 class="footer_header">Quick Links</h5>
                    <ul class="links-vertical">
                        <li>
                            <a class="underline" href="/">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a class="underline" href="/privacy-policy">
                                Privacy Policy
                            </a>
                        </li>
                        <li>
                            <a class="underline" href="/faq">
                                FAQs
                            </a>
                        </li>
                        <li>
                            <a class="underline" href="/career">
                                Career
                            </a>
                        </li>
                        <li>
                            <a class="underline" href="/return-and-refund-policy">
                                Return and Refund Policy
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h5 class="footer_header">Latest Blogs</h5>
                    <div class="social-feed">
                        @foreach($lblogs as $lblog)
                        <div class="feed-line">
                            <p style="color:white;font-size:14px"><a class="underline" style="color:white;font-size:14px" href="{{route('front.blogshow',$lblog->slug)}}" style="font-size:13px;">{{strlen($lblog->title) > 30 ? substr($lblog->title,0,30)."..." : $lblog->title}}</a></p>
                        </div>
                        @endforeach
                        <p style="color:white;font-size:14px"><a style="color:rgb(255, 178, 89);font-size:14px"; href="{{route('front.blog')}}" style="font-size:13px;">See more...</a></p>

                      
                     
                    </div>
                </div>
                <div class="col-md-3">
                    <h5 class="footer_header">Follow Us</h5>
                    <div class="social-feed">
                        <div class="feed-line">
                            <i class="fa fa-map-marker"></i>
                            <p style="color:white;font-size:14px"><a class="underline" href="{{route('front.contact')}}" >3rd floor, Model Plaza, Khusibun, Kathmandu, Nepal</a>
                            </p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-phone"></i>
                            <p style="color:white;font-size:14px"><a class="underline" href="tel:+977-1-5902444">+977-1-5902444</a></p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-envelope"></i>
                            <p style="color:white;font-size:14px"><a class="underline" href="mailto:info@merohealthcare.com"> info@merohealthcare.com</a></p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-globe"></i>
                            <p style="color:white;font-size:14px;"><a class="underline" href="/"> merohealthcare.com</a></p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-download"></i>
                            <p style="color:white;font-size:14px;">We are now available at</p>
                            <a href="https://play.google.com/store/apps/details?id=com.merohealth&hl=en" target="__blank">
                                <img src="https://liviaride.com/wp-content/uploads/2019/06/gplay.png" style="height:50px;width:150px;margin-bottom:5px;" />
                            </a>
                            <a href="https://apps.apple.com/us/app/mero-health-care/id1544033535" target="__blank">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/0/0d/Downloadontheappstore.png" style="height:47px;width:145px;margin-left:3px;margin-bottom:5px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
      
        <div class="" style="display:flex;justify-content: space-between;flex-wrap:wrap">
            <div style="display:flex;justify-content: flex-start;grid-gap:10px">
                <p style="font-size:14px;color:white">COPYRIGHT 2021 by Web Health Company | Powered By <a class="underline" href="https://www.incubeweb.com" style="color:rgb(255, 178, 89);" target="__blank">Incube</a></p>

            </div>
            <div class="ml-auto mr-auto" style="display:flex;justify-content: flex-start;grid-gap:20px;text-decoration: none !important;margin-bottom:5px;">
                <div class="media-logos media-logos-product">
                    <span style="color: white">We accept: </span>
                    <br class="footer-spacing" style="display: none;">
                    <img style="height:30px;border-radius:50px;" src="https://cdn1.iconfinder.com/data/icons/marketplace-and-shipping-filled-outline/64/COD_cash_on_delivery_shipping_payment_delivery-512.png"> 
                    <img style="height:30px;border-radius:50px;" src="http://icons.iconarchive.com/icons/graphicloads/folded/256/bank-folded-icon.png"> 
                      <img style="height:30px;border-radius:50px;" src="https://i.pinimg.com/280x280_RS/4e/26/e7/4e26e7db448e7570f223bcdf38523d33.jpg"> 
                    <img style="height:30px;border-radius:50px;" src="https://otsnepal.com/wp-content/uploads/2020/05/unnamed.png"> 
                     <img style="height:30px;border-radius:50px;" src="https://media-exp1.licdn.com/dms/image/C510BAQE-gVAYFB4FUg/company-logo_200_200/0?e=2159024400&amp;v=beta&amp;t=oJy0sDDwzrx3cH5KCmGMYsy7FotUix4drGCTJX4kfAA">
                      <img style="height:30px;border-radius:50px;" src="https://www.merohealthcare.com/assets/images/1597057079117688840_355124495888664_8627824190237150414_n.jpg"> 
                    <img style="height:30px;border-radius:50px;" src="https://www.merohealthcare.com/assets/images/1599126542107-1079686_transparent-visa-mastercard-png-visa-and-mastercard-logo.png">
                 <br>
                </div>
            </div>

   
            <div class="ml-auto mr-auto" style="display:flex;justify-content: center;grid-gap:10px">
                <a href="https://www.facebook.com/merohealthcarenp/"
                    class="btn btn-fab btn-round btn-outline btn-sm" style="border-color: white;">
                    <i class="fa fa-facebook"></i>
                    <div class="ripple-container"></div>
                </a>
                <a href="https://www.instagram.com/merohealthcare/" class="btn btn-fab btn-round btn-outline btn-sm"
                    style="border-color: white;   ">
                    <i class="fa fa-instagram"></i>
                    <div class="ripple-container"></div>
                </a>
                <a href="https://twitter.com/merohealthcare" class="btn btn-fab btn-round btn-outline btn-sm"
                    style="border-color: white;   ">
                    <i class="fa fa-twitter "></i>
                    <div class="ripple-container"></div>
                </a>
                <a href="https://www.linkedin.com/company/merohealthcare"
                    class="btn btn-fab btn-round btn-outline btn-sm" style="border-color: white;   ">
                    <i class="fa fa-linkedin "></i>
                    <div class="ripple-container"></div>
                </a>
                <a href="https://www.youtube.com/channel/UClJjsjDTE0kCkCxIXPSnfuQ"
                    class="btn btn-fab btn-round btn-outline btn-sm" style="border-color: white;   ">
                    <i class="fa fa-youtube "></i>
                    <div class="ripple-container"></div>
                </a>
                <a href="https://www.pinterest.com/merohealthcare/" class="btn btn-fab btn-round btn-outline btn-sm"
                    style="border-color: white;   ">
                    <i class="fa fa-pinterest "></i>
                    <div class="ripple-container"></div>
                </a>
            </div>
        </div>
    </div>
</footer>

    @if (Session::has('success'))
        <div class="container">
            <div class="row">
                <div>
                    <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-success alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; bottom: 20px; right: 10px; animation-iteration-count: 1;">
                        <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="h5">
                            <i class="fa fa-check-circle"></i>
                            Success
                        </h4>
                        <p>{{Session::get('success')}}</p>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('error'))
        <div class="container">
            <div class="row">
                <div>
                    <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; bottom: 20px; right: 10px; animation-iteration-count: 1;">
                        <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="h5">
                            <i class="fa fa-check-circle"></i>
                            Oops
                        </h4>
                        <p>{{Session::get('error')}}</p>

                    </div>
                </div>
            </div>
        </div>
    @endif
   <!--   Core JS Files   -->
<script>
    // if ('ontouchstart' in document.documentElement)
    //        document.addEventListener('touchstart', onTouchStart, {passive: true});
    @include('layouts.partials.js.jquery')
    @include('layouts.partials.js.popperjs')
    @include('layouts.partials.js.momentjs')
    @include('layouts.partials.js.pagejs')
    @include('layouts.partials.js.materialkitjs')
    @include('layouts.partials.js.buttonsjs')
    @include('layouts.partials.js.owljs')
    @include('layouts.partials.js.notifyjs')
    @include('layouts.partials.js.jquery-migrate-min')
    @include('layouts.partials.js.hscore-js')
    @include('layouts.partials.js.jquery-countdown-min')
    @include('layouts.partials.js.hs-countdown-js')
</script>

{{-- <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script> --}}


{{-- <script src="./material-assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="./material-assets/js/core/popper.min.js" type="text/javascript"></script> --}}
{{-- <script src="./material-assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script> --}}
{{-- <script src="./material-assets/js/plugins/moment.min.js"></script>  --}}

<!--    Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<!-- <script src="./material-assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script> -->
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<!-- <script src="./material-assets/js/plugins/nouislider.min.js" type="text/javascript"></script> -->
<!--  Google Maps Plugin    -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<!-- <script src="./material-assets/js/plugins/bootstrap-tagsinput.js"></script> -->
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<!-- <script src="./material-assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script> -->
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<!-- <script src="./material-assets/js/plugins/jasny-bootstrap.min.js" type="text/javascript"></script> -->
<!--    Plugin for Small Gallery in Product Page -->
<!-- <script src="./material-assets/js/plugins/jquery.flexisel.js" type="text/javascript"></script> -->
<!-- Plugins for presentation and navigation  -->
<!-- <script src="./material-assets/demo/modernizr.js" type="text/javascript"></script>
    <script src="./material-assets/demo/vertical-nav.js" type="text/javascript"></script> -->
<!-- Place this tag in your head or just before your close body tag. -->

{{-- <script defer src="https://buttons.github.io/buttons.js"></script> --}}

<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
{{-- <script src="./material-assets/js/material-kit.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
<script src="{{asset('assets/user/js/notify.js')}}"></script> --}}

 <!-- JS Timer Unify-->
 
{{-- <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/jquery.countdown.min.js"></script>
<script  src="/frontend-assets/main-assets/assets/js/components/hs.countdown.js"></script> --}}

<!-- JS Plugins Init. -->
<script >
$(document).on('ready', function () {
    // initialization of countdowns
    var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
    yearsElSelector: '.js-cd-years',
    monthElSelector: '.js-cd-month',
    daysElSelector: '.js-cd-days',
    hoursElSelector: '.js-cd-hours',
    minutesElSelector: '.js-cd-minutes',
    secondsElSelector: '.js-cd-seconds'
    });
});
</script>


<script>
    $(document).on("click", ".addcart" , function(){
      var pid = $(this).parent().find('input[type=hidden]').val();
    //   mixpanel.track("Add to cart", {"product_id": pid});
      console.log(pid);
          $.ajax({
                  type: "GET",
                  url:"{{URL::to('/json/addcart')}}",
                  data:{id:pid},
                  success:function(data){
                      if(data == 0)
                      {
                          $.notify("{{$gs->cart_error}}","error");
                      }
                      else
                      {
                      $(".empty").html("");
                      $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                      $(".cart-quantity").html(data[2]);
                      var arr = $.map(data[1], function(el) {
                      return el });
                      $(".cart").html("");
                      for(var k in arr)
                      {
                          var x = arr[k]['item']['name'];
                          var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                          var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                            $(".cart").append(
                                '<div class="single-myCart" style="margin: 10px !important;">'+
                                    '<div class=" g-brd-none g-px-20">'+
                                        '<div class="row no-gutters g-pb-5">'+
                                            '<div class="col-4 pr-3 cart_image_center">'+
                                                    '<img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                            '</div>'+
                                            '<div class="col-6">'+
                                                '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'">'+'<h6 class="card-title">'+p+'</h6></a>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> <span>'+measure+'</span></p>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">'+
                                                    @if($gs->sign == 0)
                                                    '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                                    @else
                                                    '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                                    @endif
                                                '</p>'+
                                            '</div>'+
                                                '<div class="col-2">'+
                                                '<button style="margin-top:10px;padding:12px;" type="button" class="cart-close btn btn-danger" onclick="remove('+arr[k]['item']['id']+')"><i class="material-icons">clear</i></button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');
                        }
                      $.notify("{{$gs->cart_success}}","success");
                    // alert("Success added");
                    // setTimeout(function(){ $(".alert-success").show();}, 100);
                      }
                  },
                  error: function(data){
                      if(data.responseJSON)
                      $.notify(data.responseJSON.error,"error");
                      else
                      $.notify('Something went wrong',"error");
                  }
            });
      return false;
    });
    </script>
    
    <script type="text/javascript">
        function remove(id) {
            $("#del"+id).hide();
                $.ajax({
                        type: "GET",
                        url:"{{URL::to('/json/removecart')}}",
                        data:{id:id},
                        success:function(data){
                            $(".empty").html("");
                            $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                            $(".cart-quantity").html(data[2]);
                            $(".cart").html("");
                            if(data[1] == null)
                            {
                                $(".total").html("0.00");
                                $(".cart-quantity").html("0");
                                $(".empty").html("{{$lang->h}}");
                            }
        
                                var arr = $.map(data[1], function(el) {
                                return el });
                                for(var k in arr)
                                {
                                    var x = arr[k]['item']['name'];
                                    var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                                    var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                                    $(".cart").append(
                                '<div class="single-myCart" style="margin: 10px !important;">'+
                                    '<div class=" g-brd-none g-px-20">'+
                                        '<div class="row no-gutters g-pb-5">'+
                                            '<div class="col-4 pr-3 cart_image_center">'+
                                                    '<img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                            '</div>'+
                                            '<div class="col-6">'+
                                                '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'">'+'<h6 class="card-title">'+p+'</h6></a>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> <span>'+measure+'</span></p>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">'+
                                                    @if($gs->sign == 0)
                                                    '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                                    @else
                                                    '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                                    @endif
                                                '</p>'+
                                            '</div>'+
                                                '<div class="col-2">'+
                                                '<button style="margin-top:10px;padding:12px;" type="button" class="cart-close btn btn-danger" onclick="remove('+arr[k]['item']['id']+')"><i class="material-icons">clear</i></button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');
                            }                            
                                                        
                          },
                    error: function(data){
                      
                        $.notify('Something went wrong',"error");
        
                    }
                  }); 
        
        }
    </script>
    <script type="text/javascript">
            $(".ss").keyup(function() {
               var search = $(this).val();
               if(search == ""){
                   $(".header-searched-item-list-wrap").hide();
               }
               else {
                   $.ajax({
                           type: "GET",
                           url:"{{URL::to('/json/suggest')}}",
                           data:{search:search},
                           success:function(data){
                               if(!$.isEmptyObject(data))
                               {
                                   $(".header-searched-item-list-wrap").show();
                                   $(".header-searched-item-list-wrap ul").html("");
                                   // var arr = $.map(data, function(el) {
                                   //     return el 
                                   // });
                                   // for(var k in arr)
                                   // {
                                   //     var x = arr[k]['name'];
                                   //     var p = x.length  > 50 ? x.substring(0,50)+'...' : x;
                                       
                                   //     $(".header-searched-item-list-wrap ul").append('<li><a href="{{url('/')}}/product/'+arr[k]['id']+'/'+arr[k]['name']+'">'+p+'</a></li>');
                                   // }
                                   // console.log(data);
                                   $(".header-searched-item-list-wrap ul").append(data.html);
           
                               }
                               else{
                                   $(".header-searched-item-list-wrap").hide();
                               }
                           }
                         }) 
                   
               }
            });
    </script>  
           
    <script type="text/javascript">
            $(".ss").keyup(function() {
               var search = $(this).val();
               if(search == ""){
                   $(".header-searched-item-list-wrap-mobile").hide();
               }
               else {
                console.log("hello1st");
                   $.ajax({
                           type: "GET",
                           url:"{{URL::to('/json/mobilesuggest')}}",
                           data:{search:search},
                           success:function(data){
                               if(!$.isEmptyObject(data))
                               {
                                   $(".header-searched-item-list-wrap-mobile").show();
                                   $(".header-searched-item-list-wrap-mobile ul").html("");
                                   // var arr = $.map(data, function(el) {
                                   //     return el 
                                   // });
                                   // for(var k in arr)
                                   // {
                                   //     var x = arr[k]['name'];
                                   //     var p = x.length  > 50 ? x.substring(0,50)+'...' : x;
                                       
                                   //     $(".header-searched-item-list-wrap ul").append('<li><a href="{{url('/')}}/product/'+arr[k]['id']+'/'+arr[k]['name']+'">'+p+'</a></li>');
                                   // }
                                   console.log(data);
                                   $(".header-searched-item-list-wrap-mobile ul").append(data.html);
           
                               }
                               else{
                                   $(".header-searched-item-list-wrap-mobile").hide();
                                   console.log('unsucess');
                               }
                           }
                         }) 
                   
               }
            });
    </script>  
           
    <script type="text/javascript">
            $(document).on("click", ".wish-listt" , function(){
                var max = '';
                var pid = $(this).parent().find('input[type=hidden]').val();
                $("#myModal .modal-content").html('');
                    $.ajax({
                            type: "GET",
                            url:"{{URL::to('/json/quick')}}",
                            data:{id:pid},
                            success:function(data){
                                $("#myModal .modal-content").append(''+
                                    '<div class="modal-header">'+
                                    '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                                    '</div>'+
                                    '<div class="modal-body">'+
                                    '<div class="row">'+
                                    '<div class="col-md-3 col-sm-12">'+
                                    '<div class="product-review-details-img">'+
                        '<img src="{{asset('assets/images/')}}/'+data[0]+'" alt="Product image">'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="col-md-9 col-sm-12">'+
                                    '<div class="product-review-details-description">'+
                                    '<h3>'+data[1]+'</h3>'+
                                    '<p class="modal-product-review">'+
                                    '<i class="fa fa-star"></i>'+
                                    '</p>'+
                                    '<div class="product-price">'+
                                    '<div class="single-product-price">'+
                                     @if($gs->sign == 0)
                                    '{{$curr->sign}}'+data[2]+' <span>{{$curr->sign}}'+data[3]+'</span> '+
                                    @else
                                    ''+data[2]+'{{$curr->sign}} <span>'+data[3]+'{{$curr->sign}}</span> '+
                                    @endif
                                    '</div>'+
                                    '<div class="product-availability">'+
            
                                    '</div>'+
                                    ' </div>'+
                                    '<div class="product-review-description">'+
                                    '<h4>{{$lang->dol}}</h4>'+
                                    '<p style="text-align:justify;">'+data[4]+'</p>'+
                                    '</div>'+
                                    '<div class="product-size">'+
                                    '</div>'+
                                    '<div class="product-color">'+
                                    '</div>'+
                                    '<div class="product-quantity">'+
                                    '</div>'+
                                    '</div>'+   
                                    '</div>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="modal-footer">'+
                                    '</div>');
            
                                    if(data[5] == "0")
                                    {
                                        if(data[9] == 0)
                                        {
                                             $(".product-availability").append(''+
                                            '{{$lang->availability}} '+
                                            '<span style="color:red;">'+
                                            '<i class="fa fa-times-circle-o"></i> '+
                                            '{{$lang->dni}}'+
                                            '</span>'
                                            );                                   
                                        }
            
                                    }
                                    else
                                    {
                                    // $(".empty").html("");
                                    // $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                                    //  $(".cart-quantity").html(data[2]);
                                    var arr = $.map(data[1], function(el) {
                                    return el });
                                    $(".cart").html("");
                                    for(var k in arr)
                                    {
                                        var x = arr[k]['item']['name'];
                                        var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                                        var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                                        $(".cart").append(
                                          '<div class="single-myCart">'+
                        '<p class="cart-close" onclick="remove('+arr[k]['item']['id']+')"><i class="fa fa-close"></i></p>'+
                                        '<div class="cart-img">'+
                                '<img src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                        '</div>'+
                                        '<div class="cart-info">'+
                    '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'" style="color: black; padding: 0 0;">'+'<h5>'+p+'</h5></a>'+
                                    '<p>{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> '+measure+'</p>'+
                                    @if($gs->sign == 0)
                                    '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                    @else
                                    '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                    @endif
                                    '</div>'+
                                    '</div>');
                                      }
                                    $.notify("{{$gs->cart_success}}","success");
                                    }
                                  },error: function(data){
                          
                                      $.notify('Something went wrong',"error");
            
                                  }
                          }); 
                    return false;
                });
            
                $(document).on("click", ".addcartforSearch" , function(){
                    
                    var pid = $(this).parent().parent().find('input[type=hidden]').val();
                    var quantityDiv = $(this).parent();
            
                    $.ajax({
                        type: "GET",
                        url:"{{URL::to('/json/addcart')}}",
                        data:{id:pid},
                        success:function(data){
                            if(data == 0)
                            {
                                $.notify("{{$gs->cart_error}}","error");
                            }
                            else
                            {
                                var qty = 1;
            
                                $(".empty").html("");
                                $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                                $(".cart-quantity").html(data[2]);
                                var arr = $.map(data[1], function(el) {
                                    return el 
                                });
                                $(".cart").html("");
                                for(var k in arr)
                                {
                                    if(arr[k]['item']['id'] == pid) qty = arr[k]['qty'];
            
                                    var x = arr[k]['item']['name'];
                                    var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                                    var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                                    $(".cart").append(
                                '<div class="single-myCart" style="margin: 10px !important;">'+
                                    '<div class=" g-brd-none g-px-20">'+
                                        '<div class="row no-gutters g-pb-5">'+
                                            '<div class="col-4 pr-3 cart_image_center">'+
                                                    '<img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                            '</div>'+
                                            '<div class="col-6">'+
                                                '<div class="cart-info">'+
                                                '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'">'+'<h6 class="card-title">'+p+'</h6></a>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> <span>'+measure+'</span></p>'+
                                                '<p class="card-text" style="margin-bottom:0px; font-size:14px;">'+
                                                    @if($gs->sign == 0)
                                                    '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                                    @else
                                                    '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                                    @endif
                                                '</p>'+
                                                '</div>'+
                                            '</div>'+
                                                '<div class="col-2">'+
                                                '<button style="margin-top:10px;padding:12px;" type="button" class="cart-close btn btn-danger" onclick="remove('+arr[k]['item']['id']+')"><i class="material-icons">clear</i></button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');
                                }
                                
                                quantityDiv.html(
                                    '<span class="quantity-btn reducingforSearch"><i class="fa fa-minus"></i></span>'+
                                    '<span class="qtyforSearch">'+qty+'</span>'+
                                    '<span class="quantity-btn addingforSearch"><i class="fa fa-plus"></i></span>'
                                );
                                $.notify("{{$gs->cart_success}}","success");
                            }
                        },
                        error: function(data){
                          if(data.responseJSON)
                            $.notify(data.responseJSON.error,"error");
                          else
                            $.notify('Something went wrong',"error");
            
                        }
                        
                    }); 
                    return false;
                });
            
                $(document).on("click", ".addingforSearch" , function(e){
                    e.preventDefault()
                    var pid =  $(this).parent().parent().find('input[type=hidden]').val();
                    var qty = $(this).parent().find(".qtyforSearch").html();
                    var quantityDiv = $(this).parent();
            
                    $.ajax({
                        type: "GET",
                        url:"{{URL::to('/json/addbyone')}}",
                        data:{id:pid},
                        success:function(data){
                            if(data == 0)
                            {
                                $.notify("{{$gs->cart_error}}","error");
                            }
                            
                            else
                            {
                                $(".total").html((data[0] * {{$curr->value}}).toFixed(2));                        
                                $(".cart-quantity").html(data[3]);
                                $("#cqty"+pid).val("1");
                                $("#prc"+pid).html((data[2] * {{$curr->value}}).toFixed(2));
                                $('.cart-info').find("#prct"+pid).html((data[2] * {{$curr->value}}).toFixed(2));
                                $('.cart-info').find("#cqt"+pid).html(data[1]);
                                quantityDiv.find(".qtyforSearch").html(data[1]);
                            }
                        },
                        error: function(data){
                          if(data.responseJSON)
                            $.notify(data.responseJSON.error,"error");
                          else
                            $.notify('Something went wrong',"error");
            
                        }
                    }); 
                });
            
                $(document).on("click", ".reducingforSearch" , function(e){
                    e.preventDefault()
                    
                    var id =  $(this).parent().parent().find('input[type=hidden]').val();
                    var qty = $(this).parent().find(".qtyforSearch").html();
                    var quantityDiv = $(this).parent();
                    qty--;
                    if(qty < 1)
                    {
                        remove(id);
                        quantityDiv.html('<button class="btn btn-sm btn-primary addcartforSearch">Add to Cart</button>');
                    }
                    else{
                     
                        $.ajax({
                            type: "GET",
                            url:"{{URL::to('/json/reducebyone')}}",
                            data:{id:id},
                            success:function(data){
                                $(".total").html((data[0] * {{$curr->value}}).toFixed(2));
                                $(".cart-quantity").html(data[3]);
                                $("#cqty"+id).val("1");
                                $("#prc"+id).html((data[2] * {{$curr->value}}).toFixed(2));
                                
                                $('.cart-info').find("#prct"+id).html((data[2] * {{$curr->value}}).toFixed(2));
                                $('.cart-info').find("#cqt"+id).html(data[1]);
                                quantityDiv.find(".qtyforSearch").html(data[1]);
                                
                            },
                            error: function(data){
                              if(data.responseJSON)
                                $.notify(data.responseJSON.error,"error");
                              else
                                $.notify('Something went wrong',"error");
            
                            }
                        }); 
                    }
                });
    </script>
    <script>
        $(document).on("click", ".removecart" , function(e){
            $(".addToMycart").show();
        });
    </script>
    <script>
                  var size = "";
                  var colorss = "";
                  $(document).on("click", ".msize" , function(){
                    $('.msize').removeClass('mselected-size');
                    $(this).addClass('mselected-size');
                    size = $(this).html();
                  });
            
                  $(document).on("click", ".mcolor" , function(){
                    $('.mcolor').removeClass('mselected-color');
                    $(this).addClass('mselected-color');
                    colorss = $(this).html();
                  });
                 $(document).on("click", "#maddcart" , function(){
                    var qty = $("#mqty").val();
                    if(qty < 1)
                    {
                        $.notify("{{$gs->invalid}}","error");
                    }
                    else
                    {
                      var pid = $("#mid").val();
                        $.ajax({
                            type: "GET",
                            url:"{{URL::to('/json/addnumcart')}}",
                            data:{id:pid,qty:qty,size:size,color:colorss},
                            success:function(data){
                              if(data == 0)
                              {
                                $(".product-size").append(
                                '<p>{{$lang->doo}}</p>'
                                );
                                for(var size in data[6])
                                $(".product-size").append(
                                '<span style="cursor:pointer;" class="msize">'+data[6][size]+'</span> '
                                );
                              }
                              
                              if(data[8] != null)
                              {
                                  var x = arr[k]['item']['name'];
                                  var p = x.length  > 45 ? x.substring(0,45)+'...' : x;
                                  var measure = arr[k]['item']['measure'] != null ? arr[k]['item']['measure'] : "";
                                  $(".cart").append(
                                  '<div class="single-myCart">'+
                                    '<p class="cart-close" onclick="remove('+arr[k]['item']['id']+')"><i class="fa fa-close"></i></p>'+
                                    '<div class="cart-img">'+
                                      '<img src="{{ asset('assets/images/') }}/'+arr[k]['item']['photo']+'" alt="Product image">'+
                                    '</div>'+
                                    '<div class="cart-info">'+
                                      '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'" style="color: black; padding: 0 0;">'+'<h5>'+p+'</h5></a>'+
                                      '<p>{{$lang->cquantity}}: <span id="cqt'+arr[k]['item']['id']+'">'+arr[k]['qty']+'</span> '+measure+'</p>'+
                                      @if($gs->sign == 0)
                                      '<p>{{$curr->sign}}<span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span></p>'+
                                      @else
                                      '<p><span id="prct'+arr[k]['item']['id']+'">'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</span>{{$curr->sign}}</p>'+
                                      @endif
                                    '</div>'+
                                    '<div class="cart-info">'+
                                      '<a href="{{url('/')}}/product/'+arr[k]['item']['id']+'/'+arr[k]['item']['name']+'" style="color: black; padding: 0 0;">'+'<h5>'+p+'</h5></a>'+
                                      '<p>{{$lang->cquantity}}: '+arr[k]['qty']+' '+measure+'</p>'+
                                      @if($gs->sign == 0)
                                      '<p>{{$curr->sign}}'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'</p>'+
                                      @else
                                      '<p>'+(arr[k]['price'] * {{$curr->value}}).toFixed(2)+'{{$curr->sign}}</p>'+
                                      @endif
                                    '</div>'+
                                  '</div>');
                              }
                              $.notify("{{$gs->cart_success}}","success");
                            },
                            error: function(data){
                              if(data.responseJSON)
                                $.notify(data.responseJSON.error,"error");
                              else
                                $.notify('Something went wrong',"error");
            
                            }
                        }); 
                    }
                    return false;
                });
        </script>
        <script>
            $(document).ready(function(){
             $(window).scroll(function () {
                    if ($(this).scrollTop() > 50) {
                        $('#back-to-top').fadeIn();
                    } else {
                        $('#back-to-top').fadeOut();
                    }
                });
                // scroll body to 0px on click
                $('#back-to-top').click(function () {
                    $('#back-to-top').tooltip('hide');
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
                
                $('#back-to-top').tooltip('show');
        
        });
        </script>
    

@section('js')
@if(Auth::guard('user')->check())
<script>
    var user= {!!json_encode(Auth::guard('user')->user())!!};
    console.log(user.name,user.id,user.email);
    var USER_ID = user.id;
    mixpanel.people.set({
        "$email": user.email,    // only reserved properties need the $
        "Sign up date": user.created_at,    // Send dates in ISO timestamp format (e.g. "2020-01-02T21:07:03Z")
        "USER_ID": USER_ID,    // ...or numbers
    });
    mixpanel.identify(user.email+', '+user.name+', '+user.id);
    // analytics.identify(user.id, {
    //     name: user.name,
    //     email: user.email
    // });
    // ga('set', 'userId', user.email+', '+user.name+', '+user.id);
    // ga('set', 'username', user.name);
    // ga('set', 'email', user.email);
</script>
@endif




@endsection




