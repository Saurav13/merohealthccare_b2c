<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WHJLDQ4');

    </script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if (isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
    @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
    @elseif(isset($product->meta_tag) && isset($product->meta_description))
        <meta name="keywords" content="{{ $product->meta_tag }}">
        <meta name="description" content="{{ $product->meta_description }}">
    @elseif(isset($home))
        <meta name="description"
            content="One of the best online pharmacy/ medical store to buy medicine, beauty & healthcare products in Kathmandu-Nepal. FREE* home delivery with DISCOUNTS.">
        <meta name="keywords"
            content="online pharmacy in Nepal, buy medicine online in Nepal, healthcare products in Kathmandu, online medical store in Kathmandu">
    @endif
    <title>@yield('title') | {{ $gs->title }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <link rel="icon" type="image/png" href="{{ asset('assets/images/' . $gs->favicon) }}">

    {{-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" /> --}}
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> --}}
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" /> --}}
    {{-- <link href="./material-assets/css/material-kit.css?v=2.2.1" rel="stylesheet" />
    <link href="./material-assets/css/mhc.css?v=0.0.4" rel="stylesheet" /> --}}
    {{-- <link href="./material-assets/css/responsive.css?v=0.0.3" rel="stylesheet" /> --}}

    <style>
        @include('layouts.partials.css.materialicons') @include('layouts.partials.css.fa') @include('layouts.partials.css.muli') @include('layouts.partials.css.owlcarousel') @include('layouts.partials.css.materialkitmincss') @include('layouts.partials.css.mhc') @include('layouts.partials.css.responsive')

    </style>
    <style>
        @font-face {
            font-family: Muli;
            src: url(./material-assets/font/muli.ttf);
        }

        @media all and (min-width: 992px) {
            .navbar {
                padding-top: 0;
                padding-bottom: 0;
            }

            .navbar .has-megamenu {
                position: static !important;
            }

            .navbar .megamenu {
                left: 0;
                right: 0;
                width: 100%;
                padding: 20px;
            }

            .navbar .nav-link {
                padding-top: 1rem;
                padding-bottom: 1rem;
            }
        }

        .scroll-box {
            overflow-y: scroll;
        }

        #search-color .form-control {
            color: white !important;
        }

        #search-color .form-control::placeholder {
            color: white !important;
        }

        .btn.btn-primary:hover {
            color: #fff;
            background-color: #2385aa;
            border-color: #2385aa;
            box-shadow: 0 2px 2px 0 #00bcd4, 0 3px 1px -2px #00bcd4, 0 1px 5px 0 #00bcd4;
        }

        .btn.btn-primary {
            color: #fff;
            background-color: #2385aa;
            border-color: #2385aa;
            box-shadow: 0 2px 2px 0 #2385aa, 0 3px 1px -2px #2385aa, 0 1px 5px 0 #2385aa;
        }

        html {
            font-family: 'Muli' !important;
        }

        .pac-container {
            z-index: 1050 !important;
        }

        .form-control::placeholder {
            color: #999 !important;
        }

    </style>

    @yield('css')
    <script type='text/javascript'
        src='https://platform-api.sharethis.com/js/sharethis.js#property=5e8c318da1c1430012f50dc8&product=inline-share-buttons&cms=sop'
        async='async'></script>
</head>

<body class="index-page sidebar-collapse">
