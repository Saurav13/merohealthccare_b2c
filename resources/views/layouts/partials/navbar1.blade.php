<nav class="navbar navbar-default navbar-expand-lg" role="navigation-demo" style="padding:0; margin:0;">
    <div class="r_container nav_container" style="height:6rem;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-translate nav_translate" style="height:100%">
            <a class="navbar-brand pt-0 nav_brand" href="/"><img src="{{asset('assets/images/'.$gs->logo)}}"
                    style="height:4rem">
                    <h1 class="logo_title">Online Pharmacy In Nepal</h1>
                </a>

                <a id="mobile-prescription" href="{{ Auth::guard('user')->check() ? route('user-prescriptions.create') : '/upload-prescription' }}" class="btn btn-primary nav-link m_nav_link">
                    <i class="material-icons">assignment</i>
                    Prescription
                </a>


            <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="hidden-sm">

            <ul class="navbar-nav ml-auto" style="margin-top:10px">

                <li class="nav-item nav_items dropdown">
                  <a class="nav-link nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">phone</i>
                    Customer Care
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="tel:+977-1-5902444" style="color:rgb(131, 88, 8);">  <i class="fa fa-phone"></i></i>&nbsp; +977-1-5902444</a>
                    <a class="dropdown-item" href="https://wa.me/9779840860410" style="color:green;"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp; +9779840860410</a>
                    <a class="dropdown-item" href="viber://chat?number=9779840860410" style="color:purple;"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp; +9779840860410</a>
                    <a class="dropdown-item" href="mailto:info@merohealthcare.com" style="color:rgb(24, 161, 207);"><i class="fa fa-envelope"></i>&nbsp; info@merohealthcare.com</a>
                  </div>
                </li>

             
                <li class="nav-item nav_items">
                    <a href="{{route('front.sell-on-mhc')}}" class="nav-link nav_link">
                        <i class="material-icons">store</i>
                        Sell On Merohealthcare
                    </a>
                </li>
                <li class="nav-item nav_items">
                    <div class="nav_icons ">
                        {{-- <a href="#pablo" class="nav-link nav_link px-0">
                            <i class="material-icons">favorite</i><span class="badge badge-danger">1</span>
                        </a> --}}
                        @if(Auth::guard('user')->check())
                            <a href="{{route('user-wishlists')}}" class="nav-link nav_link px-0">
                              <i class="material-icons">favorite</i>
                              @if($wishlist == 0)
                              @else
                              <span class="badge badge-success" style="border-radius:15px; position: absolute;top:0px;left:18px; padding:5px 8px;">{{ $wishlist}}</span>
                              @endif
                          </a>
                        @else
                            <a href="#pablo" class="nav-link nav_link px-0">
                            <i class="material-icons">favorite</i>
                            </a>
                        @endif
                        
                    </div>
                </li>
                <li class="nav-item nav_items dropdown">
                  <a href="#pablo" class="nav-link nav_link px-0 nav-link nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">shopping_cart</i>
                    <span class="badge badge-danger cart-quantity" style="border-radius:15px; position: absolute;top:0px;left:18px; padding:5px 8px;">{{ Session::has('cart') ? count(Session::get('cart')->items) : '0' }}</span>
                </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="max-height: 500px; overflow-y: scroll;left:-175px;z-index:10000;">
                    <div class="card card-nav-tabs" style="width: 20rem; margin-top:40px;box-shadow:none;">
                      <h4 class="card-header h3 card-header-info text-center"> 
                        @if($gs->sign == 0)
                        <strong style="font-weight:800;">Rs. </strong><span class="total g-font-weight-800" style="font-size:40px;font-weight:800;"> {{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>
                        @else
                            <span class="total">{{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>{{$curr->sign}}
                        @endif  
                        <br>
                        <span class="h6 empty text-center g-mt-10">{{ Session::has('cart') ? '' :$lang->h }}</span>
                      </h4>
                     
                      <div class="addToMycart " style="right:0px; border-radius:10px;" >

                        <div class="container text-center" style="border-radius:10px;">
                       
                        {{-- <hr style="margin-top:1rem; margin-bottom:1rem;"> --}}
                        {{-- <h5 class="text-center" style="text-transform:capitalize;
                        padding: 5px;border-radius: 10px;font-size:24px;">
                        @if($gs->sign == 0)
                            <strong style="font-weight:700">Rs. </strong><span class="total g-font-weight-700 g-font-size-32"> {{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>
                        @else
                            <span class="total">{{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>{{$curr->sign}}
                        @endif
                        </h5> --}}
                        {{-- <hr style="margin-top:1rem; margin-bottom:1rem;"> --}}
                        <div class="addMyCart-btns g-mb-10">
                          <div class="row">
                          <div class="col-md-5" style="padding-right:2px;">
                            <a href="{{route('front.cart')}}" class="btn btn-block btn-info" ><i class="material-icons">add_shopping_cart</i> Cart</a>
                          </div>
                        <div class="col-md-7" style="padding-left:2px;">
                            <a href="{{route('front.checkout')}}" id="proceed-checkout" class="btn btn-block btn-success" ><i class="material-icons">shopping_cart</i> Checkout </a>
                        </div>
                          </div>
                        </div>
                        </div>
                        <div class="cart" >
                            @if(Session::has('cart'))
                                @foreach(Session::get('cart')->items as $product)
                                    <div class="single-myCart" style="margin: 10px !important;" >
                                        <div class="u-basket__product g-brd-none g-px-20">
                                            <div class="row no-gutters g-pb-5">

                                                <div class="col-4 pr-3 cart_image_center">
                                                        <img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/'.$product['item']['photo']) }}" alt="Product image">
                                                </div>

                                                <div class="col-6">
                                                    <a href="{{ route('front.product',[$product['item']['id'],str_slug($product['item']['name'],'-')]) }}"><h6 class="card-title">{{strlen(ucwords(strtolower($product['item']['name']))) > 45 ? substr(ucwords(strtolower($product['item']['name'])),0,45).'...' : ucwords(strtolower($product['item']['name']))}}</h6></a>
                                                    <p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt{{$product['item']['id']}}">{{$product['qty']}}</span> <span>{{ $product['item']['measure'] }}</span></p>
                                                    <p class="card-text" style="margin-bottom:0px; font-size:14px;">
                                                    @if($gs->sign == 0)
                                                        {{$curr->sign}}<span id="prct{{$product['item']['id']}}">{{round($product['price'] * $curr->value , 2) }}</span>
                                                    @else
                                                        <span id="prct{{$product['item']['id']}}">{{round($product['price'] * $curr->value , 2) }}</span>{{$curr->sign}}
                                                    @endif
                                                    </p>
                                                </div>
                                                    <div class="col-2">
                                                        {{-- <button type="button" class="u-basket__product-remove cart-close" onclick="remove({{$product['item']['id']}})">×</button> --}}
                                                    <button style="margin-top:10px;padding:12px;" type="button" class="btn btn-danger u-basket__product-remove cart-close" onclick="remove({{$product['item']['id']}})"><i class="material-icons">clear</i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                    </div>
                </div>
                  </div>
                </div>
                </li>
                <li class="nav-item nav_items dropdown">
                    @if(Auth::guard('user')->check())
                        @php
                            $user = Auth::guard('user')->user();
                            $user_str = $user->name;
                            $user_name = explode(' ', $user_str);
                        @endphp
                        <a class="nav-link nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if($user->is_provider == 0)
                                <img src="{{ $user->photo ? asset('assets/images/'.$user->photo) :asset('assets/images/user.png')}}" style="height:1.7rem;border-radius:30px;"/>
                                Welcome, {{ $user_name[0] }}
                            @else
                            <img src="{{ $user->photo ? $user->photo :asset('assets/images/user.png')}}" style="height:1.7rem;border-radius:30px;"/>
                            Welcome, {{ $user_name[0] }}
                            @endif
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <div class="card card-nav-tabs" style="width: 15rem; margin-top:40px;box-shadow:none;margin-bottom: 10px;">
                                <h4 class="card-header h3 card-header-info text-center"> 
                                    <span class="h6 text-center">Loyality Balance</span><br>
                                    <strong style="font-weight:800;">Rs. </strong><span class="g-font-weight-800" style="font-size:40px;font-weight:800;"> 
                                        {{ $user->current_balance }}
                                    </span>
                                </h4>
                            </div>

                            <a class="dropdown-item" href="{{route('user-dashboard')}}"><i class="material-icons">account_circle</i>&nbsp; Dashboard</a>
                            <a class="dropdown-item" href="{{route('user-logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i class="material-icons">exit_to_app</i>&nbsp; Logout</a>

                
                            <form id="logout-form" action="{{route('user-logout')}}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </div>
                    @else
                    <a href="{{route('user-login')}}" class="nav-link nav_link">
                        <i class="material-icons">person_outline</i>
                        Login
                    </a>
                    @endif



                </li>
            </ul>
        </div>


        </ul>
    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>

<nav class="navbar navbar-default navbar-expand-lg hidden-lg bg-primary mb-0 pb-0" style="border-radius:0px;">
  <div class="m_nav_div_1 r_m_nav_div">
      <div class="m_nav_inner_1">
       
        <a class="nav-link m_nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">phone</i>
            Contact
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="tel:+977-1-5902444" style="color:rgb(131, 88, 8);">  <i class="fa fa-phone"></i></i>&nbsp; +977-1-5902444</a>
            <a class="dropdown-item" href="https://wa.me/9779840860410" style="color:green;"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp; +9779840860410</a>
            <a class="dropdown-item" href="viber://chat?number=9779840860410" style="color:purple;"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp; +9779840860410</a>
            <a class="dropdown-item" href="mailto:info@merohealthcare.com" style="color:rgb(24, 161, 207);"><i class="fa fa-envelope"></i>&nbsp; info@merohealthcare.com</a>
          </div>
          <a href="{{route('front.sell-on-mhc')}}" class="nav-link m_nav_link">
              <i class="material-icons">store</i>
              Sell On MHC
          </a>
      </div>
      <div class="m_nav_inner_1 ">
        <div class="nav-item nav_items">
            <div class="nav_icons ">
                @if(Auth::guard('user')->check())
                <a href="{{route('user-wishlists')}}" class="nav-link m_nav_link px-0" style="position:relative">
                <i class="material-icons">favorite</i>
                @if($wishlist == 0)
                @else
                <span class="badge badge-success" style="border-radius:15px; position: absolute;top:0px;right:-5px; padding:5px 8px;">{{ $wishlist}}</span>
                @endif
            </a>
            @else
                <a href="#pablo" class="nav-link m_nav_link px-0">
                <i class="material-icons">favorite</i>
                </a>
            @endif
            <a href="#pablo" class="nav-link m_nav_link px-0 nav-link nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative">
                <i class="material-icons">shopping_cart</i>
                <span class="badge badge-danger cart-quantity" style="border-radius:15px; position: absolute;top:0px;right:10px; padding:5px 8px;">{{ Session::has('cart') ? count(Session::get('cart')->items) : '0' }}</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="max-height: 450px;width: 100%; overflow-y: scroll">
                <div class="card card-nav-tabs" style="width: 100%; margin-top:40px;box-shadow:none;">
                <h4 class="card-header h3 card-header-info text-center"> 
                    @if($gs->sign == 0)
                    <strong style="font-weight:800;">Rs. </strong><span class="total g-font-weight-800" style="font-size:40px;font-weight:800;"> {{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>
                    @else
                        <span class="total">{{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>{{$curr->sign}}
                    @endif  
                    <br>
                    <span class="h6 empty text-center g-mt-10">{{ Session::has('cart') ? '' :$lang->h }}</span>
                </h4>
                
                <div class="addToMycart" style="right:0px; border-radius:10px;" >

                    <div class="container text-center" style="border-radius:10px;">
                
                    {{-- <hr style="margin-top:1rem; margin-bottom:1rem;"> --}}
                    {{-- <h5 class="text-center" style="text-transform:capitalize;
                    padding: 5px;border-radius: 10px;font-size:24px;">
                    @if($gs->sign == 0)
                        <strong style="font-weight:700">Rs. </strong><span class="total g-font-weight-700 g-font-size-32"> {{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>
                    @else
                        <span class="total">{{ Session::has('cart') ? round(Session::get('cart')->totalPrice * $curr->value , 2) : '0.00' }}</span>{{$curr->sign}}
                    @endif
                    </h5> --}}
                    {{-- <hr style="margin-top:1rem; margin-bottom:1rem;"> --}}
                    <div class="addMyCart-btns g-mb-10">
                    <div class="row">
                        <div class="col-5 col-xs-5 col-sm-5 col-md-5" style="padding-right:2px;" >
                            <a href="{{route('front.cart')}}" class="btn btn-block btn-info" ><i class="material-icons">add_shopping_cart</i> Cart</a>
                        </div>
                            <div class="col-7 col-xs-7 col-sm-7 col-md-7" style="padding-left:2px;"  >
                                <a href="{{route('front.checkout')}}" id="proceed-checkout" class="btn btn-block btn-success" ><i class="material-icons">shopping_cart</i> Checkout </a>
                            </div>
                    </div>
                    </div>
                    </div>
                    <div class="cart" >
                        @if(Session::has('cart'))
                            @foreach(Session::get('cart')->items as $product)
                                <div class="single-myCart" style="margin: 10px !important;" >
                                    <div class="u-basket__product g-brd-none g-px-20">
                                        <div class="row no-gutters g-pb-5">

                                            <div class="col-4 pr-3 cart_image_center">
                                                    <img class="img-fluid mCS_img_loaded" style="height:65px;margin-top:12px;" src="{{ asset('assets/images/'.$product['item']['photo']) }}" alt="Product image">
                                            </div>

                                            <div class="col-6">
                                                <a href="{{ route('front.product',[$product['item']['id'],str_slug($product['item']['name'],'-')]) }}"><h6 class="card-title">{{strlen(ucwords(strtolower($product['item']['name']))) > 45 ? substr(ucwords(strtolower($product['item']['name'])),0,45).'...' : ucwords(strtolower($product['item']['name']))}}</h6></a>
                                                <p class="card-text" style="margin-bottom:0px; font-size:14px;">{{$lang->cquantity}}: <span id="cqt{{$product['item']['id']}}">{{$product['qty']}}</span> <span>{{ $product['item']['measure'] }}</span></p>
                                                <p class="card-text" style="margin-bottom:0px; font-size:14px;">
                                                @if($gs->sign == 0)
                                                    {{$curr->sign}}<span id="prct{{$product['item']['id']}}">{{round($product['price'] * $curr->value , 2) }}</span>
                                                @else
                                                    <span id="prct{{$product['item']['id']}}">{{round($product['price'] * $curr->value , 2) }}</span>{{$curr->sign}}
                                                @endif
                                                </p>
                                            </div>
                                                <div class="col-2">
                                                <button style="margin-top:10px;padding:12px; " type="button" class="btn btn-danger u-basket__product-remove cart-close" onclick="remove({{$product['item']['id']}})"><i class="material-icons">clear</i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                </div>
            </div>
            </div>
            </div>
            </div>
        </div>
        <div class="nav-item nav_items">
            {{-- <a href="#pablo" class="nav-link m_nav_link">
                <i class="material-icons">person_outline</i>
            </a> --}}
            @if(Auth::guard('user')->check())
            @php
                $user = Auth::guard('user')->user();
                $user_str = $user->name;
                $user_name = explode(' ', $user_str);
            @endphp
            <a class="nav-link m_nav_link dropdown-toggle" href="javascript:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if($user->is_provider == 0)
                    <img src="{{ $user->photo ? asset('assets/images/'.$user->photo) :asset('assets/images/user.png')}}" style="height:1.7rem;border-radius:30px;border-radius:30px;"/>
                    {{-- Welcome, {{ $user_name[0] }} --}}
                @else
                <img src="{{ $user->photo ? $user->photo :asset('assets/images/user.png')}}" style="height:1.7rem;border-radius:30px;"/>
                {{-- Welcome, {{ $user_name[0] }} --}}
                @endif
            </a>
            <div class="dropdown-menu dropdown_menu_profile" aria-labelledby="navbarDropdownMenuLink">
                <div class="card card-nav-tabs" style="width: 15rem; margin-top:40px;box-shadow:none;margin-bottom: 10px;">
                    <h4 class="card-header h3 card-header-info text-center"> 
                        <span class="h6 text-center">Loyality Balance</span><br>
                        <strong style="font-weight:800;">Rs. </strong><span class="g-font-weight-800" style="font-size:40px;font-weight:800;"> 
                            {{ $user->current_balance }}
                        </span>
                    </h4>
                </div>
                <a class="dropdown-item" href="">Welcome, {{ $user_name[0] }}</a>
                <a class="dropdown-item" href="{{route('user-dashboard')}}"><i class="material-icons">account_circle</i>&nbsp; Dashboard</a>
                <a class="dropdown-item" href="{{route('user-logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i class="material-icons">exit_to_app</i>&nbsp; Logout</a>


                <form id="logout-form" action="{{route('user-logout')}}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </div>
            @else
        <a href="{{route('user-login')}}" class="nav-link m_nav_link">
            <i class="material-icons">person_outline</i>
            
        </a>
        @endif
        </div>
        </div>
  </div>

  <div class="m_nav_div r_m_nav_div">
    <div>
        <a href="{{route('lab.index')}}" class="nav-link m_nav_link">
           
            Lab Test
            <div class="ripple-container"></div></a>
    </div>
    <div>
        <a href="{{route('user-askdoctor-index')}}" class="nav-link m_nav_link">
            Ask a Doctor <span class="badge badge-pill badge-success">Free</span>
        </a>
    </div>

  
 
    <div class="nav-item nav_items">
        <a href="https://appointment.merohealthcare.com/" class="nav-link m_nav_link">
            Appointment <span class="badge badge-pill badge-rose">New</span>
        </a>
    </div>
    </div>
</nav>

<nav class="navbar navbar-default navbar-expand-lg navbar_2 py-0" style="background:#2385aa !important;border-radius:0px;">
    <div class="r_container" style="height: 100%;">
        <div class="nav_2_rows">
            <div class="nav_upload_pres_div hidden-sm" >
                <a href="{{ Auth::guard('user')->check() ? route('user-prescriptions.create') : '/upload-prescription' }}" class="btn btn-info" style="letter-spacing:1px;font-weight:600;"><i class="material-icons">assignment</i>&nbsp;Upload Prescription</a>
            </div>
            <div class="nav_menu_link_div hidden-sm" style="margin-left:20px; ">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item nav_items">
                        <a href="{{route('lab.index')}}" class="nav-link nav_link_2">
                            LAB TEST
                            <div class="ripple-container"></div></a>
                    </li>
                    <li class="nav-item nav_items">
                        <a href="{{route('user-askdoctor-index')}}" class="nav-link nav_link_2">
                            ASK A DOCTOR <span class="badge badge-pill badge-success">Free</span>
                        </a>
                    </li>
                    <li class="nav-item nav_items">
                        <a href="https://appointment.merohealthcare.com/" class="nav-link nav_link_2">
                            APPOINTMENT <span class="badge badge-pill badge-rose">New</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="desktop-search" class="nav_search_div">
                <form id="search-color" class="form form-newsletter" action="{{route('front.search')}}" method="GET">
                    <div class="form-group bmd-form-group nav_search_input">
                        <input type="text" class="form-control ss" id="search-product" name="product" placeholder="{{$lang->ec}}" required>
                    </div>
                    <button type="submit" class="btn btn-info btn-just-icon nav_search_btn" name="button">
                        <i class="material-icons">search</i>
                    </button>
                </form>

                <div id="search-list-desktop" class="header-searched-item-list-wrap scroll-box search_dropdown_content" >
                  <ul class="list-group" style="padding-left:25px;">

                  </ul>
              </div>

            </div>

            <div id="mobile-search" class="nav_search_div">
                <form id="search-color" class="form form-newsletter" action="{{route('front.search')}}" method="GET">
                    <div style="display: flex; ">
                    <div class="form-group bmd-form-group nav_search_input">
                        <input type="text" class="form-control ss" id="search-product" name="product" placeholder="{{$lang->ec}}" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-just-icon nav_search_btn" name="button" style="background-image: linear-gradient( 
                        108.7deg
                        , rgb(54 166 197) 8.1%, #36a6c5 91.2% );">
                        <i class="material-icons">search</i>
                    </button>
                    </div>
                </form>

                <div class="header-searched-item-list-wrap-mobile m_search_dropdown_content">
                    <ul class="list-group" style="padding-left:25px;">

                    </ul>
                </div>

            </div>
        </div>
    </div>
</nav>

<nav class="navbar navbar-default navbar-expand-lg hidden-lg mb-0 py-0">
        <div class="nav_menu_link_div hidden-sm">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item nav_items">
                        <a href="#pablo" class="nav-link nav_link_2">
                            LAB TEST
                            <div class="ripple-container"></div></a>
                    </li>
                    <li class="nav-item nav_items">
                        <a href="#pablo" class="nav-link nav_link_2">
                            ASK A DOCTOR <span class="badge badge-pill badge-success">Free</span>
                        </a>
                    </li>
                    <li class="nav-item nav_items">
                        <a href="#pablo" class="nav-link nav_link_2">
                            APPOINTMENT <span class="badge badge-pill badge-rose">New</span>
                        </a>
                    </li>
                </ul>
            </div>
</nav>

<nav class="navbar navbar-default navbar-expand-lg navbar_3 py-0 my-0">
    <div class="r_container" style="z-index:10;">
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav category_ul">
                @foreach($categories->sortBy('cat_name') as $category)
                <li class="nav-item dropdown has-megamenu">
                    <a href="#" class="nav-link category_link" style="padding: 0px !important; color:#2385aa !important;font-weight:700;">
                            {{$category->cat_name}}
                    </a>
                    <div class="dropdown-content" style="background-image: linear-gradient( 
                        108.1deg, rgb(237 243 241) 11.2%, rgb(234 245 247) 88.9% );">
                        <div class="mega_menu">
                            <div class="dropdown_header">
                                <a href="{{route('front.category',$category->cat_slug)}}" class="btn btn-primary btn-sm">{{$category->cat_name}}</a>

                            </div>
                            <div class="row" style="padding: 10px 30px;">
                                @foreach($category->subs()->where('status','=',1)->orderBy('sub_name')->get() as $subcategory)
                                    <div class="col-sm-6 col-md-12 col-lg-2 side_category_div ">
                                        <div class="side_category_item_div">
                                            <div class="">
                                                <a href="{{route('front.subcategory',$subcategory->sub_slug)}}"><h3 class="side_category_title">{{$subcategory->sub_name}}</h3></a>
                                            </div>
                                            <div class="side_category_list">
                                                <ul class="links-vertical side_category_ul"
                                                    style="list-style: none; padding-left: 0;">
                                                     @foreach($subcategory->childs()->where('status','=',1)->orderBy('child_name')->get() as $childcategory)
                                                    <li>
                                                        <a class="side_category_links" href="{{route('front.childcategory',$childcategory->child_slug)}}">
                                                                {{ $childcategory->child_name }}
                                                        </a>
                                                    </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>


                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>


