
h1,h2,h3,h4,h5,h6,p,span,text,button,input{
    font-family: Muli;
}
p{
    font-size:16px;
}
.overlay{
    background-color: #00000021;
    height: 100%;
}

.r_container{
    width:100%;
    margin: 0 7%;
}
.r_container_1{
    margin: 0 7%;
}
.nav_container{
    height: 5rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
}

.nav_items{
    color:#2385aa;
}

.nav_link{
    text-transform: capitalize !important;
    font-size:16px !important;
    font-family: Muli;
    padding: 10px 7px !important;

}

.m_nav_div_1{
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.m_nav_div{
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.m_nav_inner_1{
    display: flex;
    justify-content: center;
}

.m_nav_link{
    text-transform: capitalize !important;
    font-size:12px !important;
    font-family: Muli;
    padding: 10px 7px !important;
    color:white;
}
.m_nav_link i{
    color:white;
}

.nav_icons{
    display: flex;
    justify-content: center;
    flex-direction: row;
}
.navbar_2{
    background: #f3f3f3;
    height:4rem;
    margin-bottom: 0;
}
.nav_2_rows{
    display: flex;
    align-items: center;
    justify-content: start;
    width: 100%;
    height: 100%;
}
.nav_link_2{
    text-transform: uppercase !important;
    font-size:13px !important;
    font-family: Muli;
    padding: 10px 7px !important;
    color: white !important;
}
.nav_upload_pres_div{
    flex:2;
}
.nav_menu_link_div{
    flex:6;
}
.nav_search_div{
    flex:6;
    position:relative !important;
}
.nav_search_btn{
    width: 13% !important;
    margin-top: 8px !important;
}
.nav_search_input{
    width:85% !important;
    margin-top: 14px !important;

}
.m_search_dropdown_content{
    display: none; 
    overflow-y:auto;
    border-radius:10px;
}
.search_dropdown_content{
    display: none ; 
    z-index:1000 !important;
    border-radius:10px;
    box-shadow:0px 8px 20px 9px #ddd;
    right:0px !important;

}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    width: 90%;
    left: 0;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    margin: 0px 5%;
    overflow-y: auto;
    height:70vh;
}
  
.dropdown:hover .dropdown-content {
    display: block;
}
.dropdown_header{
    margin-top:1rem;
    padding-left:2rem;
}

.category_link{
    font-family: 'Muli' !important;
    color:black !important;
}
.side_category_ul{
    padding:0;
}
.side_category_ul li{
    padding: 0px 0;
}
.side_category_title{
    font-size:15px;
    margin-top:10px;
    margin-bottom: 10px;
    text-transform: uppercase;
    color:#2385aa;
    font-weight:700;
}
.side_category_links{
    color: #6c757d;
    font-size: 15px;
    font-family: 'Muli' !important;
}
.banner_card_container{
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
}
.banner_card_text{
    color: #000 !important;
    font-weight: 500;
    text-shadow: 2px 2px 4px #fff;
    font-size: 18px;
}
.banner_card_div{
    box-shadow: 0 10px 15px 0 rgba(0, 0, 0, 0.14);
    border-radius: 30px;
    background: #eeeeee24 !important;
    padding: 2rem 3rem;
}
.offer_div{
    background: #eee !important;
    text-align: center;   
}
.offer_div h5{
    margin:10px;
}
.offer_div a{
    font-size: 15px;
    color: #555 !important;
    text-transform: uppercase;
    font-weight: 600;
}
.offer_div i{
    float:none !important;
    top:0px !important;
}

.banner {
    position: relative;
    /* z-index: 1; */
    margin: 80px auto;
    width: 260px;
}

.banner .line {
    margin: 0 0 10px;
    width: 100%;
    height: 60px;
    box-shadow: 10px 10px 10px rgba(0,0,0,0.05);
    text-align: center;
    text-transform: uppercase;
    font-size: 2em;
    line-height: 60px;
    transform: skew(0, -15deg);
}

.banner .line:after,
.banner .line:first-child:before {
    position: absolute;
    top: 35px;
    left: 0;
    z-index: -1;
    display: block;
    width: 260px;
    height: 58px;
    border-radius: 4px;
    background: rgba(180,180,180,0.8);
    content: '';
    transform: skew(0, 15deg);
}

.banner .line:first-child:before {
    top: -6px;
    right: 0;
    left: auto;
}

.banner .line:first-child:before,
.banner .line:last-child:after {
    width: 0;
    height: 0;
    border-width: 27px;
    border-style: solid;
    border-color: rgba(180,180,180,0.8) rgba(180,180,180,0.8) transparent transparent;
    background: transparent;
}

.banner .line:last-child:after {
    top: 12px;
    border-color: transparent transparent rgba(180,180,180,0.8) rgba(180,180,180,0.8);
}

.banner span {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    background: rgba(255,255,255,0.9);
    color: #666;
    text-shadow: 1px 1px 0 #444;
}

.progress-steps {
    font-family: 'Open Sans', sans-serif;
  }
  .progress-steps > div {
    margin-left: 39px;
    position: relative;
  }
  .progress-steps > div.hidden {
    display: none;
  }
  .progress-steps > div.active > h1::before {
    border: none;
  }
  .progress-steps > div.complete > h1 {
    color:#dc3545;
    font-weight: 600;
  }
  .progress-steps > div.complete > h1 > .number {
    background: #dc3545;
    color: #FFF;
  }
  .progress-steps > div:last-child > h1::before {
    border: none;
  }
  .progress-steps > div > h3 {
    font-size: 1em;
    margin-left: -39px;
    color: #222;
    font-weight: 600;
  }
  .progress-steps > div > h3 > .number {
    background: #2385aa;
    color: #FFF;
  }
  .progress-steps > div > h3::before {
    content: '';
    position: absolute;
    border-left: dashed 1px #dc3545;
    width: 1px;
    height: calc(100% - 27px);
    top: 39px;
    left: -22.5px;
  }
  .progress-steps > div > h3 > .number {
    border-radius: 50%;
    width: 33px;
    height: 33px;
    display: inline-block;
    text-align: center;
    line-height: 33px;
  }
  .progress-steps > div:last-child > h3::before {
    border: none;
}
.featured_offer_header{
    display: flex;
    justify-content: center;
    padding: 14px;
}
.offer_title_red{
    background:#dc3545;
    padding: 6px 16px;
    border-top-left-radius: 38px;
    border-bottom-left-radius: 38px;
    color: white;
    padding-bottom: 9px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
}
.offer_title_green{
    background:#71c02d;
    padding: 6px 16px;
    border-top-right-radius: 38px;
    border-bottom-right-radius: 38px;
    color: white;
    padding-bottom: 9px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
}
.featured_product_title{
    text-align: center;
    text-transform: uppercase;
    color: #2385aa;
    font-size: 20px;
    font-weight: 600;
    /* text-shadow: 2px 2px 4px #2385aa38; */
}
.featured_product_name{
    text-align: center;
    font-size: 16px;
    font-weight: 500;
    color: black;
    margin-bottom: 0px;
    margin-top: 0px;
    line-height:20px;
    font-weight:600;
}
.featured_product_subtitle{
    text-align: center;
    font-size: 12px;
    margin-top: 2px;
    line-height: 18px;
    color: grey;
    margin-bottom: 0px;
}
.featured_product_price{
    color: #2385aa;
    text-align: center;
    font-weight: 600;
    font-size: 15px;
    

}
.featured_product_price_old{
    color: red;
    text-align: center;
    font-weight: 600;
    font-size: 12px;
    text-decoration: line-through
}
.featured_product_price_discount{
    color: #2385aa;
    text-align: center;
    font-weight: 600;
    font-size: 12px;
}
.featured_product_image_div{
    display: flex;
    justify-content: center;
}
.featured_product_detail_div{
    height: 10rem;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
}
.footer_info_div{
    display:flex;
    grid-gap: 13px;
    height: 100%;
}
.footer_info_div .info_icon{
    margin-top: 18px;
}
.info_icon i{
    font-size:44px !important;
    color:white;
}
.info_desc h3{
    color:white;
    font-size:17px;
}
.info_desc p{
    color:white;
    font-size:14px
}
.footer h5{
    font-family: Muli;
}
.footer_header{
    text-transform: uppercase
}
.discount_badge{
    position: absolute;
    top:10px;
    background: #2385aa;
    border-radius: 50%;
    padding: 14px 15px;
}
.discount_badge p{
    line-height: 13px;
    text-align: center;
    color: white;
}
.discount_badge span{
    font-size:10px;
}
.offer_timer_div{
    position: absolute;
    top:10px;
    right:10px;
    font-weight:500;
}
.offer_subdiv_primary{
    background: #2385aa;
    height: 33px;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 20px;
}
.offer_subdiv_white{
    background:white;
    height: 33px;
    color: #2385aa;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 20px;
}
.offer_subdiv_primary p{
    margin-bottom:0;
    font-weight:600;
}
.offer_subdiv_white p{
    margin-bottom:0;
    font-weight:600;
}
.offer_time_unit{
    font-size:12px;
}


.main_left_section_div{
    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: flex-start;
    padding:3rem;
}
.nav_translate{
    height:100%;
    display: flex;
    justify-content: center;
    align-items: center;
}
.nav_brand{
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}
.logo_title{
    font-size: 16px;
    font-weight: 600;
    color: #2485aa;
    margin-top: 0px;
    margin-bottom: 0px;

    font-size: 11px;

}
.main_section_right{
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
}
.main_section_title{
    margin-bottom: 0px;
    font-size: 32px;
}
.main_section_subtitle{
    margin-top: 0px;
    color: #043892;
    font-size: 28px;
}
.steps_inner_div{
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 12px 0px;

}
.step_icon{
    height: 100%;
    flex: 2;
    text-align: center;
    color: #2485aa;
}
.step_icon span{
    font-size: 48px;
}
.step_text_div{
    flex: 4;
    padding-right: 18px;
}
.step_title{
    font-size: 16px;
    margin-bottom: 5px;
    font-weight: 600;
    color: #2485aa;
    margin-top: 0px;
}
.step_desc{
    font-size: 13px;
    line-height: 16px;
    margin-bottom: 0px;
}
.instructions_title{
    color: #043892;
    font-size: 22px;
}
input::placeholder{
    color: black !important;
}

.hover-zoom:hover img, .hover-zoom:hover video {
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
    transition: all .5s linear;
    
}
.item{
    border-radius:10px;
}
.item:hover{
    background:rgba(80, 76, 76, 0.103);
    
}

.card-product-image
{
    z-index:-1;
}

.cart_image_center{
    display: flex;
    justify-content: center;
    align-items: center;
}

.timer_alignment{
    display:flex;
    justify-content:center;
    align-items:center;
}

.header-searched-item-list-wrap {
    position: absolute;
    background: #fff;
    right: 145px;
    width: 100%;
    top:50px;
    z-index: 9;
    padding: 10px;
    box-shadow: 0 0 5px #ddd;
    border-bottom: 5px solid #2385aa;
    display: none;
    max-height: 500px;
}

.header-searched-item-list-wrap-mobile {
    position: absolute;
    background: #fff;
    right: 0;
    width: 500px;
    top: 10px;
    z-index: 9;
    padding: 10px;
    box-shadow: 0 0 5px #ddd;
    border-bottom: 5px solid #2385aa;
    display: none;
    max-height: 500px;
}

.search_dropdown_title{
    text-align: right;
    font-size: 12px;
    margin-right: 12px;
    margin-top: 0px;
}
.search_dropdown_img{
    height: 70px;
    width: 70px;
    border-radius: 30px;
    border: 1px solid #f1f1f1;
}
.dropdown_menu_profile{
    left: unset;
    right: 0;
}

.category_ul{
    justify-content: space-between;
    display: flex;
    width: 100%;
}



.card-title{
    font-family: 'Muli' !important;
}



h1,h2,h3,h4,h5{
    font-family: 'Muli' !important;
}
.h5{
    font-weight:700 !important;
}
.bmd-form-group {
    position: relative;
    padding-top: 12px;
}

font{
    font-size: 16px;;
    font-family: 'Muli' ;
}

a.underline:hover {
    text-decoration:underline;
}


li{
    font-family: 'Muli' !important;
}

.table-shopping .td-number {
    text-align: right;
    min-width: 45px;
}

.table-shopping>thead>tr>th {
    font-size: 18px;
    text-transform: capitalize;
    font-family: 'Muli';
}

.table {
    font-family: 'Muli';
}

.radio-tile-group {
    display: -webkit-box;
    display: flex;
    flex-wrap: wrap;
    -webkit-box-pack: center;
    justify-content: center;
}

.radio-tile-group .input-container {
    position: relative;
    height: 6rem;
    width: 6rem;
    margin: 0.5rem;

}

.radio-tile-group .input-container .radio-button {
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    margin: 0;
    cursor: pointer;
}

.radio-tile-group .input-container .radio-tile {
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    -webkit-box-align: center;
    align-items: center;
    -webkit-box-pack: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    border: 1px solid lightgray;
    border-radius: 10px;
    padding: 1rem;
    -webkit-transition: -webkit-transform 300ms ease;
    transition: -webkit-transform 300ms ease;
    transition: transform 300ms ease;
    transition: transform 300ms ease, -webkit-transform 300ms ease;
}

.radio-tile-group .input-container .icon svg {
    fill: #2385aa;
    width: 3rem;
    height: 3rem;
}

.radio-tile-group .input-container .radio-tile-label {
    text-align: center;
    font-size: 0.6rem;
    font-weight: 600;
    text-transform: capitalize;
    letter-spacing: 1px;
    color: gray;
}

.radio-tile-group .input-container .radio-button:checked+.radio-tile {
    background-color: #2385aa;
    border: 2px solid #2385aa;
    box-shadow: 0px 4px 4px 0px #4a4a4a;
    color: white;
    -webkit-transform: scale(1.1, 1.1);
    transform: scale(1.1, 1.1);
}

.radio-tile-group .input-container .radio-button:checked+.radio-tile .icon svg {
    fill: white;
    background-color: #2385aa;
}

.radio-tile-group .input-container .radio-button:checked+.radio-tile .radio-tile-label {
    color: white;
    background-color: #2385aa;
}

.slick-slider .slick-prev, .slick-slider .slick-next {
    z-index: 100;
    font-size: 2.5em;
    height: 40px;
    width: 40px;
    margin-top: -20px;
    color: #B7B7B7;
    position: absolute;
    top: 50%;
    text-align: center;
    color: #000;
    opacity: .3;
    transition: opacity .25s;
    cursor: pointer;
    }
    
    .slick-slider .slick-prev:hover, .slick-slider .slick-next:hover {
    opacity: .65;
    }
    .slick-slider .slick-prev {
    left: 0;
    }
    .slick-slider .slick-next {
    right: 0;
    }
    
    #detail .product-images {
    width: 340px;
    margin: 0 auto;
    border:0px solid #eee;
    }
    #detail .product-images li, #detail .product-images figure, #detail .product-images a, #detail .product-images img {
    display: block;
    outline: none;
    border: none;
    }
    #detail .product-images .main-img-slider figure {
    margin: 0 auto;
    padding: 0 2em;
    }
    #detail .product-images .main-img-slider figure a {
    cursor: pointer;
    cursor: -webkit-zoom-in;
    cursor: -moz-zoom-in;
    cursor: zoom-in;
    }
    #detail .product-images .main-img-slider figure a img {
    width: 340px;
    max-width: 400px;
    margin: 0 auto;
    }
    #detail .product-images .thumb-nav {
    margin: 0 auto;
    padding:20px 10px;
    max-width: 600px;
    }
    #detail .product-images .thumb-nav.slick-slider .slick-prev, #detail .product-images .thumb-nav.slick-slider .slick-next {
    font-size: 1.2em;
    height: 20px;
    width: 26px;
    margin-top: -10px;
    }
    #detail .product-images .thumb-nav.slick-slider .slick-prev {
    margin-left: -30px;
    }
    #detail .product-images .thumb-nav.slick-slider .slick-next {
    margin-right: -30px;
    }
    #detail .product-images .thumb-nav li {
    display: block;
    margin: 0 auto;
    cursor: pointer;
    }
    #detail .product-images .thumb-nav li img {
    display: block;
    width: 340px;
    max-width: 75px;
    margin: 0 auto;
    border: 2px solid transparent;
    -webkit-transition: border-color .25s;
    -ms-transition: border-color .25s;
    -moz-transition: border-color .25s;
    transition: border-color .25s;
    }
    #detail .product-images .thumb-nav li:hover, #detail .product-images .thumb-nav li:focus {
    border-color: #999;
    }
    #detail .product-images .thumb-nav li.slick-current img {
    border-color: #d12f81;
    }
    .bmd-form-group {
        position: relative;
        padding-top: 5px;
        padding-left:8px;
        padding-right:8px;
    }
    



