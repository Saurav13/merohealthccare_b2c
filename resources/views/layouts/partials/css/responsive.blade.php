.hidden-sm{
    display:none;
}
.hidden-lg{
    display:block;
}
.featured_product_image_div img{
    height:20rem;
}
.navbar_3{
    height:0rem;
    margin-bottom: 0;
}

.r_sm_centered{
    text-align: center;
}
#mobile-search{
    display:block;
}
#desktop-search{
    display:none !important;
}
.r_m_nav_div{
    margin:0 0px;
}

#mobile-prescription{
    display:none ;
}

@media only screen and (min-width: 600px)
{
   
    .hidden-lg{
        display:block;
    }
    .hidden-sm{
        display:none;
    }
    .featured_product_image_div img{
        height:20rem;
    }
    .navbar_3{
        height:0rem;
        margin-bottom: 0;
    }
    .r_sm_centered{
        text-align: center;
    }
    #mobile-search{
        display:block;
    }
    #desktop-search{
        display:none !important;
    }
    .r_m_nav_div{
        margin:0 0px;
    }
}

@media only screen and (min-width: 768px)
{
    
    .hidden-lg{
        display:block;
    }
    .hidden-sm{
        display:none;
    }
    .featured_product_image_div img{
        height:18rem;
    }
    .navbar_3{
        height:3rem;
        margin-bottom: 0;
    }
    .r_sm_centered{
        text-align: left;
    }
    #desktop-search{
        display:block !important;
    }
    #mobile-search{
        display:none !important;
    }
    .r_m_nav_div{
        margin:0 33px !important;
    }
}

@media only screen and (min-width: 1100px)
{
    .hidden-lg{
        display:none;
    }
    .hidden-sm{
        display:block;
    }
    
    .featured_product_image_div img{
    height:13rem;
    }
    .navbar_3{
        height:3rem;
        margin-bottom: 0;
    }
  
    .r_sm_centered{
        text-align: left;
    }
    #desktop-search{
        display:block;
    }
    #mobile-search{
        display:none !important;
    }
}

@media only screen and (max-width: 768px)
{
    #mobile-prescription{
        display:block;
    }
   
}

@media only screen and (min-width: 768px and max-width:1024px)
{
    .featured_product_body{
        height:30rem !important;
    }

}

@media only screen and (max-width:768px)
{
    .footer-spacing{
       display:block !important;
    }
}


@media only screen and (max-width:768px)
{
    .catalog-mobile-filter{
       display:block !important;
    }
}

