.owl-carousel .owl-wrapper:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
}
/* display none until init */
.owl-carousel{
    display: none;
    position: relative;
    width: 100%;
    -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper{
    display: none;
    position: relative;
    -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer{
    overflow: hidden;
    position: relative;
    width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight{
    -webkit-transition: height 500ms ease-in-out;
    -moz-transition: height 500ms ease-in-out;
    -ms-transition: height 500ms ease-in-out;
    -o-transition: height 500ms ease-in-out;
    transition: height 500ms ease-in-out;
}

.owl-carousel .owl-item{
	float: left;
}
.owl-controls .owl-page,
.owl-controls .owl-buttons div{
	cursor: pointer;
}
.owl-controls {
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

/* mouse grab icon */
.grabbing {
    cursor:url(grabbing.png) 8 8, move;
}

/* fix */
.owl-carousel  .owl-wrapper,
.owl-carousel  .owl-item{
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility:    hidden;
	-ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0,0,0);
  -moz-transform: translate3d(0,0,0);
  -ms-transform: translate3d(0,0,0);
}

        h1,h2,h3,h4,h5,h6,p,span,text,button,input{
    font-family: Muli;
}
p{
    font-size:16px;
}
.overlay{
    background-color: #00000021;
    height: 100%;
}
.nav_container{
    height: 5rem;
    display: flex;
    align-items: center;
}

.nav_items{
    color:#2385aa;
}

.nav_link{
    text-transform: capitalize !important;
    font-size:16px !important;
    font-family: Muli;
    padding: 10px 7px !important;
}
.m_nav_div{
    display: flex;
    justify-content: center;
    align-items: center
}

.m_nav_link{
    text-transform: capitalize !important;
    font-size:13px !important;
    font-family: Muli;
    padding: 10px 7px !important;
    color:white;
}
.m_nav_link i{
    color:white;
}

.nav_icons{
    display: flex;
    justify-content: center;
    flex-direction: row;
}
.navbar_2{
    background: #f3f3f3;
    height:4rem;
    margin-bottom: 0;
}
.nav_2_rows{
    display: flex;
    align-items: center;
    justify-content: start;
    width: 100%;
    height: 100%;
}
.nav_link_2{
    text-transform: uppercase !important;
    font-size:13px !important;
    font-family: Muli;
    padding: 10px 7px !important;
    color: black !important;
}
.nav_upload_pres_div{
    flex:2
}
.nav_menu_link_div{
    flex:4
}
.nav_search_div{
    flex:4
}
.nav_search_btn{
    width: 13% !important;
    margin-top: 8px !important;
}
.nav_search_input{
    width:85% !important;
    margin-top: 14px !important;

}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    width: 90%;
    left: 0;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    margin: 0px 5%;
    overflow-y: auto;
    height:70vh;
}

.dropdown:hover .dropdown-content {
    display: block;
}
.dropdown_header{
    margin-top:1rem;
    padding-left:2rem;
}

.category_link{
    font-family: Muli;
    color:black !important;
}
.side_category_ul{
    padding:0;
}
.side_category_ul li{
    padding: 0px 0;
}
.side_category_title{
    font-size:15px;
    margin-top:10px;
    margin-bottom: 10px;
    text-transform: uppercase;
    color:#2385aa;
}
.side_category_links{
    color: black;
    font-size: 13px;
}
.banner_card_container{
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
}
.banner_card_text{
    color: #000 !important;
    font-weight: 500;
    text-shadow: 2px 2px 4px #fff;
    font-size: 18px;
}
.banner_card_div{
    box-shadow: 0 10px 15px 0 rgba(0, 0, 0, 0.14);
    border-radius: 30px;
    background: #eeeeee24 !important;
    padding: 2rem 3rem;
}
.offer_div{
    background: #eee !important;
    text-align: center;
}
.offer_div h5{
    margin:10px;
}
.offer_div a{
    font-size: 15px;
    color: #555 !important;
    text-transform: uppercase;
    font-weight: 600;
}
.offer_div i{
    float:none !important;
    top:0px !important;
}

.banner {
    position: relative;
    /* z-index: 1; */
    margin: 80px auto;
    width: 260px;
}

.banner .line {
    margin: 0 0 10px;
    width: 100%;
    height: 60px;
    box-shadow: 10px 10px 10px rgba(0,0,0,0.05);
    text-align: center;
    text-transform: uppercase;
    font-size: 2em;
    line-height: 60px;
    transform: skew(0, -15deg);
}

.banner .line:after,
.banner .line:first-child:before {
    position: absolute;
    top: 35px;
    left: 0;
    z-index: -1;
    display: block;
    width: 260px;
    height: 58px;
    border-radius: 4px;
    background: rgba(180,180,180,0.8);
    content: '';
    transform: skew(0, 15deg);
}

.banner .line:first-child:before {
    top: -6px;
    right: 0;
    left: auto;
}

.banner .line:first-child:before,
.banner .line:last-child:after {
    width: 0;
    height: 0;
    border-width: 27px;
    border-style: solid;
    border-color: rgba(180,180,180,0.8) rgba(180,180,180,0.8) transparent transparent;
    background: transparent;
}

.banner .line:last-child:after {
    top: 12px;
    border-color: transparent transparent rgba(180,180,180,0.8) rgba(180,180,180,0.8);
}

.banner span {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    background: rgba(255,255,255,0.9);
    color: #666;
    text-shadow: 1px 1px 0 #444;
}

.progress-steps {
    font-family: 'Open Sans', sans-serif;
  }
  .progress-steps > div {
    margin-left: 39px;
    position: relative;
  }
  .progress-steps > div.hidden {
    display: none;
  }
  .progress-steps > div.active > h1::before {
    border: none;
  }
  .progress-steps > div.complete > h1 {
    color:#dc3545;
    font-weight: 600;
  }
  .progress-steps > div.complete > h1 > .number {
    background: #dc3545;
    color: #FFF;
  }
  .progress-steps > div:last-child > h1::before {
    border: none;
  }
  .progress-steps > div > h3 {
    font-size: 1em;
    margin-left: -39px;
    color: #222;
    font-weight: 600;
  }
  .progress-steps > div > h3 > .number {
    background: #2385aa;
    color: #FFF;
  }
  .progress-steps > div > h3::before {
    content: '';
    position: absolute;
    border-left: dashed 1px #dc3545;
    width: 1px;
    height: calc(100% - 27px);
    top: 39px;
    left: -22.5px;
  }
  .progress-steps > div > h3 > .number {
    border-radius: 50%;
    width: 33px;
    height: 33px;
    display: inline-block;
    text-align: center;
    line-height: 33px;
  }
  .progress-steps > div:last-child > h3::before {
    border: none;
}
.featured_offer_header{
    display: flex;
    justify-content: center;
    padding: 14px;
}
.offer_title_red{
    background:#dc3545;
    padding: 6px 16px;
    border-top-left-radius: 38px;
    border-bottom-left-radius: 38px;
    color: white;
    padding-bottom: 9px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
}
.offer_title_green{
    background:#71c02d;
    padding: 6px 16px;
    border-top-right-radius: 38px;
    border-bottom-right-radius: 38px;
    color: white;
    padding-bottom: 9px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
}
.featured_product_title{
    text-align: center;
    text-transform: uppercase;
    color: #2385aa;
    font-size: 20px;
    font-weight: 600;
    /* text-shadow: 2px 2px 4px #2385aa38; */
}
.featured_product_name{
    text-align: center;
    font-size: 15px;
    font-weight: 500;
    color: black;
    margin-bottom: 0px;
    margin-top: 0px;
    line-height:20px;
}
.featured_product_subtitle{
    text-align: center;
    font-size: 12px;
    margin-top: 2px;
    line-height: 18px;
    color: grey;
    margin-bottom: 0px;
}
.featured_product_price{
    color: #2385aa;
    text-align: center;
    font-weight: 600;
    font-size: 15px;


}
.featured_product_price_old{
    color: red;
    text-align: center;
    font-weight: 600;
    font-size: 12px;
    text-decoration: line-through
}
.featured_product_price_discount{
    color: #2385aa;
    text-align: center;
    font-weight: 600;
    font-size: 12px;
}
.featured_product_image_div{
    display: flex;
    justify-content: center;
}

.footer_info_div{
    display:flex;
    grid-gap: 13px;
    height: 100%;
}
.footer_info_div .info_icon{
    margin-top: 18px;
}
.info_icon span{
    font-size:44px;
    color:white;
}
.info_desc h3{
    color:white;
    font-size:17px;
}
.info_desc p{
    color:white;
    font-size:14px
}
.footer h5{
    font-family: Muli;
}
.footer_header{
    text-transform: uppercase
}
.discount_badge{
    position: absolute;
    top: 24px;
    background: #2385aa;
    border-radius: 50%;
    padding: 14px 15px;
}
.discount_badge p{
    line-height: 13px;
    text-align: center;
    color: white;
}
.discount_badge span{
    font-size:10px;
}
.offer_timer_div{
    position: absolute;
    right: 0px;
    top: 35px;
}
.offer_subdiv_primary{
    background: #2385aa;
    height: 33px;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 25px;
}
.offer_subdiv_white{
    background:white;
    height: 33px;
    color: #2385aa;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 25px;
}
.offer_subdiv_primary p{
    margin-bottom:0;
    font-weight:600;
}
.offer_subdiv_white p{
    margin-bottom:0;
    font-weight:600;
}
.offer_time_unit{
    font-size:12px;
}


.main_left_section_div{
    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: flex-start;
    padding:3rem;
}
.nav_translate{
    height:100%;
    display: flex;
    justify-content: center;
    align-items: center;
}
.nav_brand{
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}
.logo_title{
    font-size: 16px;
    font-weight: 600;
    color: #2485aa;
    margin-top: 0px;
    margin-bottom: 0px;

    font-size: 11px;

}
.main_section_right{
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
}
.main_section_title{
    margin-bottom: 0px;
    font-size: 32px;
}
.main_section_subtitle{
    margin-top: 0px;
    color: #043892;
    font-size: 28px;
}
.steps_inner_div{
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 12px 0px;

}
.step_icon{
    height: 100%;
    flex: 2;
    text-align: center;
    color: #2485aa;
}
.step_icon span{
    font-size: 48px;
}
.step_text_div{
    flex: 4;
    padding-right: 18px;
}
.step_title{
    font-size: 16px;
    margin-bottom: 5px;
    font-weight: 600;
    color: #2485aa;
    margin-top: 0px;
}
.step_desc{
    font-size: 13px;
    line-height: 16px;
    margin-bottom: 0px;
}
.instructions_title{
    color: #043892;
    font-size: 22px;
}
input::placeholder{
    color: black !important;
}

        .hidden-sm{
    display:none;
}
.hidden-lg{
    display:block;
}
.featured_product_image_div img{
    height:20rem;
}
.navbar_3{
    height:0rem;
    margin-bottom: 0;
}

.r_sm_centered{
    text-align: center;
}

@media only screen and (min-width: 600px)
{

    .hidden-lg{
        display:block;
    }
    .hidden-sm{
        display:none;
    }
    .featured_product_image_div img{
        height:20rem;
    }
    .navbar_3{
        height:0rem;
        margin-bottom: 0;
    }
    .r_sm_centered{
        text-align: center;
    }
}

@media only screen and (min-width: 768px)
{

    .hidden-lg{
        display:none;
    }
    .hidden-sm{
        display:block;
    }
    .featured_product_image_div img{
        height:18rem;
    }
    .navbar_3{
        height:3rem;
        margin-bottom: 0;
    }
    .r_sm_centered{
        text-align: left;
    }
}

@media only screen and (min-width: 1100px)
{
    .hidden-lg{
        display:none;
    }
    .hidden-sm{
        display:block;
    }

    .featured_product_image_div img{
    height:13rem;
    }
    .navbar_3{
        height:3rem;
        margin-bottom: 0;
    }

    .r_sm_centered{
        text-align: left;
    }
}
{{--@font-face {
                font-family: Muli;
                src: url(//fonts.googleapis.com/css?family=Muli);
            }--}}

            @media all and (min-width: 992px) {
                .navbar {
                    padding-top: 0;
                    padding-bottom: 0;
                }

                .navbar .has-megamenu {
                    position: static !important;
                }

                .navbar .megamenu {
                    left: 0;
                    right: 0;
                    width: 100%;
                    padding: 20px;
                }

                .navbar .nav-link {
                    padding-top: 1rem;
                    padding-bottom: 1rem;
                }
            }
            .header-searched-item-list-wrap {
                position: absolute;
                background: #fff;
                right: 275px;
                width: 500px;
                top:50px;
                z-index: 9;
                padding: 10px;
                box-shadow: 0 0 5px #ddd;
                border-bottom: 5px solid #2385aa;
                display: none;
                max-height: 500px;
            }
            .scroll-box {

                overflow-y: scroll;
                }
                /* .scroll-box::-webkit-scrollbar {
                width:.4rem;
                }
                .scroll-box::-webkit-scrollbar,
                .scroll-box::-webkit-scrollbar-thumb {
                overflow:visible;
                border-radius: 4px;
                }
                .scroll-box::-webkit-scrollbar-thumb {
                background: rgba(0,0,0,.2);
                } */
