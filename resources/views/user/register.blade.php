@extends('layouts.front1')
@section('title','Register')
@section('content')

<style>

    @media(min-width:320px) and (max-width:720px){
    .index-page .page-header, .presentation-page .page-header {
    height: auto !important;
    overflow: hidden;
    }
}
    .index-page .page-header, .presentation-page .page-header {
    height: auto;
    overflow: hidden;
    }
    .card-login .form-check {
        padding-top:0px !important;
    }
    .form-control::placeholder{
    color:#999 !important;
  }
  .card-login .input-group {
    padding-bottom: 7px;
    margin: 0px 0 0 0;
}
</style>

<div class="page-header" style="background-image: url('https://demos.creative-tim.com/material-kit-pro/assets/img/bg7.jpg'); background-size: cover; background-position: top center;">

    <div class="container mt-4">
     
      <div class="row">
        <div class="col-sm-8 ml-auto mr-auto">
            <form action="{{route('user-register-submit')}}" method="POST">
                {{csrf_field()}}
            <div class="card card-login card-hidden">
              <div class="card-header card-header-success text-center">
                <h4 class="card-title">Register</h4>
              </div>
              @include('includes.form-error')
              <div class="card-body">
                  <div class="row">
                      <div class="col-md-6">
                            <span class="bmd-form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="firstname" value="{{old('firstname')}}" placeholder="First Name..." required>
                                </div>
                            </span>
                            <span class="bmd-form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="lastname" value="{{old('lastname')}}" placeholder="Last Name..." required>
                                </div>
                            </span>

                            <span class="bmd-form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="material-icons">map</i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="address" value="{{old('address')}}" placeholder="Address..." required>
                                </div>
                            </span>
                            <span class="bmd-form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="material-icons">calendar_today</i>
                                    </span>
                                </div>
                                <input type="date" class="form-control" name="dob" value="{{old('dob')}}" placeholder="Date of Birth ..." required>
                                </div>
                            </span>
                            

                                <h6 class="text-center">Gender</h6>
                                <div class="form-check form-check-radio form-check-inline text-center" style="margin-left:5px;">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="radio" name="gender" value="Male" checked> Male
                                      <span class="circle">
                                          <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                  <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="radio" name="gender" value="Female"> Female
                                      <span class="circle">
                                          <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                  <div class="form-check form-check-radio form-check-inline disabled">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="radio" name="gender" value="Others"> Others
                                      <span class="circle">
                                          <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                             

                                
                      </div>
                      <div class="col-md-6">
                        <span class="bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">email</i>
                                </span>
                                </div>
                                <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email..." required>
                            </div>
                            </span>

                   

                        <span class="bmd-form-group">
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <i class="material-icons">phone</i>
                                </span>
                            </div>
                            <input type="text" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Phone eg. 9841000000" required> 
                            </div>
                        </span>
          
                        <span class="bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                </div>
                                <input type="password" class="form-control" id="see_repassword" name="password" placeholder="Password..." required>
                            </div>
                            </span>

                            <span class="bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                </div>
                                <input type="password" class="form-control" id="see_password" name="password_confirmation" placeholder="Re-Password..." required>
                            </div>
                            </span>

                            <div class="form-check" style="padding-top:0px; margin-left:5px;">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" onclick="myFunction()"> &nbsp;&nbsp;Show Password
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                      </div>
                    
                  </div>

              </div>

              <div class="form-group {{$lang->rtl == 1 ? 'text-right' : ''}} ">
                <select id="reg_usertype" name="user_type" class="form-control rounded-0" id="" hidden>
                  <option>Customer</option>
                </select>
              </div>

              <div class="card-footer justify-content-center">
                <button type="submit" id="register_text" class="btn btn-success btn-rounded btn-block"><span><i class="fa fa-circle-o-notch fa-spin" id="loader" style="display: none;"></i></span> Register</button>
              </div>

                <p class="card-description text-center" style="font-size:14px;">Already have account? <a href="{{route('user-login')}}" class="text-primary ">Login</a></p>
       
           
            </div>
          </form>
        </div>
      </div>
    </div>
 
  </div>


@endsection


@section('scripts')

<script>
function myFunction() {
  var x = document.getElementById("see_password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }

  var y = document.getElementById("see_repassword");
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}

function registerText() {
  var x = document.getElementById("register_text");
  if (x.innerHTML === "Register") {
    x.innerHTML = "Registering Please wait .... !";
  } else {
    x.innerHTML = "Register";
  }
}

$('#register_text').on('click',function(){
    var x = document.getElementById("loader");
    console.log('click');
	x.style.display = "";

});

</script>
@endsection