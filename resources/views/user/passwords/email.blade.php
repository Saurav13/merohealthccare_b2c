@extends('layouts.front1')
@section('title','Reset Password')
@section('content')
<div class="section-padding login-area-wrapper">
           <div class="container">
               <div class="row">
                   <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 mr-auto ml-auto">
                       <div class="signIn-area">
                           <h2 class="signIn-title text-center">{{$lang->fpt}}</h2>
                           <hr>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                           @include('includes.form-error')
                           @include('includes.form-success')
                           <div class="login-form">
                           <form action="{{route('user-forgot-submit')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="material-icons">mail</i>
                                    </span>
                                  </div>
                                  <input class="form-control" placeholder="yourmail@example.com" type="email" name="email" id="forgot_email" required="">
                                </div>
                              </div>
        
                               <div class="form-group">
                                   <button type="submit" class="btn btn-info btn-block">{{$lang->fpb}}</button>
                               </div>
                               <div class="form-group">
                                   <div class="row">
                                       <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                            <a href="{{route('user-login')}}">{{$lang->al}}</a>
                                       </div>
                                   </div>
                               </div>
                           </form>
                       </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
@endsection