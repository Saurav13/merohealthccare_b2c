@extends('layouts.front1')
@section('title','Login')
@section('content')
<style>
    .index-page .page-header, .presentation-page .page-header {
    height: auto !important;
    overflow: hidden;
    }
    .form-control::placeholder{
    color:#999 !important;
  }
  .card-login .input-group {
    padding-bottom: 7px;
    margin: 0px 0 0 0;
}
</style>
<div class="page-header" style="background-image: url('https://demos.creative-tim.com/material-kit-pro/assets/img/bg7.jpg'); background-size: cover; background-position: top center;">
    
    <div class="container mt-4">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
            <form action="{{route('user-login-submit')}}" method="POST">
                {{csrf_field()}}
            <div class="card card-login card-hidden">
              <div class="card-header card-header-info text-center">
                <h4 class="card-title">Login</h4>
                <div class="social-line">
                  <a href="{{route('social-provider','facebook')}}" class="btn btn-just-icon btn-link btn-white">
                    <i class="fa fa-facebook-square"></i>
                  </a>
                  <a href="{{route('social-provider','google')}}" class="btn btn-just-icon btn-link btn-white">
                    <i class="fa fa-google"></i>
                  </a>
                </div>
              </div>

              @include('includes.form-success')
              <div class="card-body">
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">email</i>
                      </span>
                    </div>
                    <input type="email" class="form-control" name="email" placeholder="Email..." required>
                  </div>
                </span>
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" class="form-control" name="password" id="see_password" placeholder="Password..." required>
                   
                  </div>

                  <div class="form-check" style="padding-top:0px; margin-left:5px;">
                    <label class="form-check-label">
                      <input class="form-check-input" type="checkbox" onclick="myFunction()"> &nbsp;&nbsp;Show Password
                      <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                  </div>
                </span>
              </div>
              <div class="card-footer justify-content-center">
                <button type="submit" class="btn btn-info btn-rounded btn-block">Login</button>
              </div>
              <p class="card-description text-center" style="font-size:13px;"><a href="{{route('user-forgot')}}" class="text-warning ">Forgot Password?</a></p>
                <p class="card-description text-center" style="font-size:14px;">Dont have account? <a href="{{route('user-register')}}" class="text-success ">Register</a></p>
        
           
            </div>
          </form>
        </div>
      </div>
    </div>
 
  </div>

@endsection


@section('scripts')

<script>
    function myFunction() {
  var x = document.getElementById("see_password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endsection