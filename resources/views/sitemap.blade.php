<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  @foreach ($static_links as $cat)
    <url>
        <loc>{{URL::to('/'.$cat)}}</loc>
     </url>
     @endforeach
       @foreach ($blogs as $cat)
    <url>
		<loc>{{URL::to('/blog/'.$cat->slug)}}</loc>
	 </url>
	 @endforeach

	 @foreach ($categories as $cat)
	<url>
		<loc>{{URL::to('/category/'.$cat->cat_slug)}}</loc>
	 </url>
	 @endforeach
     @foreach ($subcategories as $cat)
    <url>
		<loc>{{URL::to('/subcategory/'.$cat->sub_slug)}}</loc>

	 </url>
	 @endforeach
     @foreach ($childcategories as $cat)
    <url>
		<loc>{{URL::to('/childcategory/'.$cat->child_slug)}}</loc>
	 </url>
	 @endforeach

     @foreach ($products as $product)
    <url>
        <loc>{{URL::to('/product/'.$product->id.'/'.$product->slug)}}</loc>
	 </url>
	 @endforeach

</urlset>
