@include('layouts.partials.header1')
@include('layouts.partials.navbar1')


    {{-- <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'> --}}
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Muli'>


    <style>
        .page_404 {
            padding: 40px 0;
            background: #fff;
            font-family: 'Arvo', serif;
        }

        .page_404 img {
            width: 100%;
        }

        .four_zero_four_bg {

            background-image: url(https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif);
            height: 400px;
            background-position: center;
        }


        .four_zero_four_bg h1 {
            font-size: 80px;
        }

        .four_zero_four_bg h3 {
            font-size: 80px;
        }

        .link_404 {
            color: #fff !important;
            padding: 10px 20px;
            background: #2385aa;
            margin: 20px 0;
            display: inline-block;
            border-radius: 20px;
        }

        .contant_box_404 {
            margin-top: -50px;
        }

    </style>

    <body>

        <section class="page_404">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="col-sm-10 text-center ml-auto mr-auto">
                            <div class="four_zero_four_bg">
                                <h1 class="text-center ">404</h1>


                            </div>

                            <div class="contant_box_404">
                                <h3 class="h2">
                                    Look like you're lost
                                </h3>

                                <p>the page you are looking for not available!</p>

                                <a href="/" class="btn btn-info btn-round">Go to Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@include('layouts.partials.footer1')
