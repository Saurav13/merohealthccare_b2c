
<style>
    .s_item:hover{
        color:#2385aa;
    }
    
    .product-details-wrapper .productDetails-quantity span, .productDetails-quantity span {
        margin-top: 10px;
        display: inline-block;
        width: 30px;
        height: 30px;
        line-height: 30px;
        border: 1px #d9d9d9 solid;
        text-align: center;
        font-size: 12px;
        color: #4c4c4c;
        font-weight: 500;
        margin-right: -5px;
        position: relative;
        margin-bottom: 10px;
    }
    hr {
    margin-top: 0.25rem;
    margin-bottom: 0.25rem;
    }

    @media (max-width: 767px) and (min-width: 320px){}
    .header-searched-item-list-wrap-mobile {
        width: 100% !important;
        top: 70px;
        z-index: 99999;
    }
    }
    
    </style>

    <h6><b>Available Product :</b> <span style="color:#28a745">{{count($products)}}/{{count($product_count)}} Showing</span></h6>
    @forelse($products as $product)
        @php
        $product->pprice = $product->pprice ? : $product->cprice;
        $product->cprice = $product->getPrice(1);
        // dd('asdfasdfas');
        @endphp
    
    
        {{-- <li> --}}
            <a class="g-pt-0" style="text-decoration:gold;" href="{{ route('front.product',[$product->id,str_slug($product->name,'-')]) }}" class="s_item">
                <div class="row">
                    <div class="col-2" style="padding-left:0px;padding-right:0px;">
                        <img src="{{ asset('/assets/images/'.$product->photo) }}" style="height: 50px;width: 50px;border-radius: 30px;border: 1px solid #f1f1f1;" alt="{{$product->name}}" />
                    </div>
                    <div class="col-10" style=" ">
                        <p class="g-font-size-12 mb-0 h6 g-mt-0 g-font-weight-700" style="font-size:14px;font-weight:600;margin-left:15px;">{{ ucwords(strtolower($product->name)) }} </p>
                        {{-- <h6 class="g-font-size-12 mb-0">{{ $product->sub_title }} </h6> --}}
                        <p style="font-size:12px !important; color:gray; margin-left:15px;">{{ucwords(strtolower($product->company_name))}}</p>
                        @if($product['requires_prescription'])
                        <h6 style="color:red;font-size:11px;">
                         
                         Prescription Required
                        </h6>
                        @endif  
                        <hr style="margin-left:15px;"/>
                    </div>
                </div>
            </a>
        {{-- </li> --}}

    
     
   

    @empty
        
    <h6 class="text-center">No product found!</h6>

    @endforelse

    