<div class="modal fade" id="catalogFilter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content"
            style="background:transparent;box-shadow: 0 0px 0px 0 rgb(0 0 0 / 20%), 0 0 0px 0 rgb(0 0 0 / 22%) !important;">
            {{-- <div class="modal-header">
          <h5 class="modal-title" id="catalogFilter">Apply Filter</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div> --}}
            <div class="modal-body">
                <style>
                    .product-filter-option ul li {
                        border-top: 0px;
                        padding: 5px 5px;
                    }

                    .product-filter-option ul li:last-child {
                        border-bottom: 0px dotted #000;
                    }

                    .product-filter-option {
                        margin-bottom: 15px;
                    }

                    .form-control::placeholder {
                        color: #999 !important;
                    }

                    @media(min-width:320px) and (max-width:720px) {
                        #sidebar {
                            display: none;
                        }
                    }

                </style>


                <div id="" class="">

                    <div class="container">
                        <div class="card card-nav-tabs"
                            style="margin-top:60px;box-shadow: 0 27px 24px 0 rgb(0 0 0 / 20%), 0 40px 77px 0 rgb(0 0 0 / 22%);">
                            <div class="text-center card-header card-header-info"
                                style="font-weight:700;z-index:1;box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%);">
                                <a style="text-decoration: none !important;"><i class="material-icons">filter_list</i>
                                    Filters </a>
                            </div>

                            <div class="container">

                                <div id="accordion" role="tablist">
                                    <div class="card card-collapse">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a class="text-primary" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Sort By
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" role="tabpanel"
                                            aria-labelledby="headingOne">
                                            @if ($sort == 'new')
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="new" checked>
                                                        {{ $lang->doe }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @else
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="new">
                                                        {{ $lang->doe }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif

                                            @if ($sort == 'old')
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="old" checked>
                                                        {{ $lang->dor }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @else
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="old">
                                                        {{ $lang->dor }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif

                                            @if ($sort == 'low')
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="low" checked>
                                                        {{ $lang->dopr }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @else
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="low">
                                                        {{ $lang->dopr }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif

                                            @if ($sort == 'high')
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="high" checked>
                                                        {{ $lang->doc }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @else
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input sortbyradiobutton" type="radio"
                                                            name="sortBy" value="high">
                                                        {{ $lang->doc }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif


                                            <br>
                                        </div>
                                    </div>

                                </div>
                                <div class="card card-collapse">
                                    <div class="card-header" role="tab" id="headingFour">
                                        <h5 class="mb-0">
                                            <a class="text-primary" data-toggle="collapse" href="#collapseFour"
                                                aria-expanded="true" aria-controls="collapseFour">
                                                Price Range
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse show" role="tabpanel"
                                        aria-labelledby="headingFour">
                                        <div class="card-body" style="padding-top:0px;">
                                            @if (isset($cat))
                                                <form action="{{ route('front.category', $cat->cat_slug) }}"
                                                    method="GET">

                                                @elseif(isset($subcat))
                                                    <form action="{{ route('front.subcategory', $subcat->sub_slug) }}"
                                                        method="GET">

                                                    @elseif(isset($childcat))
                                                        <form
                                                            action="{{ route('front.childcategory', $childcat->child_slug) }}"
                                                            method="GET">

                                                        @elseif(isset($vcats))
                                                            <form
                                                                action="{{ route('front.vendor.category', ['slug1' => str_replace(' ', '-', $vendor->shop_name), 'slug2' => $vcat->cat_slug]) }}"
                                                                method="GET">

                                                            @elseif(isset($vsubcats))
                                                                <form
                                                                    action="{{ route('front.vendor.subcategory', ['slug1' => str_replace(' ', '-', $vendor->shop_name), 'slug2' => $vsubcat->sub_slug]) }}"
                                                                    method="GET">

                                                                @elseif(isset($vchildcats))
                                                                    <form
                                                                        action="{{ route('front.vendor.childcategory', ['slug1' => str_replace(' ', '-', $vendor->shop_name), 'slug2' => $vchildcat->child_slug]) }}"
                                                                        method="GET">

                                                                    @elseif(isset($sproducts))
                                                                        <form
                                                                            action="{{ route('front.searchs', $search) }}"
                                                                            method="GET">

                                                                        @elseif(isset($wproducts))
                                                                            <form
                                                                                action="{{ route('user-wishlists') }}"
                                                                                method="GET">

                                                                            @elseif(isset($vprods))
                                                                                <form
                                                                                    action="{{ route('front.vendor', str_replace(' ', '-', $vendor->shop_name)) }}"
                                                                                    method="GET">
                                            @endif

                                            <div class="card-body card-refine" style="display: flex;padding-top:0px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding:0 1px;">
                                                            <i class="material-icons">arrow_downward</i>Rs.
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control price-input" placeholder="0"
                                                        id="price-min" name="min"
                                                        value="{{ isset($min) ? $min : '0' }}">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding:0 1px;">
                                                            <i class="material-icons">arrow_upward</i>Rs.
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control price-input"
                                                        placeholder="1500" id="price-max" name="max"
                                                        value="{{ isset($max) ? $max : '250' }}">
                                                </div>

                                            </div>

                                            <div class="text-center">
                                                <button type="submit"
                                                    class="btn btn-info text-center btn-block price-search-btn"
                                                    style="z-index:1"><i class="fa fa-search"></i> Search<div
                                                        class="ripple-container"></div></button>
                                            </div>
                                            </form>
                                            <button type="button" class="btn btn-block btn-rose "
                                                data-dismiss="modal">Close</button>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>




            </div>

        </div>
    </div>
