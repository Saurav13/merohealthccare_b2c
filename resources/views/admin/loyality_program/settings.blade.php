@extends('layouts.admin')

@section('content')
  <div class="right-side">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <!-- Starting of Dashboard area -->
              <div class="section-padding add-product-1">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="add-product-box">
                        <div class="product__header"  style="border-bottom: none;">
                            <div class="row reorder-xs">
                                <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                    <div class="product-header-title">
                                        <h2>Loyality Program Settings</h2>
                                        <p>Dashboard <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Loyality Program <i class="fa fa-angle-right" style="margin: 0 2px;"></i> Settings
                                    </div>
                                </div>
                                  @include('includes.notification')
                            </div>   
                        </div>
                        <hr>
                      <div>

                          {{-- @include('includes.form-error') --}}
                          @include('includes.form-success')

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div class="tab-pane fade active in">
                              <form class="form-horizontal" action="{{ route('admin-loyality-settings') }}" method="POST" enctype="multipart/form-data" id="form1">
                                {{csrf_field()}}
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="loyality_percentage">Loyality Percentage* </label>
                                  <div class="col-sm-6">
                                    <div class="input-group">
                                      <input class="form-control" name="loyality_percentage" value="{{ $loyality_percentage }}" id="loyality_percentage" placeholder="e.g 15%" step="0.01" type="number" min="0" max="100" required>
                                      <span class="input-group-addon"> %</span>
                                    </div>
                                    @if ($errors->has('loyality_percentage'))
                                      <span class="text-danger" role="alert">
                                          <strong>{{ $errors->first('loyality_percentage') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                </div>
                                <hr>
                                <div class="add-product-footer">
                                    <button type="submit" class="btn add-product_btn">Save</button>
                                </div>
                              </form>
                            </div>

                          </div>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <!-- Ending of Dashboard area --> 
            </div>
        </div>
    </div>
  </div>

@endsection
