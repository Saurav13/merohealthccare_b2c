@extends('layouts.admin')
@section('title', 'Courier Charge')

@section('content')

    <div class="right-side">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- Starting of Dashboard data-table area -->
                    <div class="section-padding add-product-1">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="add-product-box">
                                    <div class="product__header">
                                        <div class="row reorder-xs">
                                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                                <div class="product-header-title">
                                                    <h2>Courier Charge</h2>
                                                    <p>Dashboard <i class="fa fa-angle-right" style="margin: 0 2px;"></i>
                                                        Courier Charge</p>
                                                </div>
                                            </div>
                                            @include('includes.notification')
                                        </div>
                                    </div>
                                    <div>
                                        @include('includes.form-error')
                                        @include('includes.form-success')
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table id="product-table_wrapper"
                                                        class="table table-striped table-hover products dt-responsive"
                                                        cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 30px;">Id</th>
                                                                <th style="width: 100px;">District</th>
                                                                <th style="width: 74px;">Courier Charge</th>
                                                                <th style="width: 74px;">Status</th>
                                                                <th style="width: 175px;">Action</th>

                                                        </thead>

                                                        <tbody>
                                                            @foreach ($districts as $district)
                                                                <tr role="row" class="odd">
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{ $district->district }}</td>
                                                                    <td>Rs. {{ $district->courier_charge }}
                                                                    </td>
                                                                    <td>
                                                                        <span class="dropdown">
                                                                            <button class="btn btn-{{$district->status == 'Active' ? "success" : "danger"}} product-btn dropdown-toggle btn-xs" type="button" data-toggle="dropdown" style="font-size: 14px;">{{$district->status == 'Active' ? "Active" : "Deactive"}}
                                                                            <span class="caret"></span></button>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li><a href="{{route('admin-couriercharge-status',['id1'=>$district->id,'id2'=>'Active'])}}">Active</a></li>
                                                                                        <li><a href="{{route('admin-couriercharge-status',['id1'=>$district->id,'id2'=>'Deactive'])}}">Deactive</a></li>
                                                                                    </ul>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{ route('admin-couriercharge-edit', $district->id) }}"
                                                                            class="btn btn-primary product-btn"><i
                                                                                class="fa fa-edit"></i> Edit</a>
                                                                    </td>

                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ending of Dashboard data-table area -->
            </div>
        </div>
    </div>

@endsection
