<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourierChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tableName = 'courier_charges';
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id');
        $table->string('district');
        $table->float('courier_charge');
        $table->string('status');
        $table->nullableTimestamps();

        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
